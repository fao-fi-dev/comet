/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.basic;

import org.fao.fi.comet.core.io.providers.impl.multiple.MultipleStreamingSeekableDataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.StreamingSeekableDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2013
 */
public class MultipleStreamableSeekableDataProvider<ENTITY> extends MultipleStreamingSeekableDataProviderSkeleton<ENTITY> {
	/**
	 * Class constructor
	 *
	 * @param dataProviders
	 */
	@SafeVarargs public MultipleStreamableSeekableDataProvider(StreamingSeekableDataProvider<ENTITY>... dataProviders) {
		super(dataProviders);
	}

	/**
	 * Class constructor
	 *
	 * @param providerId
	 * @param dataProviders
	 */
	public MultipleStreamableSeekableDataProvider(String providerId, StreamingSeekableDataProvider<ENTITY> dataProviders) {
		super(providerId, dataProviders);
	}
}
