/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.support;

import java.util.Arrays;
import java.util.Collection;

import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
public class MultipleIterator<ENTITY> extends GenericMultipleIterator<ProvidedData<ENTITY>, Iterable<ProvidedData<ENTITY>>> {
	@SafeVarargs public MultipleIterator(Iterable<ProvidedData<ENTITY>>... providers) {
		this(Arrays.asList(providers));
	}
	
	public MultipleIterator(Collection<Iterable<ProvidedData<ENTITY>>> providers) {
		super(providers);
	}
}