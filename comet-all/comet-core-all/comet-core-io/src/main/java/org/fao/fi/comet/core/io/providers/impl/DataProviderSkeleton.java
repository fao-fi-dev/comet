/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 May 2013
 */
abstract public class DataProviderSkeleton<ENTITY> extends AbstractLoggingAwareClient implements DataProvider<ENTITY> {
	private String _providerID;

	/**
	 * Class constructor
	 *
	 */
	public DataProviderSkeleton() {
		super();

		this._providerID = ObjectsHelper.thi$(this);
	}

	/**
	 * Class constructor
	 *
	 * @param providerID
	 */
	public DataProviderSkeleton(String providerID) {
		super();

		this._providerID = $nN(providerID, "Provider ID cannot be null");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#getProviderID()
	 */
	@Override
	final public String getProviderID() {
		return this._providerID;
	}

	@Override
	final public void setProviderID(String providerID) {
		this._providerID = $nN(providerID, "Provider ID cannot be null");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#releaseResources()
	 */
	@Override
	final public void releaseResources() throws Exception {
		this._log.debug("Releasing resources for {} ({})...", this._providerID, this);

		long end, start = System.currentTimeMillis();

		try {
			this.doReleaseResources();
		} catch(Exception e) {
			this._log.warn("Unable to release resources for {} ({}): {} [ {} ]", this._providerID, this, e.getClass().getSimpleName(), e.getMessage());

			throw e;
		} finally {
			end = System.currentTimeMillis();

			this._log.debug("Releasing resources for {} ({}) took {} mSec.", this._providerID, this, end - start);
		}
	}

	abstract protected void doReleaseResources() throws Exception;
}