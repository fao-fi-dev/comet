/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
public class GenericMultipleIterator<ENTITY , ITERABLE extends Iterable<ENTITY>> implements Iterator<ENTITY> {
	private List<ITERABLE> _providers;
	private List<Iterator<ENTITY>> _iterators;
	
	private int _currentProviderIndex;
	
	@SafeVarargs public GenericMultipleIterator(ITERABLE... providers) {
		this(Arrays.asList(providers));
	}
	
	public GenericMultipleIterator(Collection<ITERABLE> providers) {
		this._providers = new ArrayList<ITERABLE>();
		this._providers.addAll(providers);
		
		this._iterators = new ArrayList<Iterator<ENTITY>>();
		
		for(Iterable<ENTITY> provider : this._providers)
			this._iterators.add(provider.iterator());
		
		this._currentProviderIndex = 0;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		for(int i=this._currentProviderIndex; i<this._providers.size(); i++)
			if(this._iterators.get(i).hasNext())
				return true;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public ENTITY next() {
		Iterator<ENTITY> currentIterator = this._iterators.get(this._currentProviderIndex);
		
		if(currentIterator.hasNext())
			return currentIterator.next();
		
		while(this._currentProviderIndex < this._providers.size()) {
			currentIterator = this._iterators.get(this._currentProviderIndex);
			
			if(currentIterator.hasNext())
				return currentIterator.next();
			
			this._currentProviderIndex++;
		}
		
		throw new NoSuchElementException();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		Iterator<ENTITY> iterator;
		if(this._currentProviderIndex < this._providers.size()) {
			iterator = this._iterators.get(this._currentProviderIndex);
			iterator.remove();
		} else 
			throw new IllegalStateException();
	}
}