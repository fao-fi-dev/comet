/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.basic;

import org.fao.fi.comet.core.io.providers.impl.multiple.MultipleFeedableSizeAwareDataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
public class MultipleFeedableSizeAwareDataProvider<ENTITY> extends MultipleFeedableSizeAwareDataProviderSkeleton<ENTITY> {
	/**
	 * Class constructor
	 *
	 * @param dataProviders
	 */
	@SafeVarargs public MultipleFeedableSizeAwareDataProvider(SizeAwareDataProvider<ENTITY>... dataProviders) {
		super(dataProviders);
	}

	/**
	 * Class constructor
	 *
	 * @param providerId
	 * @param dataProviders
	 */
	@SafeVarargs public MultipleFeedableSizeAwareDataProvider(String providerId, SizeAwareDataProvider<ENTITY>... dataProviders) {
		super(providerId, dataProviders);
	}
}