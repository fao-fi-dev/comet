/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.basic;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.FeedableSizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
public class CollectionBackedDataProvider<ENTITY> extends DataProviderSkeleton<ENTITY> implements FeedableSizeAwareDataProvider<ENTITY> {
	private Collection<ProvidedData<ENTITY>> _backingCollection;

	/**
	 * Class constructor
	 *
	 */
	public CollectionBackedDataProvider() {
		super();

		this._backingCollection = new ArrayList<ProvidedData<ENTITY>>();
	}

	/**
	 * Class constructor
	 *
	 * @param providerID
	 */
	public CollectionBackedDataProvider(String providerID) {
		super(providerID);

		this._backingCollection = new ArrayList<ProvidedData<ENTITY>>();
	}

	public CollectionBackedDataProvider(Collection<ENTITY> data) {
		super();

		$nN(data, "Data cannot be NULL");

		this.setData(data);
	}

	public CollectionBackedDataProvider(String providerID, Collection<ENTITY> data) {
		this(providerID);

		$nN(data, "Data cannot be NULL");

		this.setData(data);
	}

	public CollectionBackedDataProvider(String providerID, DataProvider<ENTITY> provider) {
		this(providerID);

		$nN(provider, "Data provider cannot be NULL");

		Collection<ENTITY> data = new ArrayList<ENTITY>();

		for(ProvidedData<ENTITY> current : provider) {
			data.add(current.getData());
		}

		this.setData(data);
	}

	public CollectionBackedDataProvider(DataProvider<ENTITY> provider) {
		this();

		$nN(provider, "Data provider cannot be NULL");

		this.setProviderID(provider.getProviderID());

		Collection<ENTITY> data = new ArrayList<ENTITY>();

		for(ProvidedData<ENTITY> current : provider) {
			data.add(current.getData());
		}

		this.setData(data);
	}

	final protected void setData(Collection<ENTITY> data) {
		$nN(data, "Data cannot be NULL");

		this._backingCollection = new ArrayList<ProvidedData<ENTITY>>();

		for(ENTITY currentData : data)
			this._backingCollection.add(new ProvidedData<ENTITY>(this.getProviderID(), currentData));
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<ENTITY>> iterator() {
		final CollectionBackedDataProvider<ENTITY> $this = this;

		return new Iterator<ProvidedData<ENTITY>>() {
			private final Iterator<ProvidedData<ENTITY>> _realIterator = $this._backingCollection.iterator();

			/* (non-Javadoc)
			 * @see java.util.Iterator#hasNext()
			 */
			@Override
			public boolean hasNext() {
				return this._realIterator.hasNext();
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#next()
			 */
			@Override
			public ProvidedData<ENTITY> next() {
				return this._realIterator.next();
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#remove()
			 */
			@Override
			public void remove() {
				this._realIterator.remove();
			}
		};
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider#addData(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	public void addData(ProvidedData<ENTITY> toAdd) {
		this._backingCollection.add(toAdd);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#getDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		return this._backingCollection.size();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	final protected void doReleaseResources() throws Exception {
//		this._backingCollection = null;
	}
}