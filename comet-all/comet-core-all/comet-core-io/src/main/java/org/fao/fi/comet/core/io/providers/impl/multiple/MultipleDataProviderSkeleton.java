/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.multiple;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nEm;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.io.providers.impl.support.MultipleDataProvidersIterator;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
abstract public class MultipleDataProviderSkeleton<ENTITY, PROVIDER extends DataProvider<ENTITY>> extends DataProviderSkeleton<ENTITY> {
	protected List<PROVIDER> _dataProviders;

	@SafeVarargs public MultipleDataProviderSkeleton(PROVIDER... dataProviders) {
		this(null, dataProviders);
	}

	@SafeVarargs public MultipleDataProviderSkeleton(String providerId, PROVIDER... dataProviders) {
		super();

		if(providerId != null)
			this.setProviderID(providerId);

		$nN(dataProviders, "Data providers cannot be null");
		$nEm(dataProviders, "Data providers cannot be empty");

		for(Iterable<ProvidedData<ENTITY>> provider : dataProviders)
			$nN(provider, "Cannot handle a NULL data provider");

		this._dataProviders = Arrays.asList(dataProviders);
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<ENTITY>> iterator() {
		return new MultipleDataProvidersIterator<ENTITY, PROVIDER>(this._dataProviders);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	protected void doReleaseResources() throws Exception {
		for(PROVIDER provider : this._dataProviders)
			provider.releaseResources();
	}
}