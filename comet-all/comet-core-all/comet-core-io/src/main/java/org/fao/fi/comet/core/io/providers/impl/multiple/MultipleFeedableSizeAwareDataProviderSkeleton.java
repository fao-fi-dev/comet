/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.multiple;

import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
abstract public class MultipleFeedableSizeAwareDataProviderSkeleton<ENTITY> extends MultipleFeedableDataProviderSkeleton<ENTITY, SizeAwareDataProvider<ENTITY>> implements SizeAwareDataProvider<ENTITY> {
	@SafeVarargs public MultipleFeedableSizeAwareDataProviderSkeleton(SizeAwareDataProvider<ENTITY>... dataProviders) {
		super(null, dataProviders);
	}
	
	@SafeVarargs public MultipleFeedableSizeAwareDataProviderSkeleton(String providerId, SizeAwareDataProvider<ENTITY>... dataProviders) {
		super(providerId, dataProviders);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider#getAvailableDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		int totalSize = 0;
		for(SizeAwareDataProvider<ENTITY> provider : this._dataProviders)
			totalSize += provider.getAvailableDataSize();
		
		totalSize += this._addedData.size();
		
		return totalSize;
	}
}