/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.support;

import java.util.Collection;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
public class MultipleDataProvidersIterator<ENTITY, PROVIDER extends DataProvider<ENTITY>> extends GenericMultipleIterator<ProvidedData<ENTITY>, PROVIDER> {
	/**
	 * Class constructor
	 *
	 * @param providers
	 */
	public MultipleDataProvidersIterator(Collection<PROVIDER> providers) {
		super(providers);
	}

	/**
	 * Class constructor
	 *
	 * @param providers
	 */
	@SafeVarargs public MultipleDataProvidersIterator(PROVIDER... providers) {
		super(providers);
	}	
}