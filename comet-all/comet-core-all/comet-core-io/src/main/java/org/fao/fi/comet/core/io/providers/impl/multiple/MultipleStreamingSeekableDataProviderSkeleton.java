/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.multiple;

import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingSeekableDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2013
 */
public class MultipleStreamingSeekableDataProviderSkeleton<ENTITY> extends MultipleDataProviderSkeleton<ENTITY, StreamingSeekableDataProvider<ENTITY>> implements SizeAwareDataProvider<ENTITY> {
	@SafeVarargs public MultipleStreamingSeekableDataProviderSkeleton(StreamingSeekableDataProvider<ENTITY>... dataProviders) {
		super(dataProviders);
	}

	public MultipleStreamingSeekableDataProviderSkeleton(String providerId, StreamingSeekableDataProvider<ENTITY> dataProviders) {
		this(dataProviders);

		this.setProviderID(providerId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider#getAvailableDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		int totalSize = 0;
		for(SizeAwareDataProvider<ENTITY> provider : this._dataProviders)
			totalSize += provider.getAvailableDataSize();

		return totalSize;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	protected void doReleaseResources() throws Exception {
		for(SizeAwareDataProvider<ENTITY> provider : this._dataProviders)
			provider.releaseResources();
	}
}
