/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.basic;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
public class ArrayBackedDataProvider<ENTITY> extends CollectionBackedDataProvider<ENTITY> {
	public ArrayBackedDataProvider() {
		super(new ArrayList<ENTITY>());
	}

	public ArrayBackedDataProvider(ENTITY[] data) {
		super(data == null ? null : Arrays.asList(data));
	}
	
	public ArrayBackedDataProvider(String providerID, ENTITY[] data) {
		super(providerID, data == null ? null : Arrays.asList(data));
	}
}