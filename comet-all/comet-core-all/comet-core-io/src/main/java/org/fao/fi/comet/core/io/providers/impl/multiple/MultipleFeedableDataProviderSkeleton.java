/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.multiple;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
abstract public class MultipleFeedableDataProviderSkeleton<ENTITY, PROVIDER extends DataProvider<ENTITY>> extends MultipleDataProviderSkeleton<ENTITY, PROVIDER> implements FeedableDataProvider<ENTITY> {
	protected Collection<ProvidedData<ENTITY>> _addedData;
	
	@SafeVarargs public MultipleFeedableDataProviderSkeleton(PROVIDER... dataProviders) {
		super(null, dataProviders);
		
		this._addedData = new ArrayList<ProvidedData<ENTITY>>();
	}
	
	@SafeVarargs public MultipleFeedableDataProviderSkeleton(String providerId, PROVIDER... dataProviders) {
		super(providerId, dataProviders);

		this._addedData = new ArrayList<ProvidedData<ENTITY>>();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider#addData(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	public void addData(ProvidedData<ENTITY> toAdd) {
		this._addedData.add(toAdd);
	}
}
