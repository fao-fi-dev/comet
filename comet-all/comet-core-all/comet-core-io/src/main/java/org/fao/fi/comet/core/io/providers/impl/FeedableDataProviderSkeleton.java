/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 24/mag/2013
 */
abstract public class FeedableDataProviderSkeleton<ENTITY> extends DataProviderSkeleton<ENTITY> implements FeedableDataProvider<ENTITY> {
	protected final Collection<ProvidedData<ENTITY>> _addedData;
	
	/**
	 * Class constructor
	 *
	 */
	public FeedableDataProviderSkeleton() {
		super();
		
		this._addedData = new ArrayList<ProvidedData<ENTITY>>();
	}

	/**
	 * Class constructor
	 *
	 * @param identifier
	 */
	public FeedableDataProviderSkeleton(String identifier) {
		super(identifier);
		
		this._addedData = new ArrayList<ProvidedData<ENTITY>>();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider#addData(java.io.Serializable)
	 */
	@Override
	final public void addData(ProvidedData<ENTITY> toAdd) {
		this._addedData.add(toAdd);
	}
}