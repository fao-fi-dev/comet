/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-io)
 */
package org.fao.fi.comet.core.io.providers.impl.basic;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$pos;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$true;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingSeekableDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
public class StreamableSeekableChunkedDelegateDataProvider<ENTITY> extends DataProviderSkeleton<ENTITY> implements SizeAwareDataProvider<ENTITY> {
	private final StreamingSeekableDataProvider<ENTITY> _backingProvider;

	private final int _from;
	private final int _to;

	/**
	 * Class constructor
	 *
	 * @param providerID
	 */
	public StreamableSeekableChunkedDelegateDataProvider(String providerID, StreamingSeekableDataProvider<ENTITY> backingProvider, int from, int to) {
		super(providerID);

		$nN(backingProvider, "Backing provider cannot be NULL");
		$pos(from, "Starting position must be greater than zero (currently: {})", from);
		$pos(to, "Ending position must be greater than zero (currently: {})", to);
		$true(from <= to, "Starting position ({}) must be greater than (or equal to) the ending position ({})", from, to);

		int dataSize = backingProvider.getAvailableDataSize();

		$true(to <= dataSize, "Ending position ({}) must be lower than or equal to the provider data size ({})", to, dataSize);

		this._from = from;
		this._to = to;
		this._backingProvider = backingProvider;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<ENTITY>> iterator() {
		final StreamableSeekableChunkedDelegateDataProvider<ENTITY> $this = this;

		return new Iterator<ProvidedData<ENTITY>>() {
			private final int _currentPos = $this._from;
			private final Iterator<ProvidedData<ENTITY>> _realIterator = $this._backingProvider.seek($this._from).iterator();


			/* (non-Javadoc)
			 * @see java.util.Iterator#hasNext()
			 */
			@Override
			public boolean hasNext() {
				return this._realIterator.hasNext() && this._currentPos < $this._to;
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#next()
			 */
			@Override
			public ProvidedData<ENTITY> next() {
				if(this.hasNext())
					return this._realIterator.next();

				throw new NoSuchElementException();
			}

			/* (non-Javadoc)
			 * @see java.util.Iterator#remove()
			 */
			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.DataProvider#getDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		return this._to - this._from + 1;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	final protected void doReleaseResources() throws Exception {
		this._backingProvider.releaseResources();
	}
}