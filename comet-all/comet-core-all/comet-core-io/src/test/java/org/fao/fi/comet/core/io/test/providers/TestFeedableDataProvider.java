/**
 * (c) 2013 FAO / UN (project: comparison-engine-core)
 */
package org.fao.fi.comet.core.io.test.providers;

import java.util.Arrays;
import java.util.ConcurrentModificationException;

import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.FeedableSizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 24/mag/2013
 */
public class TestFeedableDataProvider {

	@Test
	public void testFeedableDataProviderInitiallyEmpty() {
		SizeAwareDataProvider<String> sourcesProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "foo", "bar" }));
		FeedableSizeAwareDataProvider<String> provider = new CollectionBackedDataProvider<String>();
	
		for(ProvidedData<String> source : sourcesProvider)
			provider.addData(source);
	
		Assert.assertEquals(sourcesProvider.getAvailableDataSize(), provider.getAvailableDataSize());
	}

	@Test
	public void testFeedableDataProviderInitiallyNotEmpty() {
		SizeAwareDataProvider<String> sourcesProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "foo", "bar" }));
		FeedableSizeAwareDataProvider<String> provider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "baz" }));
	
		int initialSize = provider.getAvailableDataSize();
	
		for(ProvidedData<String> source : sourcesProvider)
			provider.addData(source);
	
		Assert.assertEquals(initialSize + sourcesProvider.getAvailableDataSize(), provider.getAvailableDataSize());
	}

	@Test(expected=ConcurrentModificationException.class)
	public void testConcurrentModification() {
		SizeAwareDataProvider<String> sourcesProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "foo", "bar" }));
		FeedableSizeAwareDataProvider<String> targetsProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "baz" }));
	
		for(@SuppressWarnings("unused") ProvidedData<String> target : targetsProvider)
			for(ProvidedData<String> source : sourcesProvider) {
				targetsProvider.addData(source);
			}
	}

	@Test
	public void testNoConcurrentModification() {
		SizeAwareDataProvider<String> sourcesProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "foo", "bar", "boo", "far", "snafu"}));
		FeedableSizeAwareDataProvider<String> targetsProvider = new CollectionBackedDataProvider<String>(Arrays.asList(new String[] { "baz", "zab" }));
	
		int initialSourceSize = sourcesProvider.getAvailableDataSize();
		int initialTargetSize = targetsProvider.getAvailableDataSize();
	
		for(ProvidedData<String> source : sourcesProvider) {
			for(ProvidedData<String> target : targetsProvider) {
				System.out.println("S: " + source + ", T: " + target);
			}
	
			targetsProvider.addData(source);
		}
	
		Assert.assertEquals(initialSourceSize + initialTargetSize, targetsProvider.getAvailableDataSize());	}
	
}
