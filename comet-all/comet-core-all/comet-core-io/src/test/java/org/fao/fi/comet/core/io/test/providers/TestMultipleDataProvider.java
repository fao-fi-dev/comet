/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.io.test.providers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.io.providers.impl.basic.MultipleSizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jun 2013
 */
public class TestMultipleDataProvider {
	private String[] FIRST_DATA = new String[] {
		"first1",
		"first2",
		"first3",
		"first4"
	};
	
	private String[] SECOND_DATA = new String[] {
	};

	private String[] THIRD_DATA = new String[] {
		"third1",
		"third2",
		"third3"
	};

	private DataProvider<String> getDataProvider() {
		SizeAwareDataProvider<String> first = new CollectionBackedDataProvider<String>("first", Arrays.asList(FIRST_DATA));
		SizeAwareDataProvider<String> second = new CollectionBackedDataProvider<String>("second", Arrays.asList(SECOND_DATA));
		SizeAwareDataProvider<String> third = new CollectionBackedDataProvider<String>("third", Arrays.asList(THIRD_DATA));
		
		return new MultipleSizeAwareDataProvider<String>(first, second, third);
	}
	
	@Test
	public void testSimpleIteration() {
		DataProvider<String> toTest = this.getDataProvider();
		
		Set<String> actual = new HashSet<String>();
		Set<String> expected = new HashSet<String>();
		
		for(String[] inputs : new String[][] { FIRST_DATA, SECOND_DATA, THIRD_DATA })
			for(String input : inputs)
				expected.add(input);
		
		for(ProvidedData<String> data : toTest) {
			actual.add(data.getData());
		}
		
		Assert.assertEquals(FIRST_DATA.length + SECOND_DATA.length + THIRD_DATA.length, actual.size());
		Assert.assertEquals(expected.size(), actual.size());
		Assert.assertTrue(actual.removeAll(expected));
		Assert.assertTrue(actual.isEmpty());
	}
	
	@Test
	public void testNestedIteration() {
		DataProvider<String> toTest = this.getDataProvider();
		
		Set<String> actual = new HashSet<String>();
		Set<String> expected = new HashSet<String>();
		
		for(String[] inputs : new String[][] { FIRST_DATA, SECOND_DATA, THIRD_DATA })
			for(String input : inputs)
				expected.add(input);
		
		for(ProvidedData<String> data : toTest) {
			actual.add(data.getData());
			
			for(ProvidedData<String> innerData : toTest) {
				System.out.println("Inner: " + innerData);
			}
		}
		
		Assert.assertEquals(FIRST_DATA.length + SECOND_DATA.length + THIRD_DATA.length, actual.size());
		Assert.assertEquals(expected.size(), actual.size());
		Assert.assertTrue(actual.removeAll(expected));
		Assert.assertTrue(actual.isEmpty());
	}
	
	@Test
	public void testRemoveIteration() {
		DataProvider<String> toTest = this.getDataProvider();
		
		Iterator<ProvidedData<String>> iter = toTest.iterator();
		
		while(iter.hasNext()) {
			System.out.println(iter.next());
			iter.remove();
		}
		
		int numElems = 0;
		
		for(@SuppressWarnings("unused") ProvidedData<String> data : toTest)
			numElems++;
		
		Assert.assertEquals(0, numElems);
	}
	
	@Test
	public void testProviderIDRetrievement() {
		for(ProvidedData<String> data : this.getDataProvider()) {
			Assert.assertTrue(data.getData().startsWith(data.getProviderId()));
		}
	}
}