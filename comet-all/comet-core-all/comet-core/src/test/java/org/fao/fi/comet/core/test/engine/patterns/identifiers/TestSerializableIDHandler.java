/**
 * (c) 2013 FAO / UN (project: comparison-engine-core)
 */
package org.fao.fi.comet.core.test.engine.patterns.identifiers;

import java.io.Serializable;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;
import org.fao.fi.comet.core.patterns.handlers.id.impl.basic.SerializableDataIDHandler;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * 
 */
public class TestSerializableIDHandler extends AbstractTestSerializableIDHandler {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.test.engine.patterns.identifiers.AbstractTestSerializableIDHandler#getHandler()
	 */
	@Override
	protected IDHandler<Serializable, Serializable> getHandler() {
		return new SerializableDataIDHandler<Serializable>();
	}
}
