/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine.patterns.identifiers;

import java.io.Serializable;
import java.util.Calendar;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * 
 */
abstract public class AbstractTestSerializableIDHandler {
	abstract protected IDHandler<Serializable, Serializable> getHandler();
	
	@Test
	public void testEqualStringsStringIDHandler() {
		String firstObject = "foo";
		String secondObject = "foo";

		Serializable ID = this.getHandler().getId("foo");
		Serializable firstID = this.getHandler().getId(firstObject);
		Serializable secondID = this.getHandler().getId(secondObject);

		Assert.assertEquals(ID, firstID);
		Assert.assertEquals(ID, secondID);
	}

	@Test
	public void testEqualObjectsEqualIDHandler() throws Throwable {
		Calendar object = Calendar.getInstance();

		Thread.sleep(100); //Ensures that the second calendar refers to a different timestamp

		Calendar firstObject = Calendar.getInstance();

		Thread.sleep(100); //Ensures that the third calendar refers to a different timestamp

		Calendar secondObject = Calendar.getInstance();

		//Sets the same timestamp on all the three calendars
		firstObject.setTime(object.getTime());
		secondObject.setTime(object.getTime());

		Assert.assertEquals(object, firstObject);
		Assert.assertEquals(object, secondObject);

		Assert.assertFalse(object == firstObject);
		Assert.assertFalse(object == secondObject);
		Assert.assertFalse(firstObject == secondObject);

		Serializable ID = this.getHandler().getId(object);
		Serializable firstID = this.getHandler().getId(firstObject);
		Serializable secondID = this.getHandler().getId(secondObject);

		Assert.assertEquals(ID, firstID);
		Assert.assertEquals(ID, secondID);
	}

	@Test
	public void testClonedObjectsEqualIDHandler() throws Throwable {
		Calendar object = Calendar.getInstance();

		Thread.sleep(100);

		Calendar cloned = SerializationHelper.clone(object);

		Serializable ID = this.getHandler().getId(object);
		Serializable firstID = this.getHandler().getId(cloned);

		Assert.assertEquals(ID,  firstID);
	}
}