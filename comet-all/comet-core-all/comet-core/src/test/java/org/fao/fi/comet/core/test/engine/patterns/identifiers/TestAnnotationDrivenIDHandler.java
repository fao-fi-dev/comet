/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine.patterns.identifiers;

import java.io.Serializable;

import org.fao.fi.comet.core.model.common.annotations.Identifier;
import org.fao.fi.comet.core.patterns.handlers.id.exceptions.MultipleIdentifierAnnotationsException;
import org.fao.fi.comet.core.patterns.handlers.id.impl.basic.AnnotationDrivenDataIDHandler;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 06/giu/2013   Fabio     Creation.
 *
 * @version 1.0
 * 
 */
public class TestAnnotationDrivenIDHandler {
	private class SingleAnnotated implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 6142937411974028025L;

		@Identifier
		private Integer _aField;

		@SuppressWarnings("unused")
		private Integer _bField;

		/**
		 * Class constructor
		 */
		public SingleAnnotated() {
			super();
		}
	}

	private class AnotherSingleAnnotated extends SingleAnnotated {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 6142937411974028025L;

		@Identifier
		private Integer _cField;

		/**
		 * Class constructor
		 */
		public AnotherSingleAnnotated() {
			super();
		}
	}

	private class MultipleAnnotated extends AnotherSingleAnnotated {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -7730644605043640334L;

		@Identifier
		private Integer _dField;

		@SuppressWarnings("unused")
		private Integer _eField;

		@Identifier
		private String _fField;

		/**
		 * Class constructor
		 *
		 */
		public MultipleAnnotated() {
			super();
		}
	}

	@Test(expected=MultipleIdentifierAnnotationsException.class)
	public void testMultipleAnnotationsOnClass() {
		new AnnotationDrivenDataIDHandler<Serializable>().getId(new MultipleAnnotated());
	}

	@Test
	public void testAnotherSingleAnnotationsOnClass() {
		new AnnotationDrivenDataIDHandler<Serializable>().getId(new AnotherSingleAnnotated());
	}

	@Test
	public void testSingleAnnotationsOnClass() {
		new AnnotationDrivenDataIDHandler<Serializable>().getId(new SingleAnnotated());
	}
}
