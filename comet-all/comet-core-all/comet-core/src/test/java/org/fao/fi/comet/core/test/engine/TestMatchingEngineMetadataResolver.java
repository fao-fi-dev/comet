/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import org.fao.fi.comet.core.engine.process.MatchingEngineMetadataResolver;
import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.matchlets.skeleton.VectorialMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * 
 */
public class TestMatchingEngineMetadataResolver {
	private class FakeGenericScalarMatchlet<SOURCE, TARGET> extends ScalarMatchletSkeleton<SOURCE, String, TARGET, Date> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public String extractSourceData(Object source, DataIdentifier sourceIdentifier) {
			return null;
		}

		@Override
		public Date extractTargetData(Object target, DataIdentifier targetIdentifier) {
			return null;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public MatchingScore computeScore(Object source, DataIdentifier sourceIdentifier, String sourceData, Object target, DataIdentifier targetIdentifier, Date targetData) {
			return null;
		}
	}

	private class FakeScalarMatchlet extends FakeGenericScalarMatchlet<Object, Object> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public String extractSourceData(Object source, DataIdentifier sourceIdentifier) {
			return null;
		}

		@Override
		public Date extractTargetData(Object target, DataIdentifier targetIdentifier) {
			return null;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public MatchingScore computeScore(Object source, DataIdentifier sourceIdentifier, String sourceData, Object target, DataIdentifier targetIdentifier, Date targetData) {
			return null;
		}
	}

	private class FakeVectorialMatchlet extends VectorialMatchletSkeleton<Object, String, Object, Date> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public Collection<String> extractSourceData(Object source, DataIdentifier sourceIdentifier) {
			return null;
		}

		@Override
		public Collection<Date> extractTargetData(Object target, DataIdentifier targetIdentifier) {
			return null;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public MatchingScore computeScore(Object source, DataIdentifier sourceIdentifier, String sourceData, Object target, DataIdentifier targetIdentifier, Date targetData) {
			return null;
		}
	}

	@Test
	public void testOnGenericScalarMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeGenericScalarMatchlet<Object, Serializable>());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(Date.class.getName(), info.getExtractedTargetDataType());
	}

	@Test
	public void testOnScalarMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeScalarMatchlet());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(Date.class.getName(), info.getExtractedTargetDataType());
	}

	@Test
	public void testOnVectorialMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeVectorialMatchlet());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(Date.class.getName(), info.getExtractedTargetDataType());
	}
}
