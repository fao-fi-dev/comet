/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.providers;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <ENTITY> the generic type
 * 
 */
public interface DataProvider<ENTITY> extends Iterable<ProvidedData<ENTITY>> {
	
	/**
	 * Gets the provider id.
	 *
	 * @return the provider id
	 */
	String getProviderID();
	
	/**
	 * Sets the provider id.
	 *
	 * @param providerID the new provider id
	 */
	void setProviderID(String providerID);

	/**
	 * Release resources.
	 *
	 * @throws Exception the exception
	 */
	void releaseResources() throws Exception;
}