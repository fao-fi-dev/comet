/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.matchlets.skeleton.behaviours;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;

import org.fao.fi.comet.core.model.score.support.MatchingScore;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <SOURCE_DATA> the generic type
 * @param <TARGET> the generic type
 * @param <TARGET_DATA> the generic type
 * 
 */
abstract public class BehaviourSkeleton<SOURCE,
										SOURCE_DATA,
										TARGET,
										TARGET_DATA> implements Serializable {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = 600756509504975112L;

	/**
	 * Update score.
	 *
	 * @param current the current
	 * @param source the source
	 * @param sourceData the source data
	 * @param target the target
	 * @param targetData the target data
	 * @return the matching score
	 */
	final public MatchingScore updateScore(MatchingScore current, SOURCE source, SOURCE_DATA sourceData, TARGET target, TARGET_DATA targetData) {
		$nN(current, "Current score cannot be null");
		$gte(current.getValue(), MatchingScore.SCORE_NO_MATCH, "Current score value ({}) cannot be lower than the minimum allowed ({})", current.getValue(), MatchingScore.SCORE_NO_MATCH);
		$lte(current.getValue(), MatchingScore.SCORE_FULL_MATCH, "Current score value ({}) cannot be higher than the maximum allowed ({})", current.getValue(), MatchingScore.SCORE_FULL_MATCH);

		MatchingScore updated = new MatchingScore(current);

		if(this.continueComparison(current)) {
			updated = this.doUpdateScore(current, sourceData, targetData);
		}

		$gte(updated.getValue(), MatchingScore.SCORE_NO_MATCH, "Updated score ({}) cannot be lower than the minimum allowed ({})", updated.getValue(), MatchingScore.SCORE_NO_MATCH);
		$lte(updated.getValue(), MatchingScore.SCORE_FULL_MATCH, "Updated score ({}) cannot be higher than the maximum allowed ({})", updated.getValue(), MatchingScore.SCORE_FULL_MATCH);

		return updated;
	}

	/**
	 * Do update score.
	 *
	 * @param current the current
	 * @param sourceData the source data
	 * @param targetData the target data
	 * @return the matching score
	 */
	abstract protected MatchingScore doUpdateScore(MatchingScore current, SOURCE_DATA sourceData, TARGET_DATA targetData);

	/**
	 * Continue comparison.
	 *
	 * @param score the score
	 * @return true, if successful
	 */
	final protected boolean continueComparison(MatchingScore score) {
		$nN(score, "The provided score cannot be null");

		return score.isNonAuthoritative() && !score.isNoMatch();
	}
}