/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.fao.fi.comet.core.engine.AbstractMatchingEngineCore;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;

/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jan 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic source type
 * @param <TARGET> the generic target type
 * @param <CONFIG> the generic configuration type
 */
public class AtomicComparisonProcess<SOURCE extends Serializable,
									 TARGET extends Serializable,
									 CONFIG extends MatchingEngineProcessConfiguration> implements Callable<Matching<SOURCE, TARGET>> {
	
	private AbstractMatchingEngineCore<SOURCE, TARGET, CONFIG> _engine;

	private MatchingsData<SOURCE, TARGET> _currentMatchingsData;
	
	private Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> _matchlets;
	
	private CONFIG _conf;
	
	private MatchingProcessHandler<SOURCE, TARGET> _tracker;
	
	private ProvidedData<SOURCE> _source;
	private IDHandler<SOURCE, ?> _sourceIDHandler;

	private String _targetProviderId;
	private TARGET _target;
	private IDHandler<TARGET, ?> _targetIDHandler;

	/**
	 * Class constructor.
	 *
	 * @param engine the engine
	 * @param currentMatchingsData the current matchings data
	 * @param matchlets the matchlets
	 * @param conf the conf
	 * @param tracker the tracker
	 * @param source the source
	 * @param target the target
	 * @param targetProviderId the target provider id
	 * @param sourceIDHandler the source id handler
	 * @param targetIDHandler the target id handler
	 */
	public AtomicComparisonProcess(AbstractMatchingEngineCore<SOURCE, TARGET, CONFIG> engine,
								   MatchingsData<SOURCE, TARGET> currentMatchingsData,
								   Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets,
								   CONFIG conf,
								   MatchingProcessHandler<SOURCE, TARGET> tracker,
								   ProvidedData<SOURCE> source,
								   TARGET target,
								   String targetProviderId,
								   IDHandler<SOURCE, ?> sourceIDHandler,
								   IDHandler<TARGET, ?> targetIDHandler) {
		super();

		this._engine = engine;
		this._currentMatchingsData = currentMatchingsData;
		this._matchlets = matchlets;
		this._conf = conf;
		this._tracker = tracker;
		this._source = source;
		this._target = target;
		this._targetProviderId = targetProviderId;
		this._sourceIDHandler = sourceIDHandler;
		this._targetIDHandler = targetIDHandler;
	}

	/**
	 * Gets the source.
	 *
	 * @return the 'source' value
	 */
	public ProvidedData<SOURCE> getSource() {
		return this._source;
	}

	/**
	 * Gets the target.
	 *
	 * @return the 'target' value
	 */
	public TARGET getTarget() {
		return this._target;
	}

	/**
	 * Sets the source.
	 *
	 * @param source the 'source' value to set
	 */
	public void setSource(ProvidedData<SOURCE> source) {
		this._source = source;
	}

	/**
	 * Sets the target.
	 *
	 * @param target the 'target' value to set
	 */
	public void setTarget(TARGET target) {
		this._target = target;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Matching<SOURCE, TARGET> call() throws InterruptedException, ExecutionException {
		if(!this._tracker.getProcessStatus().is(MatchingEngineStatus.RUNNING))
			throw new InterruptedException();

		DataIdentifier sourceIdentifier = new DataIdentifier(this._source.getProviderId(), this._sourceIDHandler.getSerializedId(this._source.getData()));
		DataIdentifier targetIdentifier = new DataIdentifier(this._targetProviderId, this._targetIDHandler.getSerializedId(this._target));

		Matching<SOURCE, TARGET> matching;

		try {
			matching = this._engine.performAtomicComparison(this._conf,
															this._tracker,
															this._currentMatchingsData,
															this._source.getData(),
															this._target,
															sourceIdentifier,
															targetIdentifier,
															this._matchlets);

			return matching;
		} catch(Exception e) {
			throw new ExecutionException(e);
		} finally {
			this._engine = null;
			this._conf = null;
			this._tracker = null;
			this._currentMatchingsData = null;
			this._source = null;
			this._target = null;
			this._targetProviderId = null;
			this._sourceIDHandler = null;
			this._targetIDHandler = null;
			this._matchlets = null;
		}
	}
}
