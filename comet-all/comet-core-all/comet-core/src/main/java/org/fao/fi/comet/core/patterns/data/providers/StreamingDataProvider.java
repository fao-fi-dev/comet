/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.providers;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <ENTITY> the generic type
 * 
 */
public interface StreamingDataProvider<ENTITY> extends DataProvider<ENTITY> {
	
	/**
	 * Checks if is rewindable.
	 *
	 * @return true, if is rewindable
	 */
	boolean isRewindable();
	
	/**
	 * Rewind.
	 */
	void rewind();
}
