/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ExecutionException;

import org.fao.fi.comet.core.engine.process.AtomicComparisonProcess;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider;
import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;

// TODO: Auto-generated Javadoc
/**
 * The generic matching engine
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * @param <CONFIG> the generic type
 * 
 */
abstract public class SingleThreadMatchingEngineCore<SOURCE extends Serializable,
											TARGET extends Serializable,
											CONFIG extends MatchingEngineProcessConfiguration> extends AbstractMatchingEngineCore<SOURCE, TARGET, CONFIG> {
	
	/**
	 * Class constructor.
	 */
	public SingleThreadMatchingEngineCore() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.AbstractMatchingEngineCore#afterCompletionCallback()
	 */
	@Override
	protected void afterCompletionCallback() {
		//DO NOTHING
	}
	
	/**
	 * Compares a data source with the current domain.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param currentMatchingsData the current matchings data
	 * @param sources the sources
	 * @param target the target
	 * @param targetProviderId the target provider id
	 * @param partitioner the partitioner
	 * @param matchlets the matchlets
	 * @param sourceIDHandler the source id handler
	 * @param targetIDHandler the target id handler
	 * @return the matchings data
	 */
	final protected MatchingsData<SOURCE, TARGET> performComparison(CONFIG configuration,
																	MatchingProcessHandler<SOURCE, TARGET> tracker,
																	MatchingsData<SOURCE, TARGET> currentMatchingsData,
																	DataProvider<SOURCE> sources,
																	TARGET target,
																	String targetProviderId,
																	DataPartitioner<SOURCE, TARGET> partitioner,
																	Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets,
																	IDHandler<SOURCE, ?> sourceIDHandler,
																	IDHandler<TARGET, ?> targetIDHandler) {
		Matching<SOURCE, TARGET> matching = null;

		//Lifecycle management
		if(sources instanceof SizeAwareDataProvider)
			tracker.setMaximumNumberOfAtomicComparisonsPerformedInRound(((SizeAwareDataProvider<SOURCE>)sources).getAvailableDataSize());

		boolean hasMatched = false;

		final DataIdentifier targetIdentifier = new DataIdentifier(targetProviderId, targetIDHandler.getSerializedId(target));
		DataIdentifier sourceIdentifier;

		final int maxCandidatesPerEntry = configuration.getMaxCandidatesPerEntry();
		final boolean allCandidatesPerEntry = maxCandidatesPerEntry == MatchingEngineProcessConfiguration.MAX_CANDIDATES_PER_ENTRY_UNBOUND;

		final boolean haltAtFirstMatching = configuration.getHaltAtFirstValidMatching() == null ? Boolean.FALSE : configuration.getHaltAtFirstValidMatching();

		final boolean handleErrors = configuration.getHandleErrors();

		AtomicComparisonProcess<SOURCE, TARGET, CONFIG> process = null;
		
		//Cycle on each current target entity
		for(ProvidedData<SOURCE> source : sources) {
			if(partitioner.include(source.getData(), target, matchlets)) {
				sourceIdentifier = new DataIdentifier(source.getProviderId(), sourceIDHandler.getSerializedId(source.getData()));

				process = new AtomicComparisonProcess<SOURCE, TARGET, CONFIG>(this,
																			  currentMatchingsData,
																			  matchlets,
																			  configuration,
																			  tracker,
																			  source,
																			  target,
																			  targetProviderId,
																			  sourceIDHandler,
																			  targetIDHandler);
				try {
					matching = process.call();

					if(matching != null) {
						sourceIdentifier = matching.getSourceIdentifier();

						if(!matching.isNonPerformed() && Double.compare(matching.getScore().getValue(), configuration.getMinimumAllowedWeightedScore()) >= 0) {
							//Lifecycle management
							tracker.notifyMatch();

							hasMatched = true;

							MatchingDetails<SOURCE, TARGET> matchingDetails = currentMatchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);

							if(matchingDetails != null) {
								if(!allCandidatesPerEntry)
									matchingDetails = matchingDetails.retain(maxCandidatesPerEntry, currentMatchingsData.MATCHINGS_COMPARATOR);

								if(matchingDetails.getMatchings() == null || matchingDetails.getMatchings().isEmpty() )
									currentMatchingsData.removeMatchingDetailsBySourceIdentifier(sourceIdentifier);
							}
						} else {
							this.doRemoveMatching(currentMatchingsData, sourceIdentifier, targetIdentifier);
						}
						
						tracker.updateCurrentResults(currentMatchingsData);
					} else {
						//NULL matchings are returned when the matching score is lower than
						//the engine currently set threshold or when the source - target partitioning excludes the comparison
					}

					//Exits the domain cycle if a matching has been detected and the engine is configured to halt at first matching
					if(hasMatched && haltAtFirstMatching)
						break;
				} catch(ExecutionException Ee) {
					if(handleErrors)
						tracker.notifyComparisonError();
					else if(!InterruptedException.class.equals(Ee.getCause().getClass())) {
						throw new RuntimeException(Ee);
					}
				} catch(InterruptedException Ie) {
					break;
				}
			} else {
				tracker.notifyAtomicComparisonSkipped();
				tracker.notifyAtomicComparisonPerformed();
			}
		}

		if(sources instanceof StreamingDataProvider) {
			StreamingDataProvider<SOURCE> stream = (StreamingDataProvider<SOURCE>)sources;

			if(stream.isRewindable())
				stream.rewind();
			
		}

		return currentMatchingsData;
	}
}