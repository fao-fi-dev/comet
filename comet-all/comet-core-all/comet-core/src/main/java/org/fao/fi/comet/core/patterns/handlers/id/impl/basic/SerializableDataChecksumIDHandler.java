/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.impl.basic;

import java.io.Serializable;
import java.security.GeneralSecurityException;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * 
 */
public class SerializableDataChecksumIDHandler<DATA extends Serializable> extends SerializableDataIDHandler<DATA> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.io.Serializable)
	 */
	@Override
	final protected Serializable doGetId(DATA data) {
		try {
			return MD5Helper.digest(SerializationHelper.serialize((Serializable)data, true));
		} catch(GeneralSecurityException GSe) {
			throw new UnsupportedOperationException("Unable to compute checksum (as ID) for " + data, GSe);
		}
	}
}