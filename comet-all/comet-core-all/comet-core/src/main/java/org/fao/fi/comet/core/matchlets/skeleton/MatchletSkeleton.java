/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.matchlets.skeleton;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$em;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$pos;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingResultData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletForcesComparisonByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletMetadata;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConverter;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <SOURCE_DATA> the generic type
 * @param <TARGET> the generic type
 * @param <TARGET_DATA> the generic type
 * 
 */
abstract public class MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> implements Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = -1158509945093513149L;

	/** The Constant MANDATORY_FIELDS_MAP. */
	final private static Map<Class<?>, Set<String>> MANDATORY_FIELDS_MAP = new HashMap<Class<?>, Set<String>>();

	/** The _id. */
	@MatchletMetadata
	protected String _id;

	/** The _name. */
	@MatchletMetadata
	protected String _name;

	/** The _type. */
	@MatchletMetadata
	protected String _type;

	/**  The matchlet's weight: defaults to 1. */
	@MatchletParameter(name=WEIGHT_PARAM,
					   description="Sets the matchlet weight that will be used to calculate this' matchlet result relative contribution to the overall matching score for a given input / reference data pair. " +
					  			   "It is effective IFF more than one matchlet is chained together in the matching process. Its value must be strictly positive.")
	@DoubleRangeFrom(value=0D, include=false)
	protected double _weight = 1D;

	/** The _minimum score. */
	@MatchletParameter(name=MIN_SCORE_PARAM,
					   description="Sets the matchlet minimum score threshold. Results (as yield by this' matchlet) with a score lower than the set threshold will be treated as NO MATCHes. " +
					  			   "Its value must be in the range [0.0, 1.0].")
	@DoubleRange(from=MatchingScore.SCORE_NO_MATCH,
				 to=MatchingScore.SCORE_FULL_MATCH,
				 includeFrom=false,
				 includeTo=true)
	protected double _minimumScore = MatchingScore.SCORE_NO_MATCH;

	/** The _is cutoff. */
	@MatchletParameter(name=CUTOFF_PARAM,
					   description="Tells whether this matchlet is capable of returning authoritative matches (either NO MATCH or FULL MATCH) or not. " +
					  			   "If set to true, the matchlet will be applied as soon as possible, to ensure that as less comparisons as possible will be performed.")
	protected boolean _isCutoff;

	/** The _is optional. */
	@MatchletParameter(name=OPTIONAL_PARAM,
					   description="Tells whether this matchlet is optional or not. Optional matchlets returning a matching score of NON PERFORMED type will not contribute " +
			  					   "to the overall matching result for a given input / reference data pair.")
	protected boolean _isOptional;

	/** The _force comparison. */
	@MatchletParameter(name=FORCE_COMPARISON_PARAM,
					   description="Tells whether this matchlet will force data comparison even if one of the data being extracted from the compared entities is currently NULL.")
	protected boolean _forceComparison;

	/** The _exclusion cases. */
	protected MatchingSerializationExclusionPolicy[] _exclusionCases;

	/**  The abstract behaviour. */
	@MatchletMetadata
	protected BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> _behaviour;

	/**
	 * Instantiates a new matchlet skeleton.
	 */
	public MatchletSkeleton() {
		this(new BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>());
	}

	/**
	 * Class constructor.
	 *
	 * @param behaviour the behaviour
	 */
	public MatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super();

		$nN(behaviour, "{} behaviour cannot be null", this.getClass().getName());

		this._behaviour = behaviour;

		//Sets default attributes based on concrete matchlet's class annotations...

		//Exclusion cases
		if(this.getClass().isAnnotationPresent(MatchletDefaultSerializationExclusionPolicy.class)) {
			MatchingSerializationExclusionPolicy[] exclusionCases = this.getClass().getAnnotation(MatchletDefaultSerializationExclusionPolicy.class).value();

			if(exclusionCases != null && exclusionCases.length > 0)
				this.setExclusionCases(exclusionCases);
		}

		//MatchletIsCutoffByDefault flag
		this.setCutoff(this.getClass().isAnnotationPresent(MatchletIsCutoffByDefault.class));

		//MatchletIsOptionalByDefault flag
		this.setOptional(this.getClass().isAnnotationPresent(MatchletIsOptionalByDefault.class));

		//Force Comparison flag
		this.setForceComparison(this.getClass().isAnnotationPresent(MatchletForcesComparisonByDefault.class));

		this._type = this.getClass().getSimpleName();
		this._name = this._type;
		this._id = this._type;
	}

	/**
	 * Gets the id.
	 *
	 * @return the 'id' value
	 */
	@Override
	public String getId() {
		return this._id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the 'id' value to set
	 */
	@Override
	public void setId(String id) {
		this._id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the 'name' value
	 */
	@Override
	final public String getName() {
		return this._name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the 'name' value to set
	 */
	final public void setName(String name) {
		this._name = name;
	}

	/**
	 * Gets the type.
	 *
	 * @return the 'type' value
	 */
	@Override
	public String getType() {
		return this._type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the 'type' value to set
	 */
	public void setType(String type) {
		this._type = type;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#configure(java.util.Collection)
	 */
	@Override
	final public void configure(Collection<MatchletConfigurationParameter> parameters) throws MatchletConfigurationException {
		final Class<?> key = this.getClass();

		Set<String> mandatoryFields = new HashSet<String>();

		if(MANDATORY_FIELDS_MAP.containsKey(key)) {
			mandatoryFields = MANDATORY_FIELDS_MAP.get(this.getClass());
		} else {
			MatchletParameter annotation;
			String name;
			for(Field field : ObjectsHelper.getAllAnnotatedFields(this, MatchletParameter.class)) {
				field.setAccessible(true);

				annotation = field.getAnnotation(MatchletParameter.class);
				name = annotation.name();

				if(name == null || "".equals(name))
					name = field.getName();

				if(annotation.mandatory()) {
					mandatoryFields.add(name);
				}
			}

			MANDATORY_FIELDS_MAP.put(key, mandatoryFields);
		}

		try {
			mandatoryFields = new HashSet<String>(mandatoryFields);

			if(parameters != null) {
				for(MatchletConfigurationParameter parameter : parameters) {
					$nN(parameter, "Invalid NULL matchlet configuration parameter for {}", this.getName());

					mandatoryFields.remove(parameter.getName());
					
					this.setParameter(parameter);
				}
			}

			$em(mandatoryFields, "{} mandatory parameters ({}) have not been provided as part of {} configuration", mandatoryFields.size(), CollectionsHelper.join(mandatoryFields, ", "), this.getName());
		} catch(IllegalArgumentException IAe) {
			throw new MatchletConfigurationException(IAe.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#validateConfiguration()
	 */
	@Override
	final public void validateConfiguration() throws MatchletConfigurationException {
		$pos(this._weight, "{} weight must be greater than zero (currently: {})",
						   this.getName(),
						   this._weight);

		$gte(this._minimumScore, MatchingScore.SCORE_NO_MATCH,
			 "{} minimum allowed score must be greater than (or equal to) {} (currently: {})",
			 this.getName(),
			 MatchingScore.SCORE_NO_MATCH,
			 this._minimumScore);

		$lte(this._minimumScore, MatchingScore.SCORE_FULL_MATCH,
			 "{} minimum allowed score must be lower than (or equal to) {} (currently: {})",
			 this.getName(),
			 MatchingScore.SCORE_FULL_MATCH,
			 this._minimumScore);

		this.doValidateConfiguration();
	}

	/**
	 * Do validate configuration.
	 *
	 * @throws MatchletConfigurationException the matchlet configuration exception
	 */
	abstract protected void doValidateConfiguration() throws MatchletConfigurationException;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getConfiguration()
	 */
	@Override
	final public Collection<MatchletConfigurationParameter> getConfiguration() throws MatchletConfigurationException {
		Collection<MatchletConfigurationParameter> parameters = new ArrayList<MatchletConfigurationParameter>();

		try {
			MatchletParameter annotation;

			boolean isTransient = false;
			
			for(Field field : ObjectsHelper.getAllAnnotatedFields(this, MatchletParameter.class)) {
				field.setAccessible(true);

				annotation = field.getAnnotation(MatchletParameter.class);

				isTransient = (field.getModifiers() & Modifier.TRANSIENT) == Modifier.TRANSIENT; 
				
				MatchletConfigurationParameter parameter = new MatchletConfigurationParameter();
				parameter.setName(annotation.name());
//				parameter.setValue(isTransient ? field.get(this) : this.getParameterValue(field));
				parameter.setValue(this.getConvertedParameterValue(field));
				parameter.setTransient(isTransient);
				
				parameters.add(parameter);
			}
		} catch (Throwable t) {
			throw new MatchletConfigurationException("Unable to retrieve configuration parameters for " + this.getName() + " [ " + t.getClass().getSimpleName() + ": " + t.getMessage() + " ]");
		}

		return parameters;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setExclusionCases(org.fao.fi.comet.core.matchlets.annotations.MatchingSerializationExclusionPolicy[])
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setExclusionCases(MatchingSerializationExclusionPolicy... exclusionCases) {
		this._exclusionCases = exclusionCases;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#getExclusionCases()
	 */
	@Override
	final public MatchingSerializationExclusionPolicy[] getExclusionCases() {
		return this._exclusionCases;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#forceComparison()
	 */
	@Override
	final public boolean forceComparison() {
		return this._forceComparison;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setForceComparison(boolean)
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setForceComparison(boolean forceComparison) {
		this._forceComparison = forceComparison;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#isOptional()
	 */
	@Override
	final public boolean isOptional() {
		return this._isOptional;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setOptional(boolean)
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setOptional(boolean optional) {
		this._isOptional = optional;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#isCutoff()
	 */
	@Override
	final public boolean isCutoff() {
		return this._isCutoff;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setCutoff(boolean)
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setCutoff(boolean cutoff) {
		this._isCutoff = cutoff;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setWeight(double)
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setWeight(double weight) {
		assert Double.compare(weight, 0) > 0 : "The matchlet's weight cannot be lower than or equal to zero";

		this._weight = weight;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#getWeight()
	 */
	@Override
	final public double getWeight() {
		return this._weight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#setMinimumAllowedScore(double)
	 */
	@Override
	final public Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setMinimumAllowedScore(double minimumScore) {
		assert Double.compare(minimumScore, MatchingScore.SCORE_NO_MATCH) >= 0 : "The minimum allowed score cannot be lower than " + MatchingScore.SCORE_NO_MATCH;
		assert Double.compare(minimumScore, MatchingScore.SCORE_FULL_MATCH) <= 0 : "The minimum allowed score cannot be greater than " + MatchingScore.SCORE_FULL_MATCH;

		this._minimumScore = minimumScore;

		return this;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#getMinimumAllowedScore()
	 */
	@Override
	final public Double getMinimumScore() {
		return this._minimumScore;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#isScoreAllowed(org.fao.fi.comet.core.engine.core.MatchingScore)
	 */
	@Override
	final public boolean isScoreAllowed(MatchingScore score) {
		assert null != score : "The score cannot be null";

		return score.isAuthoritative() ||
			   score.isNonPerformed() ||
			   Double.compare(score.getValue(), this._minimumScore) >= 0;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#performMatching(org.fao.vrmf.core.behaviours.data.Identifiable, org.fao.vrmf.core.behaviours.data.Identifiable)
	 */
	@Override
	final public MatchingResult<SOURCE_DATA, TARGET_DATA> performMatching(SOURCE sourceEntity, DataIdentifier sourceIdentifier, TARGET targetEntity, DataIdentifier targetIdentifier) {
		$nN(sourceEntity, "Source entity cannot be null");
		$nN(sourceIdentifier, "Source entity identifier cannot be null");
		$nN(targetEntity, "Target entity cannot be null");
		$nN(targetIdentifier, "Target entity identifier cannot be null");

		MatchingResult<SOURCE_DATA, TARGET_DATA> result = this.compareData(sourceEntity, sourceIdentifier, targetEntity, targetIdentifier);

		if(!this.isScoreAllowed(result.getScore()))
			result.setScore(MatchingScore.getNonAuthoritativeNoMatchTemplate());

		return result;
	}

	/**
	 * Compare data.
	 *
	 * @param sourceEntity the first (source) data source
	 * @param sourceIdentifier the source identifier
	 * @param targetEntity the second (target) data source
	 * @param targetIdentifier the target identifier
	 * @return the matching result between the source and target data source
	 */
	abstract public MatchingResult<SOURCE_DATA, TARGET_DATA> compareData(SOURCE sourceEntity, DataIdentifier sourceIdentifier, TARGET targetEntity, DataIdentifier targetIdentifier);

	/**
	 * Update result.
	 *
	 * @param result a matching result to update
	 * @param score the score to set for the result
	 * @param sourceData the source data
	 * @param sourceIdentifier the source identifier
	 * @param targetData the target data collection
	 * @return the updated matching result
	 */
	final protected MatchingResult<SOURCE_DATA, TARGET_DATA> updateResult(MatchingResult<SOURCE_DATA, TARGET_DATA> result, MatchingScore score, SOURCE_DATA sourceData, DataIdentifier sourceIdentifier, Collection<TARGET_DATA> targetData) {
		Collection<MatchingResultData<SOURCE_DATA, TARGET_DATA>> matchings = new ArrayList<MatchingResultData<SOURCE_DATA, TARGET_DATA>>();
		matchings.add(new MatchingResultData<SOURCE_DATA, TARGET_DATA>(score, sourceData, sourceIdentifier, targetData));

		result.setScore(score);
//		result.setDataMatchings(score.isNoMatch() ? new ArrayList<MatchingResultData<D>>() : matchings);
		result.setMatchingsResultData(matchings);

		return result;
	}

	/**
	 * Update result.
	 *
	 * @param result a matching result to update
	 * @param score the score to set for the result
	 * @param sourceData the source data
	 * @param sourceIdentifier the source identifier
	 * @param targetData the target data
	 * @return the updated matching result
	 */
	final protected MatchingResult<SOURCE_DATA, TARGET_DATA> updateResult(MatchingResult<SOURCE_DATA, TARGET_DATA> result, MatchingScore score, SOURCE_DATA sourceData, DataIdentifier sourceIdentifier, TARGET_DATA targetData) {
		return this.updateResult(result, score, sourceData, sourceIdentifier, this.convertToCollection(targetData));
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	final public int compareTo(Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> other) {
		if(this.isCutoff())
			return Integer.MIN_VALUE;

		return Double.compare(other.getWeight(), this._weight);
	}

	/**
	 * Convert to collection.
	 *
	 * @param <DATA> the generic type
	 * @param data a data to convert
	 * @return a collection holding the provided data only
	 */
	final protected <DATA> Collection<DATA> convertToCollection(DATA data) {
		Collection<DATA> toReturn = new ArrayList<DATA>();
		toReturn.add(data);

		return toReturn;
	}

	/**
	 * Sets the parameter.
	 *
	 * @param parameter the new parameter
	 * @throws MatchletConfigurationException the matchlet configuration exception
	 */
	private void setParameter(MatchletConfigurationParameter parameter) throws MatchletConfigurationException {
		$nN(parameter, "Cannot set a NULL configuration parameter on {}", this.getName());
		$nN(StringsHelper.trim(parameter.getName()), "Cannot set a no-name configuration parameter on {}", this.getName());

		final String parameterName = parameter.getName();

		Field toSet = null;
		MatchletParameter annotation;

		for(Field field : ObjectsHelper.getAllAnnotatedFields(this, MatchletParameter.class)) {
			field.setAccessible(true);

			annotation = field.getAnnotation(MatchletParameter.class);

			if(parameterName.equals(annotation.name())) {
				toSet = field;

				break;
			}
		}

		$nN(toSet, "No configuration parameter named {} found on {}", parameterName, this.getName());

		try {
			if(!parameter.isTransient())
				this.convertAndSetParameterValue(toSet, parameter.getValue());
			else
				toSet.set(this, parameter.getActualValue());
		} catch (Throwable t) {
			throw new MatchletConfigurationException("Unable to set configuration parameter " + parameter.getName() + " (valued " + parameter.getValue() + ") on " + this.getName() + " [ " + t.getClass().getSimpleName() + ": " + t.getMessage() + " ]");
		}
	}

	/**
	 * Convert and set parameter value.
	 *
	 * @param field the field
	 * @param value the value
	 * @throws Throwable the throwable
	 */
	private void convertAndSetParameterValue(Field field, Object value) throws Throwable {
		if(value == null)
			field.set(this, null);

		MatchletParameter annotation = field.getAnnotation(MatchletParameter.class);
		MatchletParameterConverter converter = annotation.converter().newInstance();

		if((field.getModifiers() & Modifier.TRANSIENT) != Modifier.TRANSIENT) {
			if(field.getType().isArray())
				field.set(this, converter.convertToArrayFromString(field.getType().getComponentType(), (String)value));
			else
				field.set(this, converter.convertFromString(field.getType(), (String)value));
		} else
			field.set(this, value);
	}

	/**
	 * Gets the converted parameter value.
	 *
	 * @param field the field
	 * @return the converted parameter value
	 * @throws Throwable the throwable
	 */
	private String getConvertedParameterValue(Field field) throws Throwable {
		Object value = field.get(this);

		if(value == null)
			return null;

		MatchletParameter annotation = field.getAnnotation(MatchletParameter.class);
		MatchletParameterConverter converter = annotation.converter().newInstance();

		return converter.convertToString(value);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#newMatchingResult()
	 */
	@Override
	public MatchingResult<SOURCE_DATA, TARGET_DATA> newMatchingResult() {
		MatchingResult<SOURCE_DATA, TARGET_DATA> result = new MatchingResult<SOURCE_DATA, TARGET_DATA>();
		result.setOriginatingMatchletId(this._id);
		result.setOriginatingMatchletName(this._name);
		result.setOriginatingMatchletWeight(this._weight);
		result.setOriginatingMatchletExclusionPolicies(this._exclusionCases);

		return result;
	}
}