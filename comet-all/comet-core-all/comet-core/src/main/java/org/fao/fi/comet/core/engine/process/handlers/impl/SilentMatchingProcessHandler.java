/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers.impl;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

// TODO: Auto-generated Javadoc
/**
 * A
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * 
 */
public class SilentMatchingProcessHandler<SOURCE extends Serializable, TARGET extends Serializable> extends MatchingProcessHandlerSkeleton<SOURCE, TARGET> {
	
	/** The _results. */
	@XmlTransient
	protected MatchingsData<SOURCE, TARGET> _results;
	
	/** The _status. */
	@XmlTransient
	protected MatchingEngineStatus _status = MatchingEngineStatus.IDLE;
	


	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#updateCurrentResults(org.fao.fi.comet.core.model.engine.MatchingsData)
	 */
	@Override
	public void updateCurrentResults(MatchingsData<SOURCE, TARGET> results) {
		this._results = results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getCurrentResults()
	 */
	@Override
	public MatchingsData<SOURCE, TARGET> getCurrentResults() {
		return this._results;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyProcessStartEvent(java.lang.String)
	 */
	@Override
	protected void doNotifyProcessStartEvent(String processId) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyProcessEndEvent()
	 */
	@Override
	protected void doNotifyProcessEndEvent() {
//		this.setRunning(false);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyNumberOfComparisonRoundsChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfComparisonRoundsChanged(int rounds) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyComparisonRoundStart(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundStart(ProvidedData<TARGET> source) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyComparisonRoundPerformed(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundPerformed(ProvidedData<TARGET> source) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int)
	 */
	@Override
	protected void doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int comparisons) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyAtomicComparisonStart()
	 */
	@Override
	protected void doNotifyAtomicComparisonStart() {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyTotalNumberOfAtomicComparisonsChanged(int)
	 */
	@Override
	protected void doNotifyTotalNumberOfAtomicComparisonsChanged(int atomicComparisons) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyAtomicComparisonSkipped()
	 */
	@Override
	protected void doNotifyAtomicComparisonSkipped() {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyAtomicComparisonPerformed()
	 */
	@Override
	protected void doNotifyAtomicComparisonPerformed() {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyAtomicComparisonEnd()
	 */
	@Override
	protected void doNotifyAtomicComparisonEnd() {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyNumberOfAuthoritativeFullMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfAuthoritativeFullMatchesChanged(int matches) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyNumberOfAuthoritativeNoMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfAuthoritativeNoMatchesChanged(int matches) {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandlerSkeleton#doNotifyNumberOfMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfMatchesChanged(int matches) {
	}
}
