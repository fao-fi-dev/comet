/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Interface InteractiveMatchingProcessHandler.
 *
 * @param <SOURCE> the generic source type
 * @param <TARGET> the generic target type
 */
public interface InteractiveMatchingProcessHandler<SOURCE extends Serializable, TARGET extends Serializable> extends MatchingProcessHandler<SOURCE, TARGET> {
	
	/**
	 * Sets the interactive matching events listener.
	 *
	 * @param listener the listener
	 */
	void setInteractiveMatchingEventsListener(InteractiveMatchingEventsListener<SOURCE, TARGET> listener);
}
