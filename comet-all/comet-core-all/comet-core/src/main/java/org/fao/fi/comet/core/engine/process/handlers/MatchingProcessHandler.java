/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;

/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * 
 * @param <SOURCE> the generic source type
 * @param <TARGET> the generic target type
 */
public interface MatchingProcessHandler<SOURCE extends Serializable, TARGET extends Serializable> {
	/**
	 * Gets the process id.
	 *
	 * @return the process id
	 */
	String getProcessId();

	/**
	 * Gets the process status.
	 *
	 * @return the process status
	 */
	MatchingEngineProcessStatus getProcessStatus();

	/**
	 * Notify comparison process started.
	 *
	 * @param processId the process id
	 */
	void notifyComparisonProcessStarted(String processId);
	
	/**
	 * Notify comparison process completed.
	 */
	void notifyComparisonProcessCompleted();

	/**
	 * Gets the process start timestamp.
	 *
	 * @return the process start timestamp
	 */
	long getProcessStartTimestamp();
	
	/**
	 * Gets the process end timestamp.
	 *
	 * @return the process end timestamp
	 */
	long getProcessEndTimestamp();

	/**
	 * Gets the elapsed.
	 *
	 * @return the elapsed
	 */
	long getElapsed();

//	boolean isRunning();
//	void setRunning(boolean running);
//	
//	MatchingEngineStatus getStatus();
//	void setStatus(MatchingEngineStatus status);
//	boolean is(MatchingEngineStatus status);
	
	/**
 * Halt.
 */
void halt();

	/**
	 * Gets the total number of atomic comparisons performed.
	 *
	 * @return the total number of atomic comparisons performed
	 */
	int getTotalNumberOfAtomicComparisonsPerformed();

	/**
	 * Sets the number of comparison rounds.
	 *
	 * @param rounds the new number of comparison rounds
	 */
	void setNumberOfComparisonRounds(int rounds);
	
	/**
	 * Gets the current number of comparison rounds performed.
	 *
	 * @return the current number of comparison rounds performed
	 */
	int getCurrentNumberOfComparisonRoundsPerformed();

	/**
	 * Notify comparison round start.
	 *
	 * @param target the target
	 */
	void notifyComparisonRoundStart(ProvidedData<TARGET> target);
	
	/**
	 * Notify comparison round performed.
	 *
	 * @param target the target
	 */
	void notifyComparisonRoundPerformed(ProvidedData<TARGET> target);

	/**
	 * Gets the current number of atomic comparisons performed in round.
	 *
	 * @return the current number of atomic comparisons performed in round
	 */
	int getCurrentNumberOfAtomicComparisonsPerformedInRound();
	
	/**
	 * Sets the maximum number of atomic comparisons performed in round.
	 *
	 * @param comparisons the new maximum number of atomic comparisons performed in round
	 */
	void setMaximumNumberOfAtomicComparisonsPerformedInRound(int comparisons);

	/**
	 * Notify atomic comparison start.
	 */
	void notifyAtomicComparisonStart();
	
	/**
	 * Notify atomic comparison performed.
	 */
	void notifyAtomicComparisonPerformed();
	
	/**
	 * Notify atomic comparison skipped.
	 */
	void notifyAtomicComparisonSkipped();

	/**
	 * Gets the number of atomic comparisons skipped.
	 *
	 * @return the number of atomic comparisons skipped
	 */
	int getNumberOfAtomicComparisonsSkipped();

	/**
	 * Notify authoritative full match.
	 */
	void notifyAuthoritativeFullMatch();
	
	/**
	 * Notify authoritative no match.
	 */
	void notifyAuthoritativeNoMatch();
	
	/**
	 * Notify match.
	 */
	void notifyMatch();

	/**
	 * Gets the number of authoritative full matches.
	 *
	 * @return the number of authoritative full matches
	 */
	int getNumberOfAuthoritativeFullMatches();
	
	/**
	 * Gets the number of authoritative no matches.
	 *
	 * @return the number of authoritative no matches
	 */
	int getNumberOfAuthoritativeNoMatches();
	
	/**
	 * Gets the number of matches.
	 *
	 * @return the number of matches
	 */
	int getNumberOfMatches();

	/**
	 * Gets the total number of matches.
	 *
	 * @return the total number of matches
	 */
	int getTotalNumberOfMatches();

	/**
	 * Notify atomic comparison end.
	 */
	void notifyAtomicComparisonEnd();

	/**
	 * Gets the atomic throughput.
	 *
	 * @return the atomic throughput
	 */
	double getAtomicThroughput();
	
	/**
	 * Gets the throughput.
	 *
	 * @return the throughput
	 */
	double getThroughput();

	/**
	 * Notify comparison error.
	 */
	void notifyComparisonError();
	
	/**
	 * Gets the number of comparison errors.
	 *
	 * @return the number of comparison errors
	 */
	int getNumberOfComparisonErrors();
	
	/**
	 * Update current results.
	 *
	 * @param results the results
	 */
	void updateCurrentResults(MatchingsData<SOURCE, TARGET> results);
	
	/**
	 * Gets the current results.
	 *
	 * @return the current results
	 */
	MatchingsData<SOURCE, TARGET> getCurrentResults();
}