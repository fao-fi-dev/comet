/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.impl.basic;

import java.io.Serializable;

import org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * 
 */
public class SerializableDataIDHandler<DATA> extends IDHandlerSkeleton<DATA, Serializable> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.io.Serializable)
	 */
	@Override
	protected Serializable doGetId(DATA data) {
		return SerializationHelper.toString((Serializable)data, true);
	}
}
