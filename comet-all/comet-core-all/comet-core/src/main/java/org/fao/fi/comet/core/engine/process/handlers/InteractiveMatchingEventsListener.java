/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.MatchingsData;

/**
 * @param <SOURCE> the generic source type
 * @param <TARGET> the generic target type
 */
public interface InteractiveMatchingEventsListener<SOURCE extends Serializable, TARGET extends Serializable> {
	
	/**
	 * Notify halt.
	 *
	 * @param currentMatchings the current matchings
	 */
	void notifyHalt(MatchingsData<SOURCE, TARGET> currentMatchings);
	
	/**
	 * Notify dump.
	 *
	 * @param currentMatchings the current matchings
	 */
	void notifyDump(MatchingsData<SOURCE, TARGET> currentMatchings);
}
