/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * @param <IDENTIFIER> the generic type
 * 
 */
public interface IDHandler<DATA, IDENTIFIER extends Serializable> {
	
	/**
	 * Gets the id.
	 *
	 * @param data the data
	 * @return the id
	 */
	IDENTIFIER getId(DATA data);
	
	/**
	 * Serialize id.
	 *
	 * @param id the id
	 * @return the string
	 */
	String serializeId(IDENTIFIER id);
	
	/**
	 * Gets the serialized id.
	 *
	 * @param data the data
	 * @return the serialized id
	 */
	String getSerializedId(DATA data);
}
