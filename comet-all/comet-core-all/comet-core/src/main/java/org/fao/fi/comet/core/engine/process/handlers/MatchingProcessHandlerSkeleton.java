/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_assert;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_nAssert;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * 
 * @param <SOURCE> the generic source type
 * @param <TARGET> the generic target type
 * 
 */
abstract public class MatchingProcessHandlerSkeleton<SOURCE extends Serializable, TARGET extends Serializable> implements MatchingProcessHandler<SOURCE, TARGET> {
//	private boolean _running = false;

	/** The _process status. */
	@XmlTransient private MatchingEngineProcessStatus _processStatus;

	/**
	 * Instantiates a new matching process handler skeleton.
	 */
	public MatchingProcessHandlerSkeleton() {
		this._processStatus = new MatchingEngineProcessStatus();

		this._processStatus.setProcessId(ObjectsHelper.thi$(this));
	}

	/**
	 * Instantiates a new matching process handler skeleton.
	 *
	 * @param processId the process id
	 */
	public MatchingProcessHandlerSkeleton(String processId) {
		this();

		this._processStatus.setProcessId(processId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getProcessStatus()
	 */
	@Override
	public MatchingEngineProcessStatus getProcessStatus() {
		return this._processStatus;
	}

//	/* (non-Javadoc)
//	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#isRunning()
//	 */
//	@Override
//	final public boolean isRunning() {
//		return this._running;
//	}

//	/* (non-Javadoc)
//	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#setRunning(boolean)
//	 */
//	@Override
//	final public void setRunning(boolean running) {
//		synchronized(this) {
//			this._running = running;
//		}
//	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#halt()
	 */
	@Override
	final public void halt() {
		synchronized(this) {
			$_assert(this.getProcessStatus().is(MatchingEngineStatus.RUNNING), "The engine cannot be halted as it is not running!");

//			this.setRunning(false);
			this.getProcessStatus().setStatus(MatchingEngineStatus.HALTED);
		}
	}


	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyProcessStartEvent(java.lang.String)
	 */
	@Override
	final public void notifyComparisonProcessStarted(String processId) {
		synchronized(this) {
			$_nAssert(this.getProcessStatus().is(MatchingEngineStatus.RUNNING), "The engine cannot be started as it is already running!");

			this._processStatus.setProcessId(processId);
			this._processStatus.setProcessStartTimestamp(System.currentTimeMillis());
			this._processStatus.setProcessEndTimestamp(-1);

			this.getProcessStatus().setStatus(MatchingEngineStatus.RUNNING);
			
//			this._running = true;

			this.doNotifyProcessStartEvent(processId);
		}
	}

	/**
	 * Do notify process start event.
	 *
	 * @param processId the process id
	 */
	abstract protected void doNotifyProcessStartEvent(String processId);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyProcessEndEvent()
	 */
	@Override
	final public void notifyComparisonProcessCompleted() {
		synchronized(this) {
			this._processStatus.setProcessEndTimestamp(System.currentTimeMillis());
			
			this.doNotifyProcessEndEvent();
			
			
			if(!this.getProcessStatus().equals(MatchingEngineStatus.HALTED))
				this.getProcessStatus().setStatus(MatchingEngineStatus.COMPLETED);
		}
	}

	/**
	 * Do notify process end event.
	 */
	abstract protected void doNotifyProcessEndEvent();

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getElapsed()
	 */
	@Override
	final public long getElapsed() {
		long processStartTimestamp = this._processStatus.getProcessStartTimestamp();
		long processEndTimestamp = this._processStatus.getProcessEndTimestamp();
		return processStartTimestamp == -1 ? -1 :
			 ( processEndTimestamp == -1 ? System.currentTimeMillis() : processEndTimestamp ) - processStartTimestamp;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#setNumberOfComparisonRounds(int)
	 */
	@Override
	final public void setNumberOfComparisonRounds(int rounds) {
		this._processStatus.setNumberOfComparisonRounds(rounds);

		this.doNotifyNumberOfComparisonRoundsChanged(rounds);
	}

	/**
	 * Do notify number of comparison rounds changed.
	 *
	 * @param rounds the rounds
	 */
	abstract protected void doNotifyNumberOfComparisonRoundsChanged(int rounds);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyComparisonRoundStart()
	 */
	@Override
	final public void notifyComparisonRoundStart(ProvidedData<TARGET> target) {
		this._processStatus.setCurrentNumberOfAtomicComparisonsPerformedInRound(0);

		this.doNotifyComparisonRoundStart(target);
	}

	/**
	 * Do notify comparison round start.
	 *
	 * @param target the target
	 */
	abstract protected void doNotifyComparisonRoundStart(ProvidedData<TARGET> target);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyComparisonRoundEnd()
	 */
	@Override
	final public void notifyComparisonRoundPerformed(ProvidedData<TARGET> target) {
		this._processStatus.increaseCurrentNumberOfComparisonRoundsPerformed();

		this.doNotifyComparisonRoundPerformed(target);
	}

	/**
	 * Do notify comparison round performed.
	 *
	 * @param target the target
	 */
	abstract protected void doNotifyComparisonRoundPerformed(ProvidedData<TARGET> target);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#setMaximumNumberOfAtomicComparisonsPerformedInRound(int)
	 */
	@Override
	final public void setMaximumNumberOfAtomicComparisonsPerformedInRound(int comparisons) {
		this._processStatus.setMaximumNumberOfAtomicComparisonsPerformedInRound(comparisons);
		this._processStatus.setCurrentNumberOfAtomicComparisonsPerformedInRound(0);

		this.doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(comparisons);
	}

	/**
	 * Do notify maximum number of atomic comparisons performed in round changed.
	 *
	 * @param comparisons the comparisons
	 */
	abstract protected void doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int comparisons);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAtomicComparisonStart()
	 */
	@Override
	final public void notifyAtomicComparisonStart() {
		this.doNotifyAtomicComparisonStart();
	}

	/**
	 * Do notify atomic comparison start.
	 */
	abstract protected void doNotifyAtomicComparisonStart();

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAtomicComparisonPerformedEvent()
	 */
	@Override
	final public void notifyAtomicComparisonPerformed() {
		this._processStatus.increaseCurrentNumberOfAtomicComparisonsPerformedInRound();

		this.doNotifyTotalNumberOfAtomicComparisonsChanged(this._processStatus.increaseTotalNumberOfAtomicComparisonsPerformed());
		this.doNotifyAtomicComparisonPerformed();
	}

	/**
	 * Do notify total number of atomic comparisons changed.
	 *
	 * @param atomicComparisons the atomic comparisons
	 */
	abstract protected void doNotifyTotalNumberOfAtomicComparisonsChanged(int atomicComparisons);
	
	/**
	 * Do notify atomic comparison performed.
	 */
	abstract protected void doNotifyAtomicComparisonPerformed();

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAuthoritativeFullMatch()
	 */
	@Override
	final public void notifyAuthoritativeFullMatch() {
		this.doNotifyNumberOfAuthoritativeFullMatchesChanged(this._processStatus.increaseNumberOfAuthoritativeFullMatches());
	}

	/**
	 * Do notify number of authoritative full matches changed.
	 *
	 * @param matches the matches
	 */
	abstract protected void doNotifyNumberOfAuthoritativeFullMatchesChanged(int matches);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAuthoritativeNoMatch()
	 */
	@Override
	final public void notifyAuthoritativeNoMatch() {
		this.doNotifyNumberOfAuthoritativeNoMatchesChanged(this._processStatus.increaseNumberOfAuthoritativeNoMatches());
	}

	/**
	 * Do notify number of authoritative no matches changed.
	 *
	 * @param matches the matches
	 */
	abstract protected void doNotifyNumberOfAuthoritativeNoMatchesChanged(int matches);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyMatch()
	 */
	@Override
	final public void notifyMatch() {
		this.doNotifyNumberOfMatchesChanged(this._processStatus.increaseNumberOfMatches());
	}

	/**
	 * Do notify number of matches changed.
	 *
	 * @param matches the matches
	 */
	abstract protected void doNotifyNumberOfMatchesChanged(int matches);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getTotalNumberOfMatches()
	 */
	@Override
	public int getTotalNumberOfMatches() {
		return this._processStatus.getNumberOfMatches() + this._processStatus.getNumberOfAuthoritativeFullMatches();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAtomicComparisonEnd()
	 */
	@Override
	final public void notifyAtomicComparisonEnd() {
		this.doNotifyAtomicComparisonEnd();
	}

	/**
	 * Do notify atomic comparison end.
	 */
	abstract protected void doNotifyAtomicComparisonEnd();
	
	/**
	 * Do notify atomic comparison skipped.
	 */
	abstract protected void doNotifyAtomicComparisonSkipped();
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getAtomicThroughput()
	 */
	@Override
	final public double getAtomicThroughput() {
		final long elapsed = this.getElapsed();

		final int comparisonRoundsPerformed = this.getTotalNumberOfAtomicComparisonsPerformed();

		if(elapsed == 0)
			return comparisonRoundsPerformed == 0 ? 0D : Double.POSITIVE_INFINITY;

		return comparisonRoundsPerformed * 1D / elapsed;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getThroughput()
	 */
	@Override
	final public double getThroughput() {
		final long elapsed = this.getElapsed();
		final int comparisonRoundsPerformed = this.getCurrentNumberOfComparisonRoundsPerformed();

		if(elapsed == 0)
			return comparisonRoundsPerformed == 0 ? 0D : Double.POSITIVE_INFINITY;

		return comparisonRoundsPerformed * 1D / elapsed;
	}

	/**
	 * Sets the current number of atomic comparisons performed in round.
	 *
	 * @param currentNumberOfAtomicComparisonsPerformedInRound the new current number of atomic comparisons performed in round
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setCurrentNumberOfAtomicComparisonsPerformedInRound(int)
	 */
	public void setCurrentNumberOfAtomicComparisonsPerformedInRound(int currentNumberOfAtomicComparisonsPerformedInRound) {
		this._processStatus.setCurrentNumberOfAtomicComparisonsPerformedInRound(currentNumberOfAtomicComparisonsPerformedInRound);
	}

	/**
	 * Sets the current number of comparison rounds performed.
	 *
	 * @param currentNumberOfComparisonRoundsPerformed the new current number of comparison rounds performed
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setCurrentNumberOfComparisonRoundsPerformed(int)
	 */
	public void setCurrentNumberOfComparisonRoundsPerformed(int currentNumberOfComparisonRoundsPerformed) {
		this._processStatus.setCurrentNumberOfComparisonRoundsPerformed(currentNumberOfComparisonRoundsPerformed);
	}

	/**
	 * Gets the maximum number of atomic comparisons performed in round.
	 *
	 * @return the maximum number of atomic comparisons performed in round
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getMaximumNumberOfAtomicComparisonsPerformedInRound()
	 */
	public int getMaximumNumberOfAtomicComparisonsPerformedInRound() {
		return this._processStatus.getMaximumNumberOfAtomicComparisonsPerformedInRound();
	}

	/**
	 * Sets the number of authoritative full matches.
	 *
	 * @param numberOfAuthoritativeFullMatches the new number of authoritative full matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setNumberOfAuthoritativeFullMatches(int)
	 */
	public void setNumberOfAuthoritativeFullMatches(int numberOfAuthoritativeFullMatches) {
		this._processStatus.setNumberOfAuthoritativeFullMatches(numberOfAuthoritativeFullMatches);
	}

	/**
	 * Sets the number of authoritative no matches.
	 *
	 * @param numberOfAuthoritativeNoMatches the new number of authoritative no matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setNumberOfAuthoritativeNoMatches(int)
	 */
	public void setNumberOfAuthoritativeNoMatches(int numberOfAuthoritativeNoMatches) {
		this._processStatus.setNumberOfAuthoritativeNoMatches(numberOfAuthoritativeNoMatches);
	}

	/**
	 * Gets the number of comparison rounds.
	 *
	 * @return the number of comparison rounds
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getNumberOfComparisonRounds()
	 */
	public int getNumberOfComparisonRounds() {
		return this._processStatus.getNumberOfComparisonRounds();
	}

	/**
	 * Sets the number of matches.
	 *
	 * @param numberOfMatches the new number of matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setNumberOfMatches(int)
	 */
	public void setNumberOfMatches(int numberOfMatches) {
		this._processStatus.setNumberOfMatches(numberOfMatches);
	}

	/**
	 * Sets the process end timestamp.
	 *
	 * @param processEndTimestamp the new process end timestamp
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setProcessEndTimestamp(long)
	 */
	public void setProcessEndTimestamp(long processEndTimestamp) {
		this._processStatus.setProcessEndTimestamp(processEndTimestamp);
	}

	/**
	 * Sets the process start timestamp.
	 *
	 * @param processStartTimestamp the new process start timestamp
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setProcessStartTimestamp(long)
	 */
	public void setProcessStartTimestamp(long processStartTimestamp) {
		this._processStatus.setProcessStartTimestamp(processStartTimestamp);
	}

	/**
	 * Sets the total number of atomic comparisons performed.
	 *
	 * @param totalNumberOfAtomicComparisonsPerformed the new total number of atomic comparisons performed
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setTotalNumberOfAtomicComparisonsPerformed(int)
	 */
	public void setTotalNumberOfAtomicComparisonsPerformed(int totalNumberOfAtomicComparisonsPerformed) {
		this._processStatus.setTotalNumberOfAtomicComparisonsPerformed(totalNumberOfAtomicComparisonsPerformed);
	}

	/**
	 * Gets the current number of atomic comparisons performed in round.
	 *
	 * @return the current number of atomic comparisons performed in round
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getCurrentNumberOfAtomicComparisonsPerformedInRound()
	 */
	@Override
	public int getCurrentNumberOfAtomicComparisonsPerformedInRound() {
		return this._processStatus.getCurrentNumberOfAtomicComparisonsPerformedInRound();
	}

	/**
	 * Gets the current number of comparison rounds performed.
	 *
	 * @return the current number of comparison rounds performed
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getCurrentNumberOfComparisonRoundsPerformed()
	 */
	@Override
	public int getCurrentNumberOfComparisonRoundsPerformed() {
		return this._processStatus.getCurrentNumberOfComparisonRoundsPerformed();
	}

	/**
	 * Gets the number of authoritative full matches.
	 *
	 * @return the number of authoritative full matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getNumberOfAuthoritativeFullMatches()
	 */
	@Override
	public int getNumberOfAuthoritativeFullMatches() {
		return this._processStatus.getNumberOfAuthoritativeFullMatches();
	}

	/**
	 * Gets the number of authoritative no matches.
	 *
	 * @return the number of authoritative no matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getNumberOfAuthoritativeNoMatches()
	 */
	@Override
	public int getNumberOfAuthoritativeNoMatches() {
		return this._processStatus.getNumberOfAuthoritativeNoMatches();
	}

	/**
	 * Gets the number of matches.
	 *
	 * @return the number of matches
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getNumberOfMatches()
	 */
	@Override
	public int getNumberOfMatches() {
		return this._processStatus.getNumberOfMatches();
	}

	/**
	 * Gets the process end timestamp.
	 *
	 * @return the process end timestamp
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getProcessEndTimestamp()
	 */
	@Override
	public long getProcessEndTimestamp() {
		return this._processStatus.getProcessEndTimestamp();
	}

	/**
	 * Gets the process id.
	 *
	 * @return the process id
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getProcessId()
	 */
	@Override
	public String getProcessId() {
		return this._processStatus.getProcessId();
	}

	/**
	 * Sets the process id.
	 *
	 * @param processId the new process id
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#setProcessId(java.lang.String)
	 */
	public void setProcessId(String processId) {
		this._processStatus.setProcessId(processId);
	}

	/**
	 * Gets the process start timestamp.
	 *
	 * @return the process start timestamp
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getProcessStartTimestamp()
	 */
	@Override
	public long getProcessStartTimestamp() {
		return this._processStatus.getProcessStartTimestamp();
	}

	/**
	 * Gets the total number of atomic comparisons performed.
	 *
	 * @return the total number of atomic comparisons performed
	 * @see org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus#getTotalNumberOfAtomicComparisonsPerformed()
	 */
	@Override
	public int getTotalNumberOfAtomicComparisonsPerformed() {
		return this._processStatus.getTotalNumberOfAtomicComparisonsPerformed();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyComparisonError()
	 */
	@Override
	public void notifyComparisonError() {
		this._processStatus.increaseNumberOfComparisonErrors();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getNumberOfComparisonErrors()
	 */
	@Override
	public int getNumberOfComparisonErrors() {
		return this._processStatus.getNumberOfComparisonErrors();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#notifyAtomicComparisonSkipped()
	 */
	@Override
	public void notifyAtomicComparisonSkipped() {
		this._processStatus.increaseNumberOfAtomicComparisonsSkipped();
		
		this.doNotifyAtomicComparisonSkipped();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler#getNumberOfAtomicComparisonsSkipped()
	 */
	@Override
	public int getNumberOfAtomicComparisonsSkipped() {
		return this._processStatus.getNumberOfAtomicComparisonsSkipped();
	}
}