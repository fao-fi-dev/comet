/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.providers;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @param <ENTITY> the generic type
 * 
 */
public interface FeedableDataProvider<ENTITY> extends DataProvider<ENTITY> {
	
	/**
	 * Adds the data.
	 *
	 * @param toAdd the to add
	 */
	void addData(ProvidedData<ENTITY> toAdd);
}
