/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.partitioners;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.Matchlet;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE_ENTITY> the generic type
 * @param <TARGET_ENTITY> the generic type
 * 
 */
public interface DataPartitioner<SOURCE_ENTITY extends Serializable, TARGET_ENTITY extends Serializable> {
	
	/**
	 * Include.
	 *
	 * @param source the source
	 * @param target the target
	 * @param matchlets the matchlets
	 * @return true, if successful
	 */
	boolean include(SOURCE_ENTITY source, TARGET_ENTITY target, Collection<? extends Matchlet<SOURCE_ENTITY, ?, TARGET_ENTITY, ?>> matchlets);
}
