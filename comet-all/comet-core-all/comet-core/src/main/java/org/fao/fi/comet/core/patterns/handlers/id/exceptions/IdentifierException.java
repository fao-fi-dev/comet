/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.exceptions;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 06/giu/2013   Fabio     Creation.
 *
 * @version 1.0
 * 
 */
abstract public class IdentifierException extends RuntimeException {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = -2605833555036481365L;

	/**
	 * Class constructor.
	 */
	public IdentifierException() {
		super();
	}

	/**
	 * Class constructor.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public IdentifierException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor.
	 *
	 * @param message the message
	 */
	public IdentifierException(String message) {
		super(message);
	}

	/**
	 * Class constructor.
	 *
	 * @param cause the cause
	 */
	public IdentifierException(Throwable cause) {
		super(cause);
	}
}