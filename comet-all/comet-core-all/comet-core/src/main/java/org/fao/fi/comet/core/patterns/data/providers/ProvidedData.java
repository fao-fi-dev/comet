/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.providers;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <ENTITY> the generic type
 * 
 */
public class ProvidedData<ENTITY> {
	
	/** The _provider id. */
	private String _providerId;
	
	/** The _data. */
	private ENTITY _data;
	
	/**
	 * Class constructor.
	 */
	public ProvidedData() {
		super();
	}

	/**
	 * Class constructor.
	 *
	 * @param providerId the provider id
	 * @param data the data
	 */
	public ProvidedData(String providerId, ENTITY data) {
		super();
		this._providerId = providerId;
		this._data = data;
	}

	/**
	 * Gets the provider id.
	 *
	 * @return the 'providerId' value
	 */
	public String getProviderId() {
		return this._providerId;
	}

	/**
	 * Sets the provider id.
	 *
	 * @param providerId the 'providerId' value to set
	 */
	public void setProviderId(String providerId) {
		this._providerId = providerId;
	}

	/**
	 * Gets the data.
	 *
	 * @return the 'data' value
	 */
	public ENTITY getData() {
		return this._data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the 'data' value to set
	 */
	public void setData(ENTITY data) {
		this._data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._data == null) ? 0 : this._data.hashCode());
		result = prime * result + ((this._providerId == null) ? 0 : this._providerId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		ProvidedData<ENTITY> other = (ProvidedData<ENTITY>) obj;
		if (this._data == null) {
			if (other._data != null)
				return false;
		} else if (!this._data.equals(other._data))
			return false;
		if (this._providerId == null) {
			if (other._providerId != null)
				return false;
		} else if (!this._providerId.equals(other._providerId))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder(this._providerId == null ? "<PROVIDER ID NOT SET>": this._providerId).append(":").append(this._data).toString();
	}
}
