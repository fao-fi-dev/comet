/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.impl;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * @param <IDENTIFIER> the generic type
 * 
 */
abstract public class IDHandlerSkeleton<DATA, IDENTIFIER extends Serializable> implements IDHandler<DATA, IDENTIFIER> {
	
	/** The _id cache. */
	private final Map<DATA, IDENTIFIER> _idCache = Collections.synchronizedMap(new HashMap<DATA, IDENTIFIER>());

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getId(java.io.Serializable)
	 */
	@Override
	final public IDENTIFIER getId(DATA data) {
		if(data == null) return null;

		//Equal objects would be cached with the same ID
		IDENTIFIER cachedId = this._idCache.get(data);

		if(cachedId == null) this._idCache.put(data, this.doGetId(data));

		return this._idCache.get(data);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#serializeId(java.io.Serializable)
	 */
	@Override
	final public String serializeId(IDENTIFIER id) {
		if(id == null) return null;

		return this.sanitize(this.doSerializeId(id));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getSerializeId(java.io.Serializable)
	 */
	@Override
	public String getSerializedId(DATA data) {
		return this.serializeId(this.getId(data));
	}

	/**
	 * Do get id.
	 *
	 * @param data the data
	 * @return the identifier
	 */
	abstract protected IDENTIFIER doGetId(DATA data);

	/**
	 * Do serialize id.
	 *
	 * @param id the id
	 * @return the string
	 */
	protected String doSerializeId(IDENTIFIER id) {
		return id.toString();
	}

	/**
	 * Sanitize.
	 *
	 * @param id the id
	 * @return the string
	 */
	protected String sanitize(String id) {
		return id.replaceAll("\"", "&quot;");
	}
}