/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.impl.basic;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.core.patterns.handlers.id.exceptions.MultipleNamedIdentifiersException;
import org.fao.fi.comet.core.patterns.handlers.id.exceptions.NoNamedIdentifierException;
import org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
// TODO: Auto-generated Javadoc

/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * 
 */
public class NameDrivenDataIDHandler<DATA extends Serializable> extends IDHandlerSkeleton<DATA, Serializable>{
	
	/** The ID name. */
	private final String _IDName;

	/**
	 * Instantiates a new name driven data id handler.
	 *
	 * @param IDName the ID name
	 */
	public NameDrivenDataIDHandler(String IDName) {
		if(IDName == null)
			throw new UnsupportedOperationException("ID name cannot be null");

		this._IDName = IDName;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.io.Serializable)
	 */
	@Override
	final protected Serializable doGetId(DATA data) {
		this.checkNames($nN(data, "Data cannot be NULL"), this._IDName);

		Field[] annotatedFields = this.getNamedFields(data, this._IDName);

		try {
			if(annotatedFields.length == 1)
				return this.getId(data, annotatedFields[0]);

			return this.getId(data, this.getNamedMethods(data, this._IDName)[0]);
		} catch (Throwable t) {
			throw new NoNamedIdentifierException("Unable to retrieve ID by name " + this._IDName + " on " + data);
		}
	}

	/**
	 * Gets the id.
	 *
	 * @param object the object
	 * @param field the field
	 * @return the id
	 * @throws Throwable the throwable
	 */
	private Serializable getId(DATA object, Field field) throws Throwable {
		return ((Serializable)field.get(object));
	}

	/**
	 * Gets the id.
	 *
	 * @param object the object
	 * @param method the method
	 * @return the id
	 * @throws Throwable the throwable
	 */
	private Serializable getId(DATA object, Method method) throws Throwable {
		return ((Serializable)method.invoke(object));
	}

	/**
	 * Gets the named fields.
	 *
	 * @param object the object
	 * @param IDName the ID name
	 * @return the named fields
	 */
	private Field[] getNamedFields(DATA object, String IDName) {
		if(object == null)
			return null;

		Collection<Field> fields = new ArrayList<Field>();

		for(Field field : ObjectsHelper.listAllFields(object.getClass())) {
			field.setAccessible(true);

			if(IDName.equals(field.getName()))
				fields.add(field);
		}

		return fields.toArray(new Field[fields.size()]);
	}

	/**
	 * Gets the named methods.
	 *
	 * @param object the object
	 * @param IDName the ID name
	 * @return the named methods
	 */
	private Method[] getNamedMethods(DATA object, String IDName) {
		if(object == null)
			return null;

		Collection<Method> methods = new ArrayList<Method>();

		for(Method method : ObjectsHelper.getAllGetterMethods(object.getClass(), Serializable.class)) {
			method.setAccessible(true);

			if(method.getName().equals("get" + StringsHelper.capitalizeFirstLetter(IDName)))
				methods.add(method);
		}

		return methods.toArray(new Method[methods.size()]);
	}
	
	/**
	 * Check names.
	 *
	 * @param object the object
	 * @param IDName the ID name
	 */
	private void checkNames(DATA object, String IDName) {
		Field[] namedFields = this.getNamedFields(object, IDName);
		Method[] namedMethods = this.getNamedMethods(object, IDName);

		int numNamedFields = namedFields.length;
		int numNamedMethods = namedMethods.length;

		if(numNamedFields + numNamedMethods > 1)
			throw new MultipleNamedIdentifiersException("Multiple fields or getter methods referring to name '" + IDName + "' found for object of class " + object.getClass().getSimpleName() + ". Currently: " + numNamedFields + " annotated fields and " + numNamedMethods + " annotated getter methods");

		if(numNamedFields == 0 && numNamedMethods == 0)
			throw new NoNamedIdentifierException("No fields of type (or getter methods returning an instance of) java.lang.Serializable and named after '" + IDName + "' found for object of class " + object.getClass().getSimpleName() + ".");
	}
}