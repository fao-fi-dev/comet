/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.matchlets.skeleton;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.matchlets.ScalarMatchlet;
import org.fao.fi.comet.core.model.score.support.MatchingScore;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <SOURCE_DATA> the generic type
 * @param <TARGET> the generic type
 * @param <TARGET_DATA> the generic type
 * 
 */
abstract public class ScalarMatchletSkeleton<SOURCE,
											SOURCE_DATA extends Serializable,
											TARGET,
											TARGET_DATA extends Serializable>
				extends MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements ScalarMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	/**  Field serialVersionUID. */
	private static final long serialVersionUID = -5491653040298122309L;

	/**
	 * Class constructor.
	 */
	public ScalarMatchletSkeleton() {
		super(new BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>());
	}

	/**
	 * Class constructor.
	 *
	 * @param behaviour the behaviour
	 */
	public ScalarMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		return;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#compareData(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingResult<SOURCE_DATA, TARGET_DATA> compareData(SOURCE sourceEntity, DataIdentifier sourceIdentifier, TARGET targetEntity, DataIdentifier targetIdentifier) {
		$nN(sourceEntity, "Source entity cannot be null");
		$nN(sourceIdentifier, "Source entity identifier cannot be null");
		$nN(targetEntity, "Target entity cannot be null");
		$nN(targetIdentifier, "Target entity identifier cannot be null");

		SOURCE_DATA sourceData = this.extractSourceData(sourceEntity, sourceIdentifier);
		TARGET_DATA targetData = this.extractTargetData(targetEntity, targetIdentifier);

		MatchingScore score = null;

		if(this.forceComparison()) {
			score = this.computeScore(sourceEntity, sourceIdentifier, sourceData, targetEntity, targetIdentifier, targetData);
		} else {
			if(sourceData == null && targetData == null) {
				score = MatchingScore.getNonPerformedTemplate();
			} else if(sourceData == null || targetData == null) {
				if(this.isOptional())
					score = MatchingScore.getNonPerformedTemplate();
				else
					score = MatchingScore.getNonAuthoritativeNoMatchTemplate();
			} else {
				score = this.computeScore(sourceEntity, sourceIdentifier, sourceData, targetEntity, targetIdentifier, targetData);
			}
		}

		MatchingScore updated = this._behaviour.updateScore(score, sourceEntity, sourceData, targetEntity, targetData);

		return this.updateResult(this.newMatchingResult(), updated, sourceData, sourceIdentifier, targetData);
	}
}
