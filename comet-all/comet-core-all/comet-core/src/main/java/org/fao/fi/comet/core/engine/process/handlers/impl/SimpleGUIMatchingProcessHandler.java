/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers.impl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import org.fao.fi.comet.core.engine.process.handlers.InteractiveMatchingEventsListener;
import org.fao.fi.comet.core.engine.process.handlers.InteractiveMatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import javax.swing.border.BevelBorder;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date 		 Author 		 Comment
 * ------------- --------------- -----------------------
 * 8 Jul 2010 	 Fiorellato		 Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * 
 */
public class SimpleGUIMatchingProcessHandler<SOURCE extends Serializable, TARGET extends Serializable> extends SilentMatchingProcessHandler<SOURCE, TARGET> implements ActionListener, InteractiveMatchingProcessHandler<SOURCE, TARGET> {
	
	/** The Constant DOUBLE_5_FORMATTER. */
	static final private DecimalFormat DOUBLE_5_FORMATTER = new DecimalFormat("0.00000");
	
	/** The Constant DOUBLE_2_FORMATTER. */
	static final private DecimalFormat DOUBLE_2_FORMATTER = new DecimalFormat("0.00");
	
	/** The Constant INTEGER_2_FORMATTER. */
	static final private DecimalFormat INTEGER_2_FORMATTER = new DecimalFormat("00");

	/** The timer. */
	static private Timer TIMER;

	/** The _listener. */
	protected InteractiveMatchingEventsListener<SOURCE, TARGET> _listener;

	/** The _gui. */
	private JFrame _gui;
	
	/** The _main panel. */
	private JPanel _mainPanel;

	/** The _halt. */
	private JButton _halt;
	
	/** The _dump. */
	private JButton _dump;

	/** The GUI already set up. */
	private boolean _GUIAlreadySetUp = false;

	/** The _is initialized. */
	@SuppressWarnings("unused")
	private boolean _isInitialized = false;
	
	/** The _matchings panel. */
	private JPanel _matchingsPanel;
	
	/** The label_1. */
	private JLabel label_1;
	
	/** The _auth no matches. */
	private JTextField _authNoMatches;
	
	/** The label_3. */
	private JLabel label_3;
	
	/** The _auth full matches. */
	private JTextField _authFullMatches;
	
	/** The label_5. */
	private JLabel label_5;
	
	/** The _matches. */
	private JTextField _matches;
	
	/** The _reduced matches. */
	private JTextField _reducedMatches;
	
	/** The horizontal strut. */
	private Component horizontalStrut;
	
	/** The lbl new label. */
	private JLabel lblNewLabel;
	
	/** The _process panel. */
	private JPanel _processPanel;
	
	/** The _sources data panel. */
	private JPanel _sourcesDataPanel;
	
	/** The _sources max. */
	private JLabel _sourcesMax;
	
	/** The label_9. */
	private JLabel label_9;
	
	/** The _sources progress. */
	private JProgressBar _sourcesProgress;
	
	/** The _targets data panel. */
	private JPanel _targetsDataPanel;
	
	/** The label_10. */
	private JLabel label_10;
	
	/** The _targets progress. */
	private JProgressBar _targetsProgress;
	
	/** The _targets max. */
	private JLabel _targetsMax;
	
	/** The _comparisons panel. */
	private JPanel _comparisonsPanel;
	
	/** The label_12. */
	private JLabel label_12;
	
	/** The lbl new label_1. */
	private JLabel lblNewLabel_1;
	
	/** The _current comparisons. */
	private JTextField _currentComparisons;
	
	/** The _skipped comparisons. */
	private JTextField _skippedComparisons;
	
	/** The horizontal strut_1. */
	private Component horizontalStrut_1;
	
	/** The _source min. */
	private JLabel _sourceMin;
	
	/** The _target min. */
	private JLabel _targetMin;
	
	/** The _status panel. */
	private JPanel _statusPanel;
	
	/** The lbl new label_2. */
	private JLabel lblNewLabel_2;
	
	/** The _elapsed. */
	private JLabel _elapsed;
	
	/** The _label atomic throughput. */
	private JLabel _labelAtomicThroughput;
	
	/** The _label throughput. */
	private JLabel _labelThroughput;
	
	/** The _atomic throughput. */
	private JTextField _atomicThroughput;
	
	/** The _throughput. */
	private JTextField _throughput;
	
	/** The lbl heap size. */
	private JLabel lblHeapSize;
	
	/** The _heap status. */
	private JProgressBar _heapStatus;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.InteractiveMatchingProcessHandler#setInteractiveMatchingEventsListener(org.fao.fi.comet.core.engine.process.handlers.InteractiveMatchingEventsListener)
	 */
	@Override
	public void setInteractiveMatchingEventsListener(InteractiveMatchingEventsListener<SOURCE, TARGET> listener) {
		this._listener = listener;
	}

	/**
	 * Gets the main panel.
	 *
	 * @return the main panel
	 */
	public JPanel getMainPanel() {
		return this._mainPanel;
	}

	/**
	 * Creates the gui.
	 *
	 * @param id the id
	 * @param standalone the standalone
	 */
	@SuppressWarnings("serial")
	private void createGUI(String id, boolean standalone) {
		if(standalone) {
			try {
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);

				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Throwable t) {
				throw new RuntimeException("Unable to create GUI", t);
			}
		}

		GridBagConstraints gbc_mainPanel = new GridBagConstraints();
		gbc_mainPanel.anchor = GridBagConstraints.WEST;
		gbc_mainPanel.weighty = 100.0;
		gbc_mainPanel.weightx = 100.0;
		gbc_mainPanel.fill = GridBagConstraints.BOTH;
		gbc_mainPanel.gridx = 0;
		gbc_mainPanel.gridy = 0;

		this._mainPanel = new JPanel();
		GridBagLayout gbl_mainPanel = new GridBagLayout();
		this._mainPanel.setLayout(gbl_mainPanel);

		if(standalone) {
			Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

			this._gui = new JFrame(id);

			this._gui.setAutoRequestFocus(false);
			this._gui.setResizable(false);

			this._gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

			this._gui.setSize(new Dimension(604, 400));

			this._gui.setLocation(
				(int)((screen.width - this._gui.getSize().getWidth()) / 2),
				(int)((screen.height - this._gui.getSize().getHeight()) / 2)
			);

			this._gui.setPreferredSize(new Dimension(600, 370));
			this._gui.setMinimumSize(new Dimension(600, 370));

			GridBagLayout gridBagLayout = new GridBagLayout();
			gridBagLayout.columnWidths = new int[] { 600 };
			gridBagLayout.rowHeights = new int[] { 370 };

			this._gui.getContentPane().setLayout(gridBagLayout);
			this._gui.getContentPane().add(this._mainPanel, gbc_mainPanel);
		}

		JPanel _commandPanel = new JPanel();
		GridBagLayout gbl__commandPanel = new GridBagLayout();
		_commandPanel.setLayout(gbl__commandPanel);

		this._halt = new JButton("Halt");

		this._halt.setSize(200, 30);
		this._halt.setPreferredSize(this._halt.getSize());
		this._halt.setMinimumSize(this._halt.getSize());
		this._halt.setMaximumSize(this._halt.getSize());

		GridBagConstraints gbc__halt = new GridBagConstraints();
		gbc__halt.anchor = GridBagConstraints.EAST;
		gbc__halt.fill = GridBagConstraints.HORIZONTAL;
		gbc__halt.insets = new Insets(0, 0, 0, 5);
		gbc__halt.gridx = 1;
		gbc__halt.gridy = 0;
		_commandPanel.add(this._halt, gbc__halt);

		this._halt.addActionListener(this);
		this._dump = new JButton("Dump Results");

		this._dump.setSize(200, 30);
		this._dump.setPreferredSize(this._dump.getSize());
		this._dump.setMinimumSize(this._dump.getSize());
		this._dump.setMaximumSize(this._dump.getSize());
		GridBagConstraints gbc__dump = new GridBagConstraints();
		gbc__dump.anchor = GridBagConstraints.EAST;
		gbc__dump.fill = GridBagConstraints.HORIZONTAL;
		gbc__dump.gridx = 2;
		gbc__dump.gridy = 0;
		_commandPanel.add(this._dump, gbc__dump);
		this._dump.addActionListener(this);

		_comparisonsPanel = new JPanel();
		_comparisonsPanel.setBorder(new TitledBorder(null, "Comparisons",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc__comparisonsPanel = new GridBagConstraints();
		gbc__comparisonsPanel.weightx = 100.0;
		gbc__comparisonsPanel.insets = new Insets(5, 5, 5, 0);
		gbc__comparisonsPanel.fill = GridBagConstraints.BOTH;
		gbc__comparisonsPanel.gridx = 0;
		gbc__comparisonsPanel.gridy = 1;
		this._mainPanel.add(_comparisonsPanel, gbc__comparisonsPanel);
		GridBagLayout gbl__comparisonsPanel = new GridBagLayout();
		_comparisonsPanel.setLayout(gbl__comparisonsPanel);

		label_12 = new JLabel("Current comparisons:");
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.anchor = GridBagConstraints.WEST;
		gbc_label_12.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_12.insets = new Insets(5, 5, 5, 5);
		gbc_label_12.gridx = 0;
		gbc_label_12.gridy = 0;
		_comparisonsPanel.add(label_12, gbc_label_12);

		lblNewLabel_1 = new JLabel("Skipped comparisons:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel_1.gridx = 3;
		gbc_lblNewLabel_1.gridy = 0;
		_comparisonsPanel.add(lblNewLabel_1, gbc_lblNewLabel_1);

		_currentComparisons = new JTextField();
		_currentComparisons.setHorizontalAlignment(SwingConstants.RIGHT);
		_currentComparisons.setText("0");
		_currentComparisons.setEditable(false);
		GridBagConstraints gbc__currentComparisons = new GridBagConstraints();
		gbc__currentComparisons.weightx = 100.0;
		gbc__currentComparisons.anchor = GridBagConstraints.WEST;
		gbc__currentComparisons.insets = new Insets(5, 0, 5, 5);
		gbc__currentComparisons.fill = GridBagConstraints.HORIZONTAL;
		gbc__currentComparisons.gridx = 1;
		gbc__currentComparisons.gridy = 0;
		_comparisonsPanel.add(_currentComparisons, gbc__currentComparisons);
		_currentComparisons.setColumns(10);

		_skippedComparisons = new JTextField();
		_skippedComparisons.setHorizontalAlignment(SwingConstants.RIGHT);
		_skippedComparisons.setText("0");
		_skippedComparisons.setEditable(false);
		GridBagConstraints gbc__skippedComparisons = new GridBagConstraints();
		gbc__skippedComparisons.weightx = 100.0;
		gbc__skippedComparisons.insets = new Insets(5, 0, 5, 0);
		gbc__skippedComparisons.anchor = GridBagConstraints.WEST;
		gbc__skippedComparisons.fill = GridBagConstraints.HORIZONTAL;
		gbc__skippedComparisons.gridx = 4;
		gbc__skippedComparisons.gridy = 0;
		_comparisonsPanel.add(_skippedComparisons, gbc__skippedComparisons);
		_skippedComparisons.setColumns(10);

		horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut_1.gridx = 2;
		gbc_horizontalStrut_1.gridy = 0;
		_comparisonsPanel.add(horizontalStrut_1, gbc_horizontalStrut_1);

		_labelAtomicThroughput = new JLabel("Atomic throughput:");
		GridBagConstraints gbc__labelAtomicThroughput = new GridBagConstraints();
		gbc__labelAtomicThroughput.fill = GridBagConstraints.HORIZONTAL;
		gbc__labelAtomicThroughput.anchor = GridBagConstraints.WEST;
		gbc__labelAtomicThroughput.insets = new Insets(5, 5, 5, 5);
		gbc__labelAtomicThroughput.gridx = 0;
		gbc__labelAtomicThroughput.gridy = 1;
		_comparisonsPanel.add(_labelAtomicThroughput,
				gbc__labelAtomicThroughput);

		_atomicThroughput = new JTextField();
		_atomicThroughput.setEditable(false);
		_atomicThroughput.setHorizontalAlignment(SwingConstants.RIGHT);
		_atomicThroughput.setText("0");
		GridBagConstraints gbc__atomicThroughput = new GridBagConstraints();
		gbc__atomicThroughput.anchor = GridBagConstraints.WEST;
		gbc__atomicThroughput.insets = new Insets(5, 0, 5, 5);
		gbc__atomicThroughput.fill = GridBagConstraints.HORIZONTAL;
		gbc__atomicThroughput.gridx = 1;
		gbc__atomicThroughput.gridy = 1;
		_comparisonsPanel.add(_atomicThroughput, gbc__atomicThroughput);
		_atomicThroughput.setColumns(10);

		_labelThroughput = new JLabel("Throughput:");
		GridBagConstraints gbc__labelThroughput = new GridBagConstraints();
		gbc__labelThroughput.fill = GridBagConstraints.HORIZONTAL;
		gbc__labelThroughput.anchor = GridBagConstraints.WEST;
		gbc__labelThroughput.insets = new Insets(5, 5, 5, 5);
		gbc__labelThroughput.gridx = 3;
		gbc__labelThroughput.gridy = 1;
		_comparisonsPanel.add(_labelThroughput, gbc__labelThroughput);

		_throughput = new JTextField();
		_throughput.setText("0");
		_throughput.setHorizontalAlignment(SwingConstants.RIGHT);
		_throughput.setEditable(false);
		GridBagConstraints gbc__throughput = new GridBagConstraints();
		gbc__throughput.insets = new Insets(5, 0, 5, 0);
		gbc__throughput.anchor = GridBagConstraints.WEST;
		gbc__throughput.fill = GridBagConstraints.HORIZONTAL;
		gbc__throughput.gridx = 4;
		gbc__throughput.gridy = 1;
		_comparisonsPanel.add(_throughput, gbc__throughput);
		_throughput.setColumns(10);
		GridBagConstraints gbc__commandPanel = new GridBagConstraints();
		gbc__commandPanel.weightx = 100.0;
		gbc__commandPanel.insets = new Insets(5, 5, 5, 5);
		gbc__commandPanel.weighty = 5.0;
		gbc__commandPanel.fill = GridBagConstraints.HORIZONTAL;
		gbc__commandPanel.gridx = 0;
		gbc__commandPanel.gridy = 3;
		this._mainPanel.add(_commandPanel, gbc__commandPanel);

		_matchingsPanel = new JPanel();
		_matchingsPanel.setBorder(new TitledBorder(null, "Matchings",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc__matchingsPanel = new GridBagConstraints();
		gbc__matchingsPanel.weightx = 100.0;
		gbc__matchingsPanel.insets = new Insets(5, 5, 5, 0);
		gbc__matchingsPanel.fill = GridBagConstraints.BOTH;
		gbc__matchingsPanel.gridx = 0;
		gbc__matchingsPanel.gridy = 2;
		this._mainPanel.add(_matchingsPanel, gbc__matchingsPanel);
		GridBagLayout gbl__matchingsPanel = new GridBagLayout();
		_matchingsPanel.setLayout(gbl__matchingsPanel);

		label_1 = new JLabel("Authoritative NO MATCHes:");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(5, 5, 5, 5);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 0;
		_matchingsPanel.add(label_1, gbc_label_1);

		_authNoMatches = new JTextField("0");
		_authNoMatches.setEditable(false);
		_authNoMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc__authNoMatches = new GridBagConstraints();
		gbc__authNoMatches.fill = GridBagConstraints.HORIZONTAL;
		gbc__authNoMatches.weightx = 100.0;
		gbc__authNoMatches.anchor = GridBagConstraints.EAST;
		gbc__authNoMatches.insets = new Insets(0, 0, 0, 5);
		gbc__authNoMatches.gridx = 1;
		gbc__authNoMatches.gridy = 0;
		_matchingsPanel.add(_authNoMatches, gbc__authNoMatches);

		label_3 = new JLabel("Authoritative FULL MATCHes:");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.insets = new Insets(5, 5, 5, 5);
		gbc_label_3.gridx = 0;
		gbc_label_3.gridy = 1;
		_matchingsPanel.add(label_3, gbc_label_3);

		_authFullMatches = new JTextField("0");
		_authFullMatches.setEditable(false);
		_authFullMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc__authFullMatches = new GridBagConstraints();
		gbc__authFullMatches.weightx = 100.0;
		gbc__authFullMatches.fill = GridBagConstraints.HORIZONTAL;
		gbc__authFullMatches.anchor = GridBagConstraints.EAST;
		gbc__authFullMatches.insets = new Insets(0, 0, 0, 5);
		gbc__authFullMatches.gridx = 1;
		gbc__authFullMatches.gridy = 1;
		_matchingsPanel.add(_authFullMatches, gbc__authFullMatches);

		label_5 = new JLabel("Identified matches:");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(5, 5, 5, 5);
		gbc_label_5.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_5.anchor = GridBagConstraints.WEST;
		gbc_label_5.gridx = 3;
		gbc_label_5.gridy = 0;
		_matchingsPanel.add(label_5, gbc_label_5);

		_matches = new JTextField("0");
		_matches.setEditable(false);
		_matches.setHorizontalAlignment(SwingConstants.RIGHT);
		_matches.setToolTipText("Current matches");
		GridBagConstraints gbc__matches = new GridBagConstraints();
		gbc__matches.fill = GridBagConstraints.HORIZONTAL;
		gbc__matches.weightx = 100.0;
		gbc__matches.anchor = GridBagConstraints.EAST;
		gbc__matches.gridx = 4;
		gbc__matches.gridy = 0;
		_matchingsPanel.add(_matches, gbc__matches);

		_reducedMatches = new JTextField("0");
		_reducedMatches.setEditable(false);
		_reducedMatches.setHorizontalAlignment(SwingConstants.RIGHT);
		_reducedMatches.setToolTipText("Reduced matches");
		GridBagConstraints gbc__reducedMatches = new GridBagConstraints();
		gbc__reducedMatches.fill = GridBagConstraints.HORIZONTAL;
		gbc__reducedMatches.anchor = GridBagConstraints.EAST;
		gbc__reducedMatches.gridx = 4;
		gbc__reducedMatches.gridy = 1;
		_matchingsPanel.add(_reducedMatches, gbc__reducedMatches);

		horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.fill = GridBagConstraints.HORIZONTAL;
		gbc_horizontalStrut.weighty = 100.0;
		gbc_horizontalStrut.insets = new Insets(0, 0, 0, 5);
		gbc_horizontalStrut.gridx = 2;
		gbc_horizontalStrut.gridy = 0;
		_matchingsPanel.add(horizontalStrut, gbc_horizontalStrut);

		lblNewLabel = new JLabel("Reduced matches:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel.gridx = 3;
		gbc_lblNewLabel.gridy = 1;
		_matchingsPanel.add(lblNewLabel, gbc_lblNewLabel);

		_processPanel = new JPanel();
		_processPanel.setBorder(new TitledBorder(null, "Process",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc__processPanel = new GridBagConstraints();
		gbc__processPanel.weightx = 100.0;
		gbc__processPanel.insets = new Insets(5, 5, 5, 0);
		gbc__processPanel.fill = GridBagConstraints.BOTH;
		gbc__processPanel.gridx = 0;
		gbc__processPanel.gridy = 0;
		this._mainPanel.add(_processPanel, gbc__processPanel);
		GridBagLayout gbl__processPanel = new GridBagLayout();
		_processPanel.setLayout(gbl__processPanel);

		_sourcesDataPanel = new JPanel();
		_sourcesDataPanel.setMinimumSize(new Dimension(600, 32));
		GridBagConstraints gbc__sourcesDataPanel = new GridBagConstraints();
		gbc__sourcesDataPanel.weightx = 100.0;
		gbc__sourcesDataPanel.insets = new Insets(0, 5, 0, 5);
		gbc__sourcesDataPanel.fill = GridBagConstraints.BOTH;
		gbc__sourcesDataPanel.gridx = 0;
		gbc__sourcesDataPanel.gridy = 0;
		_processPanel.add(_sourcesDataPanel, gbc__sourcesDataPanel);
		GridBagLayout gbl__sourcesDataPanel = new GridBagLayout();

		_sourcesDataPanel.setLayout(gbl__sourcesDataPanel);

		label_9 = new JLabel("Processed SOURCE data:");
		label_9.setSize(new Dimension(128, 16));
		label_9.setPreferredSize(new Dimension(128, 16));
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_9.anchor = GridBagConstraints.WEST;
		gbc_label_9.insets = new Insets(5, 0, 5, 5);
		gbc_label_9.gridx = 0;
		gbc_label_9.gridy = 0;
		_sourcesDataPanel.add(label_9, gbc_label_9);

		_sourcesProgress = new JProgressBarExtended();

		_sourcesProgress.setStringPainted(true);
		_sourcesProgress.setMinimum(0);
		GridBagConstraints gbc__sourcesProgress = new GridBagConstraints();
		gbc__sourcesProgress.weightx = 100.0;
		gbc__sourcesProgress.fill = GridBagConstraints.HORIZONTAL;
		gbc__sourcesProgress.insets = new Insets(5, 0, 5, 5);
		gbc__sourcesProgress.gridx = 2;
		gbc__sourcesProgress.gridy = 0;
		_sourcesDataPanel.add(_sourcesProgress, gbc__sourcesProgress);

		_sourcesMax = new JLabel("0");
		_sourcesMax.setSize(new Dimension(64, 16));
		_sourcesMax.setPreferredSize(new Dimension(64, 16));
		GridBagConstraints gbc__sourceMax = new GridBagConstraints();
		gbc__sourceMax.weightx = 10.0;
		gbc__sourceMax.insets = new Insets(5, 5, 5, 0);
		gbc__sourceMax.fill = GridBagConstraints.HORIZONTAL;
		gbc__sourceMax.anchor = GridBagConstraints.EAST;
		gbc__sourceMax.gridx = 3;
		gbc__sourceMax.gridy = 0;
		_sourcesDataPanel.add(_sourcesMax, gbc__sourceMax);

		_sourceMin = new JLabel("0");
		GridBagConstraints gbc__sourceMin = new GridBagConstraints();
		gbc__sourceMin.anchor = GridBagConstraints.EAST;
		gbc__sourceMin.weightx = 10.0;
		gbc__sourceMin.insets = new Insets(0, 0, 0, 5);
		gbc__sourceMin.gridx = 1;
		gbc__sourceMin.gridy = 0;
		_sourcesDataPanel.add(_sourceMin, gbc__sourceMin);

		_targetsDataPanel = new JPanel();
		_targetsDataPanel.setMinimumSize(new Dimension(600, 350));
		GridBagConstraints gbc__targetsDataPanel = new GridBagConstraints();
		gbc__targetsDataPanel.weightx = 100.0;
		gbc__targetsDataPanel.insets = new Insets(0, 5, 0, 5);
		gbc__targetsDataPanel.fill = GridBagConstraints.BOTH;
		gbc__targetsDataPanel.gridx = 0;
		gbc__targetsDataPanel.gridy = 1;
		_processPanel.add(_targetsDataPanel, gbc__targetsDataPanel);
		GridBagLayout gbl__targetsDataPanel = new GridBagLayout();

		_targetsDataPanel.setLayout(gbl__targetsDataPanel);

		label_10 = new JLabel("Processed TARGET data:");
		label_10.setSize(new Dimension(128, 16));
		label_10.setPreferredSize(new Dimension(128, 16));
		GridBagConstraints gbc_label_10 = new GridBagConstraints();
		gbc_label_10.fill = GridBagConstraints.HORIZONTAL;
		gbc_label_10.anchor = GridBagConstraints.WEST;
		gbc_label_10.insets = new Insets(5, 0, 5, 5);
		gbc_label_10.gridx = 0;
		gbc_label_10.gridy = 0;
		_targetsDataPanel.add(label_10, gbc_label_10);

		_targetsProgress = new JProgressBarExtended();
		_targetsProgress.setStringPainted(true);
		_targetsProgress.setMinimum(0);
		GridBagConstraints gbc__targetProgress = new GridBagConstraints();
		gbc__targetProgress.weightx = 100.0;
		gbc__targetProgress.fill = GridBagConstraints.HORIZONTAL;
		gbc__targetProgress.insets = new Insets(5, 0, 5, 5);
		gbc__targetProgress.gridx = 2;
		gbc__targetProgress.gridy = 0;
		_targetsDataPanel.add(_targetsProgress, gbc__targetProgress);

		_targetsMax = new JLabel("0");
		_targetsMax.setSize(new Dimension(64, 16));
		_targetsMax.setPreferredSize(new Dimension(64, 16));
		GridBagConstraints gbc__targetMax = new GridBagConstraints();
		gbc__targetMax.weightx = 10.0;
		gbc__targetMax.insets = new Insets(5, 5, 5, 0);
		gbc__targetMax.fill = GridBagConstraints.HORIZONTAL;
		gbc__targetMax.anchor = GridBagConstraints.EAST;
		gbc__targetMax.gridx = 3;
		gbc__targetMax.gridy = 0;
		_targetsDataPanel.add(_targetsMax, gbc__targetMax);

		_targetMin = new JLabel("0");
		GridBagConstraints gbc__targetMin = new GridBagConstraints();
		gbc__targetMin.anchor = GridBagConstraints.EAST;
		gbc__targetMin.weightx = 10.0;
		gbc__targetMin.insets = new Insets(5, 0, 5, 5);
		gbc__targetMin.gridx = 1;
		gbc__targetMin.gridy = 0;
		_targetsDataPanel.add(_targetMin, gbc__targetMin);

		_statusPanel = new JPanel();
		_statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		GridBagConstraints gbc__statusPanel = new GridBagConstraints();
		gbc__statusPanel.weightx = 100.0;
		gbc__statusPanel.insets = new Insets(5, 5, 5, 5);
		gbc__statusPanel.fill = GridBagConstraints.BOTH;
		gbc__statusPanel.gridx = 0;
		gbc__statusPanel.gridy = 4;
		this._mainPanel.add(_statusPanel, gbc__statusPanel);
		GridBagLayout gbl__statusPanel = new GridBagLayout();
		_statusPanel.setLayout(gbl__statusPanel);

		lblNewLabel_2 = new JLabel("Elapsed:");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(5, 5, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		_statusPanel.add(lblNewLabel_2, gbc_lblNewLabel_2);

		_elapsed = new JLabel("00h:00m:00s");
		_elapsed.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc__elapsed = new GridBagConstraints();
		gbc__elapsed.fill = GridBagConstraints.HORIZONTAL;
		gbc__elapsed.weightx = 100.0;
		gbc__elapsed.insets = new Insets(5, 5, 5, 5);
		gbc__elapsed.gridx = 1;
		gbc__elapsed.gridy = 0;
		_statusPanel.add(_elapsed, gbc__elapsed);

		lblHeapSize = new JLabel("Used memory:");
		GridBagConstraints gbc_lblHeapSize = new GridBagConstraints();
		gbc_lblHeapSize.gridy = 0;
		gbc_lblHeapSize.insets = new Insets(5, 5, 5, 5);
		gbc_lblHeapSize.gridx = 3;
		_statusPanel.add(lblHeapSize, gbc_lblHeapSize);

		_heapStatus = new JProgressBar() {
			@Override
			public String getString() {
				return DOUBLE_2_FORMATTER.format(this.getPercentComplete() * 100.0) + "% of " + DOUBLE_2_FORMATTER.format(this.getMaximum() / 1024.0 / 1024.0) + "MB";
			}
		};

		_heapStatus.setStringPainted(true);
		GridBagConstraints gbc__heapStatus = new GridBagConstraints();
		gbc__heapStatus.insets = new Insets(5, 5, 5, 5);
		gbc__heapStatus.gridx = 4;
		gbc__heapStatus.gridy = 0;
		_statusPanel.add(_heapStatus, gbc__heapStatus);

		if(standalone)
			this._gui.setVisible(true);

		this._GUIAlreadySetUp = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (this._halt.equals(e.getSource())) {
			this._halt.setEnabled(false);

			this.halt();

			if(this._results != null)
				synchronized(this._results) {
					this._listener.notifyHalt(this._results);
				}
		} else if (this._dump.equals(e.getSource())) {
			if(this._results != null)
				synchronized(this._results) {
					this._listener.notifyDump(this._results);
				}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyNumberOfMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfMatchesChanged(int matches) {
		this._matches.setText(String.valueOf(matches));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler
	 * #doNotifyNumberOfAuthoritativeFullMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfAuthoritativeFullMatchesChanged(int matches) {
		this._authFullMatches.setText(String.valueOf(matches));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler
	 * #doNotifyNumberOfAuthoritativeNoMatchesChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfAuthoritativeNoMatchesChanged(int matches) {
		this._authNoMatches.setText(String.valueOf(matches));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler
	 * #doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int)
	 */
	@Override
	protected void doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int comparisons) {
		this._sourcesProgress.setMaximum(comparisons);
		this._sourcesProgress.setValue(0);

		this._sourcesMax.setText(String.valueOf(comparisons));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyAtomicComparisonStart()
	 */
	@Override
	protected void doNotifyAtomicComparisonStart() {
		this._sourcesProgress.setValue(0);

		synchronized(this) {
			this._atomicThroughput.setText(DOUBLE_5_FORMATTER.format(this.getAtomicThroughput() * 1000.0));
			this._throughput.setText(DOUBLE_5_FORMATTER.format(this.getThroughput() * 1000.0));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyAtomicComparisonPerformed()
	 */
	@Override
	protected void doNotifyAtomicComparisonPerformed() {
		this._sourcesProgress.setValue(this.getCurrentNumberOfAtomicComparisonsPerformedInRound());

		synchronized (this) {
			this._currentComparisons.setText(String.valueOf(this.getTotalNumberOfAtomicComparisonsPerformed()));
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyAtomicComparisonSkipped()
	 */
	@Override
	protected void doNotifyAtomicComparisonSkipped() {
		synchronized (this) {
			this._skippedComparisons.setText(String.valueOf(this.getNumberOfAtomicComparisonsSkipped()));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyNumberOfComparisonRoundsChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfComparisonRoundsChanged(int rounds) {
		this._targetsProgress.setMaximum(rounds);
		this._targetsProgress.setValue(0);

		this._targetsMax.setText(String.valueOf(rounds));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler
	 * #doNotifyComparisonRoundStart(org.fao.fi.comet
	 * .core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundStart(ProvidedData<TARGET> source) {
		// this._sourcesProgress.setValue(0);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler
	 * #doNotifyComparisonRoundPerformed(org.fao.fi.comet
	 * .core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundPerformed(ProvidedData<TARGET> source) {
		this._targetsProgress.setValue(this.getCurrentNumberOfComparisonRoundsPerformed());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyProcessStartEvent(java.lang.String)
	 */
	@Override
	protected void doNotifyProcessStartEvent(String processId) {
		this._isInitialized = false;

		if (!this._GUIAlreadySetUp) {
			this._GUIAlreadySetUp = true;

			this.createGUI(processId, false);

			this._isInitialized = true;

			TIMER = new Timer();
			TIMER.schedule(new StatusUpdateTask(this, this._elapsed, this._heapStatus), 0, 1000);
		}

		if (this._gui != null && this._GUIAlreadySetUp) {
			this._gui.setTitle(processId);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.
	 * SilentMatchingProcessHandler#doNotifyProcessEndEvent()
	 */
	@Override
	protected void doNotifyProcessEndEvent() {
		if(this._gui != null)
			this._gui.setTitle(this._gui.getTitle() + " : COMPLETED");

		TIMER.cancel();
	}

	/**
	 * The Class JProgressBarExtended.
	 */
	private class JProgressBarExtended extends JProgressBar {
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 6422472539939526817L;

		/* (non-Javadoc)
		 * @see javax.swing.JProgressBar#getString()
		 */
		@Override
		public String getString() {
			return this.getValue() + " (" + DOUBLE_2_FORMATTER.format(this.getPercentComplete() * 100D) + "%)";
		}
	}

	/**
	 * The Class StatusUpdateTask.
	 */
	private class StatusUpdateTask extends TimerTask {
		
		/** The _handler. */
		private MatchingProcessHandler<SOURCE, TARGET> _handler;
		
		/** The _elapsed. */
		private JLabel _elapsed;
		
		/** The _heap. */
		private JProgressBar _heap;

		/**
		 * Instantiates a new status update task.
		 *
		 * @param handler the handler
		 * @param elapsed the elapsed
		 * @param heap the heap
		 */
		public StatusUpdateTask(MatchingProcessHandler<SOURCE, TARGET> handler, JLabel elapsed, JProgressBar heap) {
			this._handler = handler;
			this._elapsed = elapsed;
			this._heap = heap;

			this._heap.setMinimum(0);
			this._heap.setMaximum((int)Runtime.getRuntime().maxMemory());
		}

		/* (non-Javadoc)
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			long elapsed = this._handler.getElapsed();

			long max = Runtime.getRuntime().maxMemory();
			long used = max - Runtime.getRuntime().freeMemory();

			this._heap.setMaximum((int)max);
			this._heap.setValue((int)used);

			long hours = elapsed / 1000 / 3600;
			long minutes = ( elapsed - ( hours * 1000 * 3600 ) ) / 1000 / 60 ;
			long seconds = ( elapsed - ( hours * 1000 * 3600 ) - ( minutes * 1000 * 60) ) / 1000;

			this._elapsed.setText(INTEGER_2_FORMATTER.format(hours) + "h:" +
								  INTEGER_2_FORMATTER.format(minutes) + "m:" +
								  INTEGER_2_FORMATTER.format(seconds) + "s");
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws Throwable the throwable
	 */
	final static public void main(String[] args) throws Throwable {
		new SimpleGUIMatchingProcessHandler<Serializable, Serializable>().createGUI("foobar", true);
	}
}