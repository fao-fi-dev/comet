/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.matchlets.skeleton.behaviours;

import org.fao.fi.comet.core.model.score.support.MatchingScore;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <SOURCE_DATA> the generic type
 * @param <TARGET> the generic type
 * @param <TARGET_DATA> the generic type
 * 
 */
public class BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> extends BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = 6487504734829476930L;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton#doUpdateScore(org.fao.fi.comet.core.support.MatchingScore, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingScore doUpdateScore(MatchingScore current, SOURCE_DATA sourceData, TARGET_DATA targetData) {
		return new MatchingScore(current);
	}
}