/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.exceptions;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * 
 */
public class MultipleNamedIdentifiersException extends IdentifierException {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = 461833166714404070L;

	/**
	 * Class constructor.
	 */
	public MultipleNamedIdentifiersException() {
		super();
	}

	/**
	 * Class constructor.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public MultipleNamedIdentifiersException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor.
	 *
	 * @param message the message
	 */
	public MultipleNamedIdentifiersException(String message) {
		super(message);
	}

	/**
	 * Class constructor.
	 *
	 * @param cause the cause
	 */
	public MultipleNamedIdentifiersException(Throwable cause) {
		super(cause);
	}

}
