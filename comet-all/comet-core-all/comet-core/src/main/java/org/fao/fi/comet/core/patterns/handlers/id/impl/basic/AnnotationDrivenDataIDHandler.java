/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.handlers.id.impl.basic;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.fao.fi.comet.core.model.common.annotations.Identifier;
import org.fao.fi.comet.core.patterns.handlers.id.exceptions.IdentifierException;
import org.fao.fi.comet.core.patterns.handlers.id.exceptions.MultipleIdentifierAnnotationsException;
import org.fao.fi.comet.core.patterns.handlers.id.exceptions.NoIdentifierAnnotationsException;
import org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <DATA> the generic type
 * 
 */
public class AnnotationDrivenDataIDHandler<DATA> extends IDHandlerSkeleton<DATA, Serializable>{
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.io.Serializable)
	 */
	@Override
	final protected Serializable doGetId(DATA data) {
		this.checkAnnotations($nN(data, "Data cannot be NULL"));

		Field[] annotatedFields = this.getAnnotatedFields(data);

		try {
			Map<Class<?>, Set<Field>> classesToFieldMap = new HashMap<Class<?>, Set<Field>>();

			Class<?> fieldClass;

			for(Field field : annotatedFields) {
				fieldClass = field.getDeclaringClass();

				if(!classesToFieldMap.containsKey(fieldClass)) {
					classesToFieldMap.put(fieldClass, new HashSet<Field>());
				}

				classesToFieldMap.get(fieldClass).add(field);
			}

			if(annotatedFields != null && annotatedFields.length > 0) {
				for(Class<?> clazz : classesToFieldMap.keySet())
					if(classesToFieldMap.get(clazz).size() > 1)
						throw new MultipleIdentifierAnnotationsException("Multiple 'identifier' annotations found on fields of class " + clazz);

				return this.getId(data, annotatedFields[0]);
			}

			return this.getId(data, this.getAnnotatedMethods(data)[0]);
		} catch (IdentifierException Ie) {
			throw Ie;
		} catch (Throwable t) {
			throw new RuntimeException("Unable to retrieve ID by annotation on " + data);
		}
	}

	/**
	 * Gets the id.
	 *
	 * @param object the object
	 * @param field the field
	 * @return the id
	 * @throws Throwable the throwable
	 */
	private Serializable getId(DATA object, Field field) throws Throwable {
		return ((Serializable)field.get(object));
	}

	/**
	 * Gets the id.
	 *
	 * @param object the object
	 * @param method the method
	 * @return the id
	 * @throws Throwable the throwable
	 */
	private Serializable getId(DATA object, Method method) throws Throwable {
		return ((Serializable)method.invoke(object));
	}

	/**
	 * Check annotations.
	 *
	 * @param object the object
	 */
	private void checkAnnotations(DATA object) {
		$nN(object, "Provided object cannot be null");

		Field[] annotatedFields = this.getAnnotatedFields(object);
		Method[] annotatedMethods = this.getAnnotatedMethods(object);

		int numAnnotatedFields = annotatedFields.length;
		int numAnnotatedMethods = annotatedMethods.length;

//		if(numAnnotatedFields + numAnnotatedMethods > 1)
//			throw new MultipleIdentifierAnnotationsException("Multiple @" + Identifier.class.getSimpleName() + " field or getter methods annotations found for object of class " + object.getClass().getSimpleName() + ". Currently: " + numAnnotatedFields + " annotated fields and " + numAnnotatedMethods + " annotated getter methods");

		if(numAnnotatedFields == 0 && numAnnotatedMethods == 0)
			throw new NoIdentifierAnnotationsException("No @" + Identifier.class.getSimpleName() + " annotations found for fields of type (or getter methods returning an instance of) java.lang.Serializable for object of class " + object.getClass().getSimpleName() + ".");
	}

	/**
	 * Gets the annotated fields.
	 *
	 * @param object the object
	 * @return the annotated fields
	 */
	private Field[] getAnnotatedFields(DATA object) {
		if(object == null) return null;

		Collection<Field> fields = new ArrayList<Field>();

		for(Field field : ObjectsHelper.listAllFields(object)) {
			field.setAccessible(true);

			if(field.isAnnotationPresent(Identifier.class) && Serializable.class.isAssignableFrom(field.getType()))
				fields.add(field);
		}

		return fields.toArray(new Field[fields.size()]);
	}

	/**
	 * Gets the annotated methods.
	 *
	 * @param object the object
	 * @return the annotated methods
	 */
	private Method[] getAnnotatedMethods(DATA object) {
		if(object == null)
			return null;

		Collection<Method> methods = new ArrayList<Method>();

		for(Method method : ObjectsHelper.getAllGetterMethods(object.getClass(), Serializable.class)) {
			method.setAccessible(true);

			if(method.isAnnotationPresent(Identifier.class))
				methods.add(method);
		}

		return methods.toArray(new Method[methods.size()]);
	}
}