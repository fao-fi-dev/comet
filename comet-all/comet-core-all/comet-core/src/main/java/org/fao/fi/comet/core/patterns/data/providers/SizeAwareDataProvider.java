/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.providers;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <ENTITY> the generic type
 * 
 */
public interface SizeAwareDataProvider<ENTITY> extends DataProvider<ENTITY> {
	
	/**
	 * Gets the available data size.
	 *
	 * @return the available data size
	 */
	int getAvailableDataSize();
}
