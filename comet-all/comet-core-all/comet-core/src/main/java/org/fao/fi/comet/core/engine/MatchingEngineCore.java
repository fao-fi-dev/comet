/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.fao.fi.comet.core.engine.process.AtomicComparisonProcess;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider;
import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;

// TODO: Auto-generated Javadoc
/**
 * The generic matching engine
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * @param <CONFIG> the generic type
 * 
 */
public class MatchingEngineCore<SOURCE extends Serializable,
								TARGET extends Serializable,
								CONFIG extends MatchingEngineProcessConfiguration> extends AbstractMatchingEngineCore<SOURCE, TARGET, CONFIG> {
	
	/** The Constant MAX_THREADS_IN_POOL. */
	final static public int MAX_THREADS_IN_POOL = 64;

	/** The _executor queue. */
	private ExecutorCompletionService<Matching<SOURCE, TARGET>> _executorQueue;
	
	/** The _thread pool. */
	private ExecutorService _threadPool;

	/** The _parallel threads. */
	private int _parallelThreads = Runtime.getRuntime().availableProcessors();
	
	/**
	 * Class constructor.
	 */
	public MatchingEngineCore() {
		this(Runtime.getRuntime().availableProcessors());
	}

	/**
	 * Class constructor.
	 *
	 * @param parallelThreads the parallel threads
	 */
	public MatchingEngineCore(int parallelThreads) {
		super();

		$lte(parallelThreads, MAX_THREADS_IN_POOL, "The number of parallel threads ({}) must be lower than or equal to the maximum available ({})", parallelThreads, MAX_THREADS_IN_POOL);

		this._log.info("Using {} as number of parallel threads for computation", parallelThreads);

		this._parallelThreads = parallelThreads;
		this._threadPool = Executors.newFixedThreadPool(parallelThreads);
		this._executorQueue = new ExecutorCompletionService<Matching<SOURCE, TARGET>>(this._threadPool);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.AbstractMatchingEngineCore#afterCompletionCallback()
	 */
	@Override
	protected void afterCompletionCallback() {
		this._threadPool.shutdownNow();
	}
	
	/**
	 * Gets the parallel threads.
	 *
	 * @return the 'parallelThreads' value
	 */
	public final int getParallelThreads() {
		return this._parallelThreads;
	}

	/**
	 * Compares a data source with the current domain.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param currentMatchingsData the current matchings data
	 * @param sources the sources
	 * @param target the target
	 * @param targetProviderId the target provider id
	 * @param partitioner the partitioner
	 * @param matchlets the matchlets
	 * @param sourceIDHandler the source id handler
	 * @param targetIDHandler the target id handler
	 * @return the matchings data
	 */
	final protected MatchingsData<SOURCE, TARGET> performComparison(CONFIG configuration,
																	MatchingProcessHandler<SOURCE, TARGET> tracker,
																	MatchingsData<SOURCE, TARGET> currentMatchingsData,
																	DataProvider<SOURCE> sources,
																	TARGET target,
																	String targetProviderId,
																	DataPartitioner<SOURCE, TARGET> partitioner,
																	Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets,
																	IDHandler<SOURCE, ?> sourceIDHandler,
																	IDHandler<TARGET, ?> targetIDHandler) {
		Matching<SOURCE, TARGET> matching = null;

		//Lifecycle management
		if(sources instanceof SizeAwareDataProvider)
			tracker.setMaximumNumberOfAtomicComparisonsPerformedInRound(((SizeAwareDataProvider<SOURCE>)sources).getAvailableDataSize());
		
		boolean hasMatched = false;

		final DataIdentifier targetIdentifier = new DataIdentifier(targetProviderId, targetIDHandler.getSerializedId(target));
		DataIdentifier sourceIdentifier;

		final int maxCandidatesPerEntry = configuration.getMaxCandidatesPerEntry();
		final boolean allCandidatesPerEntry = maxCandidatesPerEntry == MatchingEngineProcessConfiguration.MAX_CANDIDATES_PER_ENTRY_UNBOUND;

		final boolean haltAtFirstMatching = configuration.getHaltAtFirstValidMatching() == null ? Boolean.FALSE : configuration.getHaltAtFirstValidMatching();

		final boolean handleErrors = configuration.getHandleErrors();

		int actualComparisons = 0;

		AtomicComparisonProcess<SOURCE, TARGET, CONFIG> process = null;
		
		//Cycle on each current target entity
		for(ProvidedData<SOURCE> source : sources) {
			if(partitioner.include(source.getData(), target, matchlets)) {
				sourceIdentifier = new DataIdentifier(source.getProviderId(), sourceIDHandler.getSerializedId(source.getData()));
				
				process = new AtomicComparisonProcess<SOURCE, TARGET, CONFIG>(this,
																			  currentMatchingsData,
																			  matchlets,
																			  configuration,
																			  tracker,
																			  source,
																			  target,
																			  targetProviderId,
																			  sourceIDHandler,
																			  targetIDHandler);
				if(actualComparisons <= this._parallelThreads) {
					this._executorQueue.submit(process);
					
					actualComparisons++;
				} 

				if(actualComparisons == this._parallelThreads) {
					Future<Matching<SOURCE, TARGET>> future;
					
					threadCycle: for(int t=0; t<actualComparisons; t++) {
						try {
							future = this._executorQueue.take();
							matching = future.get();
							
							future.cancel(true);
	
							if(matching != null) {
								sourceIdentifier = matching.getSourceIdentifier();
	
								if(!matching.isNonPerformed() && Double.compare(matching.getScore().getValue(), configuration.getMinimumAllowedWeightedScore()) >= 0) {
									//Lifecycle management
									tracker.notifyMatch();
	
									hasMatched = true;
	
									MatchingDetails<SOURCE, TARGET> matchingDetails = currentMatchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);
	
									if(matchingDetails != null) {
										if(!allCandidatesPerEntry)
											matchingDetails = matchingDetails.retain(maxCandidatesPerEntry, currentMatchingsData.MATCHINGS_COMPARATOR);
	
										if(matchingDetails.getMatchings() == null || matchingDetails.getMatchings().isEmpty() )
											currentMatchingsData.removeMatchingDetailsBySourceIdentifier(sourceIdentifier);
									}
								} else {
									this.doRemoveMatching(currentMatchingsData, sourceIdentifier, targetIdentifier);
								}
								
								tracker.updateCurrentResults(currentMatchingsData);
							} else {
								//NULL matchings are returned when the matching score is lower than
								//the engine currently set threshold or when the source - target partitioning excludes the comparison
							}
	
							//Exits the domain cycle if a matching has been detected and the engine is configured to halt at first matching
							if(hasMatched && haltAtFirstMatching)
								break threadCycle;
						} catch(ExecutionException Ee) {
							if(handleErrors)
								tracker.notifyComparisonError();
							else if(!InterruptedException.class.equals(Ee.getCause().getClass())) {
								throw new RuntimeException(Ee);
							}
						} catch(InterruptedException Ie) {
							break threadCycle;
						}
					}
				
					actualComparisons = 0;
				}
			} else {
				tracker.notifyAtomicComparisonSkipped();
				tracker.notifyAtomicComparisonPerformed();
			}
		}
		
		threadCycle: for(int t=0; t<actualComparisons; t++) {
			try {
				matching = this._executorQueue.take().get();

				if(matching != null) {
					sourceIdentifier = matching.getSourceIdentifier();

					if(!matching.isNonPerformed() && Double.compare(matching.getScore().getValue(), configuration.getMinimumAllowedWeightedScore()) >= 0) {
						//Lifecycle management
						tracker.notifyMatch();

						hasMatched = true;

						MatchingDetails<SOURCE, TARGET> matchingDetails = currentMatchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);

						if(matchingDetails != null) {
							if(!allCandidatesPerEntry)
								matchingDetails = matchingDetails.retain(maxCandidatesPerEntry, currentMatchingsData.MATCHINGS_COMPARATOR);

							if(matchingDetails.getMatchings() == null || matchingDetails.getMatchings().isEmpty() )
								currentMatchingsData.removeMatchingDetailsBySourceIdentifier(sourceIdentifier);
						}
					} else {
						this.doRemoveMatching(currentMatchingsData, sourceIdentifier, targetIdentifier);
					}
					
					tracker.updateCurrentResults(currentMatchingsData);
				} else {
					//NULL matchings are returned when the matching score is lower than
					//the engine currently set threshold or when the source - target partitioning excludes the comparison
				}

				//Exits the domain cycle if a matching has been detected and the engine is configured to halt at first matching
				if(hasMatched && haltAtFirstMatching)
					break threadCycle;
			} catch(ExecutionException Ee) {
				if(handleErrors)
					tracker.notifyComparisonError();
				else if(!InterruptedException.class.equals(Ee.getCause().getClass())) {
					throw new RuntimeException(Ee);
				}
			} catch(InterruptedException Ie) {
				break threadCycle;
			}
		}

		if(sources instanceof StreamingDataProvider) {
			StreamingDataProvider<SOURCE> stream = (StreamingDataProvider<SOURCE>)sources;

			if(stream.isRewindable())
				stream.rewind();
			
		}

//		this._threadPool.shutdownNow();
//		
//		this._threadPool = Executors.newFixedThreadPool(this._parallelThreads);
//		this._executorQueue = new ExecutorCompletionService<Matching<SOURCE, TARGET>>(this._threadPool);

		return currentMatchingsData;
	}
}