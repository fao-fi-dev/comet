/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.matchlets.skeleton;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingResultData;
import org.fao.fi.comet.core.model.matchlets.VectorialMatchlet;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.StringValidValues;
import org.fao.fi.comet.core.model.score.support.MatchingScore;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <SOURCE_DATA> the generic type
 * @param <TARGET> the generic type
 * @param <TARGET_DATA> the generic type
 * 
 */
abstract public class VectorialMatchletSkeleton<SOURCE, SOURCE_DATA extends Serializable, TARGET, TARGET_DATA extends Serializable> 
		 extends MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> 
		 implements VectorialMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	
	/**  Field serialVersionUID. */
	private static final long serialVersionUID = -858237716778011026L;

	/** The _combination criteria. */
	@MatchletParameter(name=COMBINATION_CRITERIA_PARAM,
					   description="Sets the combination criteria to derive the final score for source data matching with multiple target data. " +
					  			   "If set to " + COMBINATION_CRITERIA_OR + ", the highest-valued matching result will be returned at the end of the process. " +
					  			   "Conversely, when set to " + COMBINATION_CRITERIA_AND + ", an authoritative match will halt this' matchlet comparisons and be returned as final match result.")
	@StringValidValues(values={ COMBINATION_CRITERIA_OR, COMBINATION_CRITERIA_AND }, multiple=false)
	private String _combinationCriteria = COMBINATION_CRITERIA_OR;

	/**
	 * Class constructor.
	 */
	public VectorialMatchletSkeleton() {
		this(new BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>());
	}

	/**
	 * Class constructor.
	 *
	 * @param behaviour the behaviour
	 */
	public VectorialMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		try {
			$true(VectorialMatchlet.COMBINATION_CRITERIA_AND.equals(this._combinationCriteria) ||
				  VectorialMatchlet.COMBINATION_CRITERIA_OR.equals(this._combinationCriteria),
				  "The combination criteria must be one among { {}, {} } (currently: {})",
				  VectorialMatchlet.COMBINATION_CRITERIA_AND,
				  VectorialMatchlet.COMBINATION_CRITERIA_OR,
				  this._combinationCriteria);
		} catch (IllegalArgumentException IAe) {
			throw new MatchletConfigurationException(IAe.getMessage());
		}
	}

	/**
	 * Gets the combination criteria.
	 *
	 * @return the 'combinationCriteria' value
	 */
	public String getCombinationCriteria() {
		return this._combinationCriteria;
	}

	/**
	 * Sets the combination criteria.
	 *
	 * @param combinationCriteria the 'combinationCriteria' value to set
	 */
	public void setCombinationCriteria(String combinationCriteria) {
		this._combinationCriteria = combinationCriteria;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#compareData(org.fao.vrmf.core.behaviours.data.Identifiable, org.fao.vrmf.core.behaviours.data.Identifiable)
	 */
	@Override
	public MatchingResult<SOURCE_DATA, TARGET_DATA> compareData(SOURCE sourceEntity, DataIdentifier sourceIdentifier, TARGET targetEntity, DataIdentifier targetIdentifier) {
		$nN(sourceEntity, "Source entity cannot be null");
		$nN(sourceIdentifier, "Source entity identifier cannot be null");
		$nN(targetEntity, "Target entity cannot be null");
		$nN(targetIdentifier, "Target entity identifier cannot be null");

		Collection<SOURCE_DATA> sourceData = this.extractSourceData(sourceEntity, sourceIdentifier);
		Collection<TARGET_DATA> targetData = this.extractTargetData(targetEntity, targetIdentifier);

		boolean noSourceData = sourceData == null;
		boolean noTargetData = targetData == null;

		boolean emptySourceData = noSourceData || sourceData.isEmpty();
		boolean emptyTargetData = noTargetData || targetData.isEmpty();

		boolean dontPerformMatching = noSourceData && noTargetData;
		boolean assertNoMatch = ( emptySourceData || emptyTargetData );

		MatchingResult<SOURCE_DATA, TARGET_DATA> result = this.newMatchingResult();

		//Matching cannot be performed (no data to compare)
		if(dontPerformMatching) {
			result.setScore(MatchingScore.getNonPerformedTemplate());

			return result;
		//Perform the match
		} else if(!assertNoMatch) {
			MatchingResult<SOURCE_DATA, TARGET_DATA> temporaryResult = null;

			MatchingScore currentScore = null;

			for(SOURCE_DATA currentSourceData : sourceData) {
				for(TARGET_DATA currentTargetData : targetData) {
					if(currentSourceData == null && currentTargetData == null) {
						currentScore = MatchingScore.getNonPerformedTemplate();
					} else if(currentSourceData == null || currentTargetData == null) {
						currentScore = MatchingScore.getNonAuthoritativeNoMatchTemplate();
					} else {
						currentScore = this._behaviour.updateScore(this.computeScore(sourceEntity, sourceIdentifier, currentSourceData, targetEntity, targetIdentifier, currentTargetData), sourceEntity, currentSourceData, targetEntity, currentTargetData);
					}

					temporaryResult = this.updateResult(new MatchingResult<SOURCE_DATA, TARGET_DATA>(this), currentScore, currentSourceData, sourceIdentifier, currentTargetData);

					if(temporaryResult.isAuthoritative() && ( COMBINATION_CRITERIA_AND.equals(this._combinationCriteria ) ))
						return temporaryResult;

					if(!temporaryResult.isNonPerformed()) {
						//Currently compared data have a higher score: replace the result with the one for the currently compared data
						if(Double.compare(temporaryResult.getScore().getValue(), result.getScore().getValue()) > 0) {
							result = temporaryResult;
						} else if(Double.compare(temporaryResult.getScore().getValue(), result.getScore().getValue()) == 0) {
							//Currently compared data have the same score: update the result by adding / merging the currently compared data
							MatchingResultData<SOURCE_DATA, TARGET_DATA> matching = null;

							if(result.containsMatchingFor(currentSourceData, sourceIdentifier) && currentTargetData != null) {
								matching = result.getMatchingsResultDataFor(currentSourceData, sourceIdentifier);

								matching.getMatchingTargetData().add(currentTargetData);
							} else {
								result.getMatchingsResultData().addAll(temporaryResult.getMatchingsResultData());
							}

							result.getScore().setMatchingType(temporaryResult.getScore().getMatchingType());
						} else {
							; //DO NOTHING: computed score is lower than currently set one
						}
					}
				}
			}

			return result;
		} else {
			if(this.isOptional())
				result.setScore(MatchingScore.getNonPerformedTemplate());
			else
				result.setScore(MatchingScore.getNonAuthoritativeNoMatchTemplate());

			return result;
		}
	}
}