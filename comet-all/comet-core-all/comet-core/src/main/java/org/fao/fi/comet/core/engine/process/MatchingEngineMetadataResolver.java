/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.ServiceLoader;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeTo;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleValidValues;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.FloatRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.FloatRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.FloatRangeTo;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.FloatValidValues;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRangeTo;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntValidValues;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRangeTo;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongValidValues;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.NotNull;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.StringValidValues;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint;
import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterInfo;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterDoubleRangeConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterDoubleValuesConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterFloatRangeConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterFloatValuesConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterIntRangeConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterIntValuesConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterLongRangeConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterLongValuesConstraint;
import org.fao.fi.comet.core.model.matchlets.support.constraints.MatchletParameterStringValuesConstraint;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

import net.jodah.typetools.TypeResolver;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 */
final public class MatchingEngineMetadataResolver {
	
	/**
	 * Class constructor.
	 */
	public MatchingEngineMetadataResolver() {
	}
	
	/**
	 * Gets the available matchlets info.
	 *
	 * @param type the type
	 * @return the available matchlets info
	 */
	@SuppressWarnings("rawtypes")
	public Collection<MatchletInfo> getAvailableMatchletsInfo(Class<? extends Matchlet> type) {
		ServiceLoader<? extends Matchlet> loader = ServiceLoader.load(type);

		Collection<MatchletInfo> matchletInfo = new ArrayList<MatchletInfo>();

		for(Matchlet matchlet : loader) {
			matchletInfo.add(this.getMatchletInfo(matchlet));
		}

		return matchletInfo;
	}

	/**
	 * Gets the matchlet info.
	 *
	 * @param matchlet the matchlet
	 * @return the matchlet info
	 */
	@SuppressWarnings("rawtypes")
	public MatchletInfo getMatchletInfo(Matchlet matchlet) {
		Class<?>[] concreteTypes = TypeResolver.resolveRawArguments(Matchlet.class, matchlet.getClass());

		String sourceDataType, targetDataType;

		if(concreteTypes.length == 4) {
			sourceDataType = concreteTypes[1].getName();
			targetDataType = concreteTypes[3].getName();
		} else {
			sourceDataType = targetDataType = "java.lang.Object";
		}

		MatchletInfo toReturn = new MatchletInfo();
		toReturn.setMatchletName(matchlet.getName());
		toReturn.setMatchletDescription(matchlet.getDescription());
		toReturn.setMatchletType(matchlet.getClass().getName());
		toReturn.setExtractedSourceDataType(sourceDataType);
		toReturn.setExtractedTargetDataType(targetDataType);
		toReturn.setMatchletParameters(MatchingEngineMetadataResolver.getMatchletParameterInfo(ObjectsHelper.getAllAnnotatedFields(matchlet, MatchletParameter.class)));

		return toReturn;
	}

	/**
	 * Gets the matchlet parameter info.
	 *
	 * @param matchletParameterFields the matchlet parameter fields
	 * @return the matchlet parameter info
	 */
	static public Collection<MatchletParameterInfo> getMatchletParameterInfo(Collection<Field> matchletParameterFields) {
		Collection<MatchletParameterInfo> matchletParameters = new ArrayList<MatchletParameterInfo>();

		MatchletParameter annotation;
		MatchletParameterInfo info;
		for(Field field : matchletParameterFields) {
			field.setAccessible(true);

			annotation = field.getAnnotation(MatchletParameter.class);

			info = new MatchletParameterInfo();
			info.setParameterName(annotation.name() == null || "".equals(annotation.name()) ? field.getName() : annotation.name());
			info.setParameterIsMandatory(annotation.mandatory());
			info.setParameterType(field.getType().getName());
			info.setParameterDescription(annotation.description());
			info.setConstraint(MatchingEngineMetadataResolver.getMatchletParameterConstraint(field));

			matchletParameters.add(info);
		}

		return matchletParameters;
	}

	/**
	 * Gets the matchlet parameter constraint.
	 *
	 * @param field the field
	 * @return the matchlet parameter constraint
	 */
	static public MatchletParameterConstraint getMatchletParameterConstraint(Field field) {
		MatchletParameterConstraint constraint = null;

		if(field.isAnnotationPresent(NotNull.class))
			constraint = new MatchletParameterConstraint();

		if(field.isAnnotationPresent(StringValidValues.class))
			constraint = new MatchletParameterStringValuesConstraint();
		else if(field.isAnnotationPresent(IntValidValues.class))
			constraint = new MatchletParameterIntValuesConstraint();
		else if(field.isAnnotationPresent(LongValidValues.class))
			constraint = new MatchletParameterLongValuesConstraint();
		else if(field.isAnnotationPresent(FloatValidValues.class))
			constraint = new MatchletParameterFloatValuesConstraint();
		else if(field.isAnnotationPresent(DoubleValidValues.class))
			constraint = new MatchletParameterDoubleValuesConstraint();
		else if(field.isAnnotationPresent(IntRange.class))
			constraint = new MatchletParameterIntRangeConstraint();
		else if(field.isAnnotationPresent(IntRangeFrom.class))
			constraint = new MatchletParameterFloatRangeConstraint();
		else if(field.isAnnotationPresent(IntRangeTo.class))
			constraint = new MatchletParameterIntRangeConstraint();
		else if(field.isAnnotationPresent(LongRange.class))
			constraint = new MatchletParameterLongRangeConstraint();
		else if(field.isAnnotationPresent(LongRangeFrom.class))
			constraint = new MatchletParameterLongRangeConstraint();
		else if(field.isAnnotationPresent(LongRangeTo.class))
			constraint = new MatchletParameterLongRangeConstraint();
		else if(field.isAnnotationPresent(FloatRange.class))
			constraint = new MatchletParameterFloatRangeConstraint();
		else if(field.isAnnotationPresent(FloatRangeFrom.class))
			constraint = new MatchletParameterFloatRangeConstraint();
		else if(field.isAnnotationPresent(FloatRangeTo.class))
			constraint = new MatchletParameterFloatRangeConstraint();
		else if(field.isAnnotationPresent(DoubleRange.class))
			constraint = new MatchletParameterDoubleRangeConstraint();
		else if(field.isAnnotationPresent(DoubleRangeFrom.class))
			constraint = new MatchletParameterDoubleRangeConstraint();
		else if(field.isAnnotationPresent(DoubleRangeTo.class))
			constraint = new MatchletParameterDoubleRangeConstraint();

		if(constraint != null) {
			constraint.initializeFromField(field);
		}

		return constraint;
	}
}
