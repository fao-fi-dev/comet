/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine.process.handlers.impl;

import java.io.Serializable;

import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;


// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * 
 */
public class SysOutMatchingProcessHandler<SOURCE extends Serializable, TARGET extends Serializable> extends SilentMatchingProcessHandler<SOURCE, TARGET> {
	
	/**
	 * So.
	 *
	 * @param o the o
	 */
	private void so(Object o) {
		System.out.println(o);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyProcessStartEvent(java.lang.String)
	 */
	@Override
	protected void doNotifyProcessStartEvent(String processId) {
		this.so("Starting process with ID #" + processId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyProcessEndEvent()
	 */
	@Override
	protected void doNotifyProcessEndEvent() {
		this.so("Ending process");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyNumberOfComparisonRoundsChanged(int)
	 */
	@Override
	protected void doNotifyNumberOfComparisonRoundsChanged(int rounds) {
		this.so("Number of comparison rounds changed to: " + rounds);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyComparisonRoundStart(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundStart(ProvidedData<TARGET> source) {
		this.so("Starting comparison round for " + source);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyComparisonRoundPerformed(org.fao.fi.comet.core.patterns.data.providers.ProvidedData)
	 */
	@Override
	protected void doNotifyComparisonRoundPerformed(ProvidedData<TARGET> source) {
		this.so("Comparison round performed " + source);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int)
	 */
	@Override
	protected void doNotifyMaximumNumberOfAtomicComparisonsPerformedInRoundChanged(int comparisons) {
		this.so("Maximum number of atomic comparisons performed in round changed to: " + comparisons);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyAtomicComparisonStart()
	 */
	@Override
	protected void doNotifyAtomicComparisonStart() {
		this.so("Atomic comparison start");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyTotalNumberOfAtomicComparisonsChanged(int)
	 */
	@Override
	protected void doNotifyTotalNumberOfAtomicComparisonsChanged(int atomicComparisons) {
		this.so("Total number of atomic comparisons changed to: " + atomicComparisons);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyAtomicComparisonPerformed()
	 */
	@Override
	protected void doNotifyAtomicComparisonPerformed() {
		this.so("Atomic comparison performed");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler#doNotifyAtomicComparisonPerformed()
	 */
	@Override
	protected void doNotifyAtomicComparisonEnd() {
		this.so("Atomic comparison end");
	}
}