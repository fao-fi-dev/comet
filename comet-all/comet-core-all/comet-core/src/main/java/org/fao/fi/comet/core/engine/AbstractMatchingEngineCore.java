/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_assert;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_iArg;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nEm;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ServiceLoader;

import org.fao.fi.comet.core.engine.process.MatchingEngineMetadataResolver;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.exceptions.MatchingProcessException;
import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessorInfo;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfigurationParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletData;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider;
import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;
import org.fao.fi.comet.core.patterns.handlers.id.impl.basic.SerializableDataChecksumIDHandler;
import org.fao.fi.comet.core.patterns.handlers.id.impl.basic.SerializableDataIDHandler;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

// TODO: Auto-generated Javadoc
/**
 * The generic matching engine
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 * @param <CONFIG> the generic type
 * 
 */
abstract public class AbstractMatchingEngineCore<SOURCE extends Serializable,
												 TARGET extends Serializable,
												 CONFIG extends MatchingEngineProcessConfiguration> extends AbstractLoggingAwareClient {
	
	/** The _resolver. */
	final protected MatchingEngineMetadataResolver _resolver = new MatchingEngineMetadataResolver();
	
	/**
	 * Class constructor.
	 */
	public AbstractMatchingEngineCore() {
		super();
	}

	/**
	 * Gets the data type set.
	 *
	 * @param matchlets the matchlets
	 * @return the data type set
	 */
	final protected Collection<Class<?>> getDataTypeSet(Collection<Matchlet<SOURCE, ?, TARGET, ?>> matchlets) {
		Collection<Class<?>> dataTypes = new HashSet<Class<?>>();

		if(matchlets != null)
			for(Matchlet<SOURCE, ?, TARGET, ?> matchlet : matchlets) {
				if(ObjectsHelper.isAnnotationPresent(matchlet.getClass(), MatchletData.class))
					for(MatchletData annotation : ObjectsHelper.getAllAnnotationsOfType(matchlet.getClass(), MatchletData.class)) {
						dataTypes.addAll(Arrays.asList(annotation.dataType()));
					}
			}

		return dataTypes;
	}

	/**
	 * Do get matching.
	 *
	 * @param matchingsData the matchings data
	 * @param sourceIdentifier the source identifier
	 * @param targetIdentifier the target identifier
	 * @param currentMatchingResult the current matching result
	 * @return the matching
	 */
	protected Matching<SOURCE, TARGET> doGetMatching(MatchingsData<SOURCE, TARGET> matchingsData,
													 DataIdentifier sourceIdentifier,
													 DataIdentifier targetIdentifier,
													 MatchingResult<?, ?> currentMatchingResult) {
		MatchingDetails<SOURCE, TARGET> details = matchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);

		if(details == null) {
			this._log.info("Ouch! Cannot find matching details to get for SID: {} - TID: {}", sourceIdentifier.getId(), targetIdentifier.getId());

			return null;
		} else
			return details.findMatchingByTargetId(targetIdentifier);
	}

	/**
	 * Do remove matching.
	 *
	 * @param matchingsData the matchings data
	 * @param sourceIdentifier the source identifier
	 * @param targetIdentifier the target identifier
	 * @return the matching
	 */
	protected Matching<SOURCE, TARGET> doRemoveMatching(MatchingsData<SOURCE, TARGET> matchingsData,
														DataIdentifier sourceIdentifier,
														DataIdentifier targetIdentifier) {
		MatchingDetails<SOURCE, TARGET> details = matchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);

		if(details == null) {
			this._log.info("Ouch! Cannot find matching details to remove for SID: {} - TID: {}", sourceIdentifier.getId(), targetIdentifier.getId());

			return null;
		} else
			return details.removeMatchingByTargetId(targetIdentifier);
	}

	/**
	 * Do get matching details.
	 *
	 * @param matchingsData the matchings data
	 * @param sourceIdentifier the source identifier
	 * @param targetIdentifier the target identifier
	 * @return the matching details
	 */
	protected MatchingDetails<SOURCE, TARGET> doGetMatchingDetails(MatchingsData<SOURCE, TARGET> matchingsData,
																   DataIdentifier sourceIdentifier,
																   DataIdentifier targetIdentifier) {
		return matchingsData.findMatchingDetailsBySourceIdentifier(sourceIdentifier);
	}

	/**
	 * Do perform actual comparison.
	 *
	 * @param source the source
	 * @param sourceIdentifier the source identifier
	 * @param target the target
	 * @param targetIdentifier the target identifier
	 * @param matchlet the matchlet
	 * @return the matching result
	 */
	protected MatchingResult<?, ?> doPerformActualComparison(SOURCE source, DataIdentifier sourceIdentifier,
															 TARGET target, DataIdentifier targetIdentifier,
															 Matchlet<SOURCE, ?, TARGET, ?> matchlet) {
		return matchlet.performMatching(source, sourceIdentifier, target, targetIdentifier);
	}

	/**
	 * After comparison callback.
	 *
	 * @param source the source
	 * @param targets the targets
	 */
	protected void afterComparisonCallback(SOURCE source, DataProvider<TARGET> targets) {
		return;
	}
	
	/**
	 * After completion callback.
	 */
	abstract protected void afterCompletionCallback();

	/**
	 * Perform atomic comparison.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param matchingsData the matchings data
	 * @param source the source
	 * @param target the target
	 * @param sourceIdentifier the source identifier
	 * @param targetIdentifier the target identifier
	 * @param matchlets the matchlets
	 * @return the matching
	 */
	final public Matching<SOURCE, TARGET> performAtomicComparison(CONFIG configuration,
																  MatchingProcessHandler<SOURCE, TARGET> tracker,
																  MatchingsData<SOURCE, TARGET> matchingsData,
																  SOURCE source,
																  TARGET target,
																  DataIdentifier sourceIdentifier,
																  DataIdentifier targetIdentifier,
																  Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets) {
		$nN(configuration, "Matching configuration cannot be null");
		$nN(tracker, "Matching tracker cannot be null");
		$nN(matchingsData, "Matchings data cannot be null");
		$nN(source, "Matching data source cannot be null");
		$nN(target, "Matching data target cannot be null");
		$nN(sourceIdentifier, "Matching source identifier cannot be null");
		$nN(targetIdentifier, "Matching target identifier cannot be null");
		$nEm(matchlets, "Matchlets cannot be null or empty");
		
		try {
			double matchletWeight;

			double matchletsTotalWeight = 0D;;
			double totalScore = 0D;

			MatchingResult<?, ?> currentMatchingResult = null;

			//Lifecycle management
			tracker.notifyAtomicComparisonStart();

			Matching<SOURCE, TARGET> matching = matchingsData.lazyGetMatching(source, sourceIdentifier,
																			  target, targetIdentifier);

			double currentScore;

			//Cycle on currently registered matchlets
			for(Matchlet<SOURCE, ?, TARGET, ?> matchlet : matchlets) {
				if(!tracker.getProcessStatus().is(MatchingEngineStatus.RUNNING))
					break;

				//If the matchlet's weight is valid
				if(!this.skipMatchlet(matchlet)) {
					matchletWeight = matchlet.getWeight();

					currentMatchingResult = this.doPerformActualComparison(source, sourceIdentifier, target, targetIdentifier, matchlet);

					matching.updateMatchingResult(currentMatchingResult);

					//Authoritative matches should halt the matchlets iteration
					if(matching.isAuthoritative()) {
						//Either NO MATCH or FULL MATCH
						$_assert(currentMatchingResult.isFullMatch() || currentMatchingResult.isNoMatch(), "An authoritative match should return either NO MATCH or FULL MATCH");

						totalScore = currentMatchingResult.getScore().getValue() * matchletWeight;

						//Lifecycle management
						if(currentMatchingResult.isNoMatch())
							tracker.notifyAuthoritativeNoMatch();
						else if(currentMatchingResult.isFullMatch()) {
							tracker.notifyAuthoritativeFullMatch();
						}

						matchletsTotalWeight = matchletWeight;

						//Skips remaining matchlets as an authoritative match has been returned...
						break;
					} else {
						//Comparisons returning a 'non performed' score will *not* contribute to the total weight IF the matchlet is optional
						if(!currentMatchingResult.isNonPerformed() || !matchlet.isOptional()) 
							matchletsTotalWeight += matchletWeight;

						currentScore = currentMatchingResult.getScore().getValue() * matchletWeight;

						totalScore += currentScore;
					}
				}
			}

			totalScore = Double.compare(matchletsTotalWeight, 0D) == 0 ? MatchingScore.SCORE_NO_MATCH : totalScore / matchletsTotalWeight;

			matching.getScore().setValue(totalScore);

			//Lifecycle management
			tracker.notifyAtomicComparisonPerformed();

			$gte(totalScore, MatchingScore.SCORE_NO_MATCH, "The computed weighted score ({}) cannot be lower than the minimum allowed ({})", totalScore, MatchingScore.SCORE_NO_MATCH);
			$lte(totalScore, MatchingScore.SCORE_FULL_MATCH, "The computed weighted score ({}) cannot be higher than the maximum allowed ({})", totalScore, MatchingScore.SCORE_FULL_MATCH);

			if(!matching.isAuthoritative() && !matching.isNonPerformed()) {
				if(Double.compare(matching.getScore().getValue(), configuration.getMinimumAllowedWeightedScore()) < 0) {
					this.doRemoveMatching(matchingsData, sourceIdentifier, targetIdentifier);

					return null;
				}
			}

			return matching;
		} finally {
			//Lifecycle management
			tracker.notifyAtomicComparisonEnd();
		}
	}

	/**
	 * Compares a data source with the current domain.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param currentMatchingsData the current matchings data
	 * @param sources the sources
	 * @param target the target
	 * @param targetProviderId the target provider id
	 * @param partitioner the partitioner
	 * @param matchlets the matchlets
	 * @param sourceIDHandler the source id handler
	 * @param targetIDHandler the target id handler
	 * @return the matchings data
	 */
	abstract protected MatchingsData<SOURCE, TARGET> performComparison(CONFIG configuration,
																	   MatchingProcessHandler<SOURCE, TARGET> tracker,
																	   MatchingsData<SOURCE, TARGET> currentMatchingsData,
																	   DataProvider<SOURCE> sources,
																	   TARGET target,
																	   String targetProviderId,
																	   DataPartitioner<SOURCE, TARGET> partitioner,
																	   Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets,
																	   IDHandler<SOURCE, ?> sourceIDHandler,
																	   IDHandler<TARGET, ?> targetIDHandler);

	/**
	 * Compare all.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param sources the sources
	 * @param partitioner the partitioner
	 * @param targets the targets
	 * @return the matching engine process result
	 * @throws MatchingProcessException the matching process exception
	 */
	final public MatchingEngineProcessResult<SOURCE, TARGET, CONFIG> compareAll(CONFIG configuration,
																				MatchingProcessHandler<SOURCE, TARGET> tracker,
																				DataProvider<SOURCE> sources,
																				DataPartitioner<SOURCE, TARGET> partitioner,
																				DataProvider<TARGET> targets) throws MatchingProcessException {
		return this.compareAll(configuration,
							   tracker,
							   sources,
							   partitioner,
							   targets,
							   new SerializableDataChecksumIDHandler<SOURCE>(),
							   new SerializableDataIDHandler<TARGET>());
	}

	/**
	 * Compare all.
	 *
	 * @param configuration the configuration
	 * @param tracker the tracker
	 * @param sources the sources
	 * @param partitioner the partitioner
	 * @param targets the targets
	 * @param sourceIDHandler the source id handler
	 * @param targetIDHandler the target id handler
	 * @return the matching engine process result
	 * @throws MatchingProcessException the matching process exception
	 */
	final public MatchingEngineProcessResult<SOURCE, TARGET, CONFIG> compareAll(CONFIG configuration,
																				MatchingProcessHandler<SOURCE, TARGET> tracker,
																				DataProvider<SOURCE> sources,
																				DataPartitioner<SOURCE, TARGET> partitioner,
																				DataProvider<TARGET> targets,
																				IDHandler<SOURCE, ?> sourceIDHandler,
																				IDHandler<TARGET, ?> targetIDHandler) throws MatchingProcessException {
		$nN(configuration, "The configuration cannot be null");
		$nN(tracker, "The tracker cannot be null");

		$nN(partitioner, "The partitioner cannot be null");

		$nN(sources, "The sources cannot be null");
		$nN(targets, "The targets cannot be null");

		configuration.validate();

		Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets = this.initializeMatchlets(configuration);

		$nEm(matchlets, "Matchlets cannot be null or empty");

		$nN(sourceIDHandler, "The source ID handler cannot be null");
		$nN(targetIDHandler, "The target ID handler cannot be null");

		$_iArg(sources.iterator().hasNext(), "The sources must contain at least one element");

		MatchingsData<SOURCE, TARGET> results = new MatchingsData<SOURCE, TARGET>();

		tracker.notifyComparisonProcessStarted(tracker.getProcessId());

		if(targets instanceof SizeAwareDataProvider)
			tracker.setNumberOfComparisonRounds(((SizeAwareDataProvider<TARGET>)targets).getAvailableDataSize());

		try {
			for(ProvidedData<TARGET> target : targets) {
				if(!tracker.getProcessStatus().is(MatchingEngineStatus.RUNNING))
					break;
	
				tracker.notifyComparisonRoundStart(target);
				
				if(sources instanceof StreamingDataProvider) {
					StreamingDataProvider<SOURCE> streaming = (StreamingDataProvider<SOURCE>)sources;
					
					if(streaming.isRewindable()) streaming.rewind();
				}
				
				this.performComparison(configuration,
									   tracker,
									   results,
									   sources,
									   target.getData(),
									   target.getProviderId(),
									   partitioner,
									   matchlets,
									   sourceIDHandler,
									   targetIDHandler);
	
				tracker.notifyComparisonRoundPerformed(target);
			}
		} finally {
			this.afterCompletionCallback();
		}

		tracker.notifyComparisonProcessCompleted();

		this.cleanup(configuration, results, true);
		
		MatchingEngineProcessorInfo<CONFIG> processorInfo = new MatchingEngineProcessorInfo<CONFIG>(this.updateConfiguration(matchlets, configuration), tracker.getProcessStatus());

		return new MatchingEngineProcessResult<SOURCE, TARGET, CONFIG>(processorInfo, results);
	}

	/**
	 * Cleanup.
	 *
	 * @param configuration the configuration
	 * @param matchingsData the matchings data
	 * @param keepData the keep data
	 */
	public void cleanup(MatchingEngineProcessConfiguration configuration, MatchingsData<SOURCE, TARGET> matchingsData, boolean keepData) {
		matchingsData.cleanup(configuration.getMaxCandidatesPerEntry(), configuration.getMinimumAllowedWeightedScore(), keepData);
	}

	/**
	 * Skip matchlet.
	 *
	 * @param matchletConfiguration a matchlet configuration
	 * @return true if the provided matchlet configuration has a weight set to zero
	 */
	protected boolean skipMatchlet(Matchlet<?, ?, ?, ?> matchletConfiguration) {
		return Double.compare(matchletConfiguration.getWeight(), 0) == 0;
	}

	/**
	 * Update configuration.
	 *
	 * @param matchlets the matchlets
	 * @param configuration the configuration
	 * @return the config
	 * @throws MatchletConfigurationException the matchlet configuration exception
	 */
	final protected CONFIG updateConfiguration(Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets, CONFIG configuration) throws MatchletConfigurationException {
		CONFIG cloned = SerializationHelper.clone(configuration);

		Collection<MatchletConfiguration> effectiveMatchletsConfigurations = new ArrayList<MatchletConfiguration>();

		MatchletConfiguration currentMatchletConfiguration;
		for(Matchlet<SOURCE, ?, TARGET, ?> matchlet : matchlets) {
			currentMatchletConfiguration = new MatchletConfiguration(matchlet.getId(), matchlet.getName(), matchlet.getClass().getName());
			currentMatchletConfiguration.setMatchletParameters(matchlet.getConfiguration());

			Iterator<MatchletConfigurationParameter> iterator = currentMatchletConfiguration.getMatchletParameters().iterator();
			
			while(iterator.hasNext()) {
				if(iterator.next().isTransient())
					iterator.remove();
			}
			
			effectiveMatchletsConfigurations.add(currentMatchletConfiguration);
		}

		cloned.setMatchletsConfiguration(effectiveMatchletsConfigurations);

		return cloned;
	}

	/**
	 * Gets the discoverable matchlet class.
	 *
	 * @return the discoverable matchlet class
	 */
	@SuppressWarnings("rawtypes")
	protected Class<? extends Matchlet> getDiscoverableMatchletClass() {
		return Matchlet.class;
	}

	/**
	 * Gets the all available matchlets info.
	 *
	 * @return the all available matchlets info
	 */
	final public Collection<MatchletInfo> getAllAvailableMatchletsInfo() {
		return _resolver.getAvailableMatchletsInfo(this.getDiscoverableMatchletClass());
	}

	/**
	 * Initialize matchlets.
	 *
	 * @param configuration the configuration
	 * @return the collection<? extends matchlet< sourc e,?, targe t,?>>
	 * @throws MatchletConfigurationException the matchlet configuration exception
	 */
	@SuppressWarnings("unchecked")
	final protected Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> initializeMatchlets(CONFIG configuration) throws MatchletConfigurationException {
		if(configuration == null || configuration.getMatchletsConfigurations() == null || configuration.getMatchletsConfigurations().isEmpty())
			return null;

		Collection<Matchlet<SOURCE, ?, TARGET, ?>> matchlets = new ArrayList<Matchlet<SOURCE, ?, TARGET, ?>>();

		@SuppressWarnings("rawtypes")
		ServiceLoader<? extends Matchlet> loader = ServiceLoader.load(this.getDiscoverableMatchletClass());

		boolean provided;
		for(MatchletConfiguration matchletConfig : configuration.getMatchletsConfigurations()) {
			provided = false;

			for(Matchlet<SOURCE, ?, TARGET, ?> matchlet : loader) {
				if(matchlet.getName().equals(matchletConfig.getMatchletName())) {
					matchlet = SerializationHelper.clone(matchlet);

					matchlet.configure(matchletConfig.getMatchletParameters());

					matchlet.setId(matchletConfig.getMatchletId());
					matchlet.validateConfiguration();

					matchlets.add(matchlet);

					provided = true;
					break;
				}
			}

			if(!provided)
				throw new MatchletConfigurationException("No provider available for matchlet named " + matchletConfig.getMatchletId());
		}

		if(matchlets.isEmpty()) //This shouldn't happen...
			return null;

		return matchlets;
	}
}