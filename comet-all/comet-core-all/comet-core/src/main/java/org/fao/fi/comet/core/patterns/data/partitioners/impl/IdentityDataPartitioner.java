/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.core.patterns.data.partitioners.impl;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;

// TODO: Auto-generated Javadoc
/**
 * Place your class / interface description here.
 * 
 * History:
 * 
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @param <SOURCE_ENTITY> the generic type
 * @param <TARGET_ENTITY> the generic type
 * 
 */
public class IdentityDataPartitioner<SOURCE_ENTITY extends Serializable, TARGET_ENTITY extends Serializable> implements DataPartitioner<SOURCE_ENTITY, TARGET_ENTITY> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner#include(java.io.Serializable, java.io.Serializable, java.util.Collection)
	 */
	@Override
	public boolean include(SOURCE_ENTITY source, TARGET_ENTITY target, Collection<? extends Matchlet<SOURCE_ENTITY, ?, TARGET_ENTITY, ?>> matchlets) {
		return true;
	}
}