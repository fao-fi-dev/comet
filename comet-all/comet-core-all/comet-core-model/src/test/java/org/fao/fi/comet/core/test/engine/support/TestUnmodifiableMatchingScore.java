/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine.support;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.model.score.support.UnmodifiableMatchingScore;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class TestUnmodifiableMatchingScore {
	@Test
	public void testCreationByConstructor() {
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE);
		} catch(Throwable t) {
			Assert.fail();
		}
	}
	
	@Test
	public void testCreationByNoArgConstructorAndSetters() {
		try {
			MatchingScore u = new UnmodifiableMatchingScore();
			u.setMatchingType(MatchingType.NON_AUTHORITATIVE);
			u.setValue(0.666);
		} catch(Throwable t) {
			Assert.fail();
		}
	}
	
	@Test
	public void testCreationByAnother() {
		try {
			MatchingScore m = new MatchingScore();
			m.setMatchingType(MatchingType.NON_AUTHORITATIVE);
			m.setValue(0.666);
			
			new UnmodifiableMatchingScore(m);
		} catch(Throwable t) {
			Assert.fail();
		}
	}
	
	@Test
	public void testEquals() {
		MatchingScore m = new MatchingScore();
		m.setMatchingType(MatchingType.NON_AUTHORITATIVE);
		m.setValue(0.666);
		
		MatchingScore u = new UnmodifiableMatchingScore(m);
		
		Assert.assertEquals(m, u);
	}
	
	@Test
	public void testUpdateValue() {
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE).setValue(0.3);
		} catch(Throwable t) {
			Assert.fail();
		}
		
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE).setValue(0.4);
			Assert.fail();
		} catch(Throwable t) {
			;
		}
	}

	@Test
	public void testUpdateType() {
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE).setMatchingType(MatchingType.AUTHORITATIVE);
		} catch(Throwable t) {
			Assert.fail();
		}
		
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE).setMatchingType(MatchingType.NON_PERFORMED);
			Assert.fail();
		} catch(Throwable t) {
			;
		}
	}

	@Test
	public void testIncreaseValue() {
		try {
			new UnmodifiableMatchingScore(0.3, MatchingType.AUTHORITATIVE).increaseValue(0.366);
			Assert.fail();
		} catch(Throwable t) {
			;
		}
	}
}
