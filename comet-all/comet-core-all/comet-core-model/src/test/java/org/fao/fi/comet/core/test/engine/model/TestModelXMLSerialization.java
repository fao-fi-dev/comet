/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingResultData;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 May 2013
 */
public class TestModelXMLSerialization {
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	static class MyNameValuePair implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 5183932576021208810L;

		private String _name;
		private String _value;

		/**
		 * Class constructor
		 *
		 */
		public MyNameValuePair() {
			super();
		}

		/**
		 * Class constructor
		 *
		 * @param name
		 * @param value
		 */
		public MyNameValuePair(String name, String value) {
			super();

			this._name = name;
			this._value = value;
		}

		/**
		 * @return the 'name' value
		 */
		public String getName() {
			return this._name;
		}

		/**
		 * @param name the 'name' value to set
		 */
		public void setName(String name) {
			this._name = name;
		}

		/**
		 * @return the 'value' value
		 */
		public String getValue() {
			return this._value;
		}

		/**
		 * @param value the 'value' value to set
		 */
		public void setValue(String value) {
			this._value = value;
		}
	}

	private MatchingResultData<MyNameValuePair, MyNameValuePair> getMatchingData() {
		MatchingResultData<MyNameValuePair, MyNameValuePair> data = new MatchingResultData<MyNameValuePair, MyNameValuePair>();

		MyNameValuePair first = new MyNameValuePair("source", "value");
		MyNameValuePair second = new MyNameValuePair("target1", "value1");
		MyNameValuePair third = new MyNameValuePair("target2", "value2");

		data.setSourceData(first);
		data.setMatchingTargetData(Arrays.asList(new MyNameValuePair[] { second, third }));
		data.setScore(new MatchingScore(0.666, MatchingType.AUTHORITATIVE));

		return data;
	}

	private MatchingResult<MyNameValuePair, MyNameValuePair> getMatchingResult() {
		MatchingResult<MyNameValuePair, MyNameValuePair> result = new MatchingResult<TestModelXMLSerialization.MyNameValuePair, TestModelXMLSerialization.MyNameValuePair>();

		result.setOriginatingMatchletName("foo.bar.baz");
		result.setOriginatingMatchletWeight(666.0);
		result.setScore(new MatchingScore(0.666, MatchingType.AUTHORITATIVE));

		Collection<MatchingResultData<MyNameValuePair, MyNameValuePair>> datas = new ArrayList<MatchingResultData<MyNameValuePair, MyNameValuePair>>();
		datas.add(this.getMatchingData());
		datas.add(this.getMatchingData());
		datas.add(this.getMatchingData());

		result.setMatchingsResultData(datas);

		return result;
	}

	private Matching<MyNameValuePair, MyNameValuePair> getMatching() {
		Matching<MyNameValuePair, MyNameValuePair> matching = new Matching<TestModelXMLSerialization.MyNameValuePair, TestModelXMLSerialization.MyNameValuePair>();
		matching.setScore(new MatchingScore(0.666, MatchingType.AUTHORITATIVE));
		matching.setSourceIdentifier(new DataIdentifier("foo", String.valueOf(69)));
		matching.setTarget(new MyNameValuePair("foo", "baz"));
		matching.setTargetIdentifier(new DataIdentifier("bar", String.valueOf(96)));
		matching.setScore(new MatchingScore(.66669, MatchingType.AUTHORITATIVE));
		matching.getMatchingResults().add(this.getMatchingResult());
		matching.getMatchingResults().add(this.getMatchingResult());

		return matching;
	}

	@Test
	public void testMatchingResultDataXMLSerialization() throws Throwable {
		System.out.println(JAXBHelper.toXML(JAXBContext.newInstance(
			MatchingResultData.class,
			MyNameValuePair.class
		), this.getMatchingData()));
	}

	@Test
	public void testMatchingResultXMLSerialization() throws Throwable {
		System.out.println(JAXBHelper.toXML(JAXBContext.newInstance(
			MatchingResult.class,
			MyNameValuePair.class
		), this.getMatchingResult()));
	}

	@Test
	public void testMatchingXMLSerialization() throws Throwable {
		Matching<?, ?> matching = this.getMatching();
		System.out.println(JAXBHelper.toXML(JAXBContext.newInstance(
			Object.class,
			Matching.class,
			MyNameValuePair.class
		), matching));
	}
}
