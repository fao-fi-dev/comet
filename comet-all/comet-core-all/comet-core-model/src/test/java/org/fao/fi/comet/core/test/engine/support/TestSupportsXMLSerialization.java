/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.test.engine.support;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 May 2013
 */
public class TestSupportsXMLSerialization {
	@Test
	public void testMatchingScoreXMLSerialization() throws Throwable {
		String serialized = JAXBHelper.toXML(new MatchingScore(0.666, MatchingType.AUTHORITATIVE), JAXBHelper.OMIT_XML_DECLARATION);
		
		Assert.assertTrue(
			serialized.equals("<MatchingScore type=\"AUTHORITATIVE\" value=\"0.666\"/>") ||
			serialized.equals("<MatchingScore value=\"0.666\" type=\"AUTHORITATIVE\"/>")
		);
	}

	@Test
	public void testMatchingTypeXMLSerialization() throws Throwable {
		Assert.assertEquals("<MatchingType>AUTHORITATIVE</MatchingType>", JAXBHelper.toXML(MatchingType.AUTHORITATIVE, JAXBHelper.OMIT_XML_DECLARATION));
	}
}
