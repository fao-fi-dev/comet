/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.score.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.score.Score;

/**
 * Models a single data comparison result by means of its score value
 * and matching type
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Jul 2010
 */
@XmlType(name="MatchingScore")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingScore implements Score<MatchingScore> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8215970917574197230L;

	/** The score value returned when there is a perfect match */
	static final public double SCORE_FULL_MATCH = 1D;

	/** The score value returned when there is no match */
	static final public double SCORE_NO_MATCH 	= 0D;

	/** Field _value */
	@XmlAttribute(name="value")
	protected double _value = MatchingScore.SCORE_NO_MATCH;

	/** Field _matchingType */
	@XmlAttribute(name="type")
	protected MatchingType _matchingType = MatchingType.NON_PERFORMED;

	static final private MatchingScore NO_MATCH_NON_AUTHORITATIVE = new UnmodifiableMatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.NON_AUTHORITATIVE);
	static final private MatchingScore NO_MATCH_AUTHORITATIVE	   = new UnmodifiableMatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.AUTHORITATIVE);

	static final private MatchingScore MATCH_NON_AUTHORITATIVE = new UnmodifiableMatchingScore(MatchingScore.SCORE_FULL_MATCH, MatchingType.NON_AUTHORITATIVE);
	static final private MatchingScore MATCH_AUTHORITATIVE	    = new UnmodifiableMatchingScore(MatchingScore.SCORE_FULL_MATCH, MatchingType.AUTHORITATIVE);

	static final private MatchingScore NON_PERFORMED = new MatchingScore();

	/**
	 * Class constructor
	 */
	public MatchingScore() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param value
	 * @param matchingType
	 */
	public MatchingScore(double value, MatchingType matchingType) {
		super();

		this.setValue(value);
		this.setMatchingType(matchingType);
	}

	/**
	 * Class constructor
	 *
	 * @param another
	 */
	public MatchingScore(MatchingScore another) {
		super();

		$nN(another, "Provided score cannot be null");

		this.setValue(another.getValue());
		this.setMatchingType(another.getMatchingType());
	}

	/**
	 * @return the 'lambda' value
	 */
	final public double getValue() {
		return this._value;
	}

	/**
	 * @param value the 'value' value to set
	 */
	public void setValue(double value) {
		$gte(value, MatchingScore.SCORE_NO_MATCH, "Score value {} cannot be lower than the minimum allowed ({})", value, MatchingScore.SCORE_NO_MATCH);
		$lte(value, MatchingScore.SCORE_FULL_MATCH, "Score value {} cannot be higher than the maximum allowed ({})", value, MatchingScore.SCORE_FULL_MATCH);

		this._value = value;
	}

	public void increaseValue(double valueDelta) {
		$gte(valueDelta, MatchingScore.SCORE_NO_MATCH, "Score value delta ({}) cannot be lower than the minimum allowed ({})", valueDelta, MatchingScore.SCORE_NO_MATCH);
		$lte(valueDelta, MatchingScore.SCORE_FULL_MATCH, "Score value delta ({}) cannot be higher than the maximum allowed ({})", valueDelta, MatchingScore.SCORE_FULL_MATCH);
		$lte(this._value + valueDelta, MatchingScore.SCORE_FULL_MATCH, "Score value ({}) cannot be higher than the maximum allowed ({})", this._value + valueDelta, MatchingScore.SCORE_FULL_MATCH);

		this._value += valueDelta;
	}

	/**
	 * @return the 'matchingType' value
	 */
	final public MatchingType getMatchingType() {
		return this._matchingType;
	}

	/**
	 * @param matchingType the 'matchingType' value to set
	 */
	public void setMatchingType(MatchingType matchingType) {
		$nN(matchingType, "Matching type cannot be null");

		this._matchingType = matchingType;
	}

	/**
	 * @return true if the matching type is {@link MatchingType#AUTHORITATIVE}
	 */
	final public boolean isAuthoritative() {
		return this._matchingType.isAuthoritative();
	}

	/**
	 * @return true if the matching type is {@link MatchingType#NON_AUTHORITATIVE}
	 */
	final public boolean isNonAuthoritative() {
		return this._matchingType.isNonAuthoritative();
	}

	/**
	 * @return true if the matching type is {@link MatchingType#NON_PERFORMED}
	 */
	final public boolean isNonPerformed() {
		return this._matchingType.isNonPerformed();
	}

	/**
	 * @return true if the score value means {@link Matchlet#SCORE_NO_MATCH} and the matching type is not {@link MatchingType#NON_PERFORMED}
	 */
	final public boolean isNoMatch() {
		return !this.isNonPerformed() && Double.compare(this._value, MatchingScore.SCORE_NO_MATCH) == 0;
	}

	/**
	 * @return true if the score value means {@link Matchlet#SCORE_FULL_MATCH}
	 */
	final public boolean isFullMatch() {
		return Double.compare(this._value, MatchingScore.SCORE_FULL_MATCH) == 0;
	}

	/**
	 * @return a score instance meaning 'match non performed'
	 */
	static public MatchingScore getNonPerformedTemplate() {
		return MatchingScore.NON_PERFORMED;
	}

	/**
	 * @return a score instance meaning 'authoritative no match'
	 */
	static public MatchingScore getAuthoritativeNoMatchTemplate() {
		return MatchingScore.NO_MATCH_AUTHORITATIVE;
	}

	/**
	 * @return a score instance meaning 'authoritative full match'
	 */
	static public MatchingScore getAuthoritativeFullMatchTemplate() {
		return MatchingScore.MATCH_AUTHORITATIVE;
	}

	/**
	 * @return a score instance meaning 'non authoritative no match'
	 */
	static public MatchingScore getNonAuthoritativeNoMatchTemplate() {
		return MatchingScore.NO_MATCH_NON_AUTHORITATIVE;
	}

	/**
	 * @return a score instance meaning 'non authoritative full match'
	 */
	static public MatchingScore getNonAuthoritativeFullMatchTemplate() {
		return MatchingScore.MATCH_NON_AUTHORITATIVE;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.topology.WeightValue#multiply(org.fao.vrmf.utilities.common.utils.topology.WeightValue)
	 */
	@Override
	public MatchingScore multiply(MatchingScore another) {
		$nN(another, "The other operand cannot be null");

		boolean currentAuthoritative = this.isAuthoritative();
		boolean anotherAuthoritative = another instanceof MatchingScore && ((MatchingScore)another).isAuthoritative();
		boolean bothAuthoritative = currentAuthoritative && anotherAuthoritative;

		return new MatchingScore(this.toDouble() * another.toDouble(), bothAuthoritative ? MatchingType.AUTHORITATIVE : MatchingType.NON_AUTHORITATIVE);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.utilities.common.utils.topology.WeightValue#toDouble()
	 */
	@Override
	final public double toDouble() {
		return this._value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	final public int compareTo(MatchingScore another) {
		$nN(another, "The other operand cannot be null");

		boolean currentAuthoritative = this.isAuthoritative();
		boolean anotherAuthoritative = another instanceof MatchingScore && ((MatchingScore)another).isAuthoritative();
		boolean bothAuthoritative = currentAuthoritative && anotherAuthoritative;
		boolean noneAuthoritative = !currentAuthoritative && !anotherAuthoritative;

		if(bothAuthoritative || noneAuthoritative)
			return Double.compare(this.toDouble(), another.toDouble());

		if(currentAuthoritative)
			return 1;

		return -1;
	}

	final public MatchingScore reverse() {
		if(this.isAuthoritative())
			return this.isFullMatch() ? MatchingScore.getAuthoritativeNoMatchTemplate() : MatchingScore.getAuthoritativeFullMatchTemplate();

		if(this.isNonPerformed())
			return this;

		MatchingScore reversed = new MatchingScore(this);
		reversed.setValue(SCORE_FULL_MATCH - reversed.getValue());

		return reversed;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	final public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(this._value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((this._matchingType == null) ? 0 : this._matchingType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	final public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MatchingScore))
			return false;
		MatchingScore other = (MatchingScore) obj;
		if (Double.doubleToLongBits(this._value) != Double.doubleToLongBits(other._value))
			return false;
		if (this._matchingType != other._matchingType)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	final public String toString() {
		return "[ S: " + this._value + ", MT: " + this._matchingType + " ]";
	}
}