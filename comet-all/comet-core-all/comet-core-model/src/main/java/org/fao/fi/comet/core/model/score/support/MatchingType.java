/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.score.support;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 04/lug/2010   Fabio     Creation.
 *
 * @version 1.0
 * @since 04/lug/2010
 */
@XmlType(name="MatchingType")
@XmlAccessorType(XmlAccessType.FIELD)
public enum MatchingType {
	AUTHORITATIVE("AUTHORITATIVE"),
	NON_AUTHORITATIVE("NON AUTHORITATIVE"),
	NON_PERFORMED("NON PERFORMED");

	@XmlAttribute(name="type")
	private String _type;

	private MatchingType(String type) {
		this._type = type;
	}

	public String getType() {
		return this._type;
	}

	public boolean isAuthoritative() {
		return AUTHORITATIVE.equals(this);
	}

	public boolean isNonAuthoritative() {
		return NON_AUTHORITATIVE.equals(this);
	}

	public boolean isNonPerformed() {
		return NON_PERFORMED.equals(this);
	}
}