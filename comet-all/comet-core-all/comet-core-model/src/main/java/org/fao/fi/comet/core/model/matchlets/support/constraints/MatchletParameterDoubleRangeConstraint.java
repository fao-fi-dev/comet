/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRangeTo;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterDoubleRangeConstraint")
public class MatchletParameterDoubleRangeConstraint extends MatchletParameterRangeConstraint<Double> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1399392558012991825L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint#initializeFromField(java.lang.reflect.Field)
	 */
	public void initializeFromField(Field matchletParameter) {
		super.initializeFromField(matchletParameter);
		
		DoubleRange range = this.canBeConfiguredFrom(matchletParameter, DoubleRange.class);
		
		if(range != null) {
			this.setFrom(range.from());
			this.setTo(range.to());
			this.setIncludeFrom(range.includeFrom());
			this.setIncludeTo(range.includeTo());
			
			return;
		}
		
		DoubleRangeFrom rangeFrom = this.canBeConfiguredFrom(matchletParameter, DoubleRangeFrom.class);
		
		if(rangeFrom != null) {
			this.setFrom(rangeFrom.value());
			this.setIncludeFrom(rangeFrom.include());
			
			return;
		}
		
		DoubleRangeTo rangeTo = this.canBeConfiguredFrom(matchletParameter, DoubleRangeTo.class);
		
		if(rangeTo != null) {
			this.setTo(rangeTo.value());
			this.setIncludeTo(rangeTo.include());
			
			return;
		}
	}
}