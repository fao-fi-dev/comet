/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.StringValidValues;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterStringValuesConstraint")
public class MatchletParameterStringValuesConstraint extends MatchletParameterValuesConstraint<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4997538027522537645L;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint#initializeFromField(java.lang.reflect.Field)
	 */
	@Override
	public void initializeFromField(Field matchletParameter) {
		super.initializeFromField(matchletParameter);
		
		StringValidValues conf = this.canBeConfiguredFrom(matchletParameter, StringValidValues.class);
		
		if(conf != null) {
			this.setValues(conf.values());
			
			this.setMultiple(conf.multiple());
		}
	}
}