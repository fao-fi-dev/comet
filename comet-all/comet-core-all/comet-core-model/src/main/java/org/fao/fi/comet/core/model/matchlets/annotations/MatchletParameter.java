/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.fao.fi.comet.core.model.matchlets.support.DefaultMatchletParameterConverter;
import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConverter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Jun 2013
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface MatchletParameter {
	String name() default "";
	String description() default "";
	Class<? extends MatchletParameterConverter> converter() default DefaultMatchletParameterConverter.class;
	boolean mandatory() default false;
}