/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.NotNull;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterConstraint")
public class MatchletParameterConstraint implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4475452004774724306L;

	@XmlElement(name="notNull")
	private boolean _notNull;

	/**
	 * @return the 'notNull' value
	 */
	public boolean getNotNull() {
		return this._notNull;
	}

	/**
	 * @param notNull the 'notNull' value to set
	 */
	public void setNotNull(boolean notNull) {
		this._notNull = notNull;
	}

	public void initializeFromField(Field matchletParameter) {
		this._notNull = this.canBeConfiguredFrom(matchletParameter, NotNull.class) != null;
	}

	protected <MA extends Annotation> MA canBeConfiguredFrom(Field matchletParameter, Class<MA> with) {
		if(matchletParameter.isAnnotationPresent(with))
			return matchletParameter.getAnnotation(with);

		return null;
	}
}