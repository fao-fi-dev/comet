/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.fao.fi.comet.core.model.engine.MatchingResult;

/**
 * A simple matching result adapter that drives the marshalling of a matching result
 * based on its score and the associated matchlet exclusion policies.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class MatchingResultAdapter extends XmlAdapter<MatchingResult<?, ?>, MatchingResult<?, ?>> {
	@Override
	public MatchingResult<?, ?> unmarshal(MatchingResult<?, ?> value) {
		return value;
	}

	@Override
	public MatchingResult<?, ?> marshal(MatchingResult<?, ?> boundType) {
		if(boundType.excludeFromSerialization())
			return null;

		return boundType;
	}
}
