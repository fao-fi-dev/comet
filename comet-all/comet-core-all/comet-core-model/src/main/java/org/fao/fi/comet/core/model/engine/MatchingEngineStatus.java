/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * @author Fiorellato
 *
 */
@XmlType(name="MatchingEngineStatus")
@XmlEnum(String.class)
public enum MatchingEngineStatus {
	@XmlEnumValue("IDLE") IDLE,
	@XmlEnumValue("RUNNING") RUNNING,
	@XmlEnumValue("COMPLETED") COMPLETED,
	@XmlEnumValue("HALTED") HALTED,
	@XmlEnumValue("FAILED") FAILED,
	@XmlEnumValue("MISCONFIGURED") MISCONFIGURED;
}
