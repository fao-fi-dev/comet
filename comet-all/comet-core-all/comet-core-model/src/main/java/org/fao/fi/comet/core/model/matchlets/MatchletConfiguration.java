/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletConfiguration")
public class MatchletConfiguration implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2282570552936781396L;

	@XmlAttribute(name="matchletId")
	private String _matchletId;

	@XmlAttribute(name="matchletName")
	private String _matchletName;

	@XmlAttribute(name="matchletType")
	private String _matchletType;

	@XmlElementWrapper(name="ConfigurationParameters")
	@XmlElement(name="Parameter")
	private Collection<MatchletConfigurationParameter> _matchletParameters;

	/**
	 * Class constructor
	 *
	 */
	public MatchletConfiguration() {
		super();

		this._matchletParameters = new ArrayList<MatchletConfigurationParameter>();
	}

	/**
	 * Class constructor
	 *
	 * @param matchletId
	 */
	public MatchletConfiguration(String matchletName) {
		this(matchletName, matchletName, null);
	}
	
	/**
	 * Class constructor
	 *
	 * @param matchletId
	 */
	public MatchletConfiguration(String matchletId, String matchletName) {
		this(matchletId, matchletName, null);
	}

	/**
	 * Class constructor
	 *
	 * @param matchletId
	 */
	public MatchletConfiguration(String matchletId, String matchletName, String matchletType) {
		this();
		this._matchletId = matchletId;
		this._matchletName = matchletName;
		this._matchletType = matchletType;
	}

	static public MatchletConfiguration configure(String matchletName) {
		return new MatchletConfiguration(matchletName, matchletName, null);
	}

	static public MatchletConfiguration configure(String matchletId, String matchletName) {
		return new MatchletConfiguration(matchletId, matchletName, null);
	}

	/**
	 * @return the 'matchletId' value
	 */
	public String getMatchletId() {
		return this._matchletId;
	}

	/**
	 * @param matchletId the 'matchletId' value to set
	 */
	public void setMatchletId(String matchletId) {
		this._matchletId = matchletId;
	}

	/**
	 * @return the 'matchletName' value
	 */
	public String getMatchletName() {
		return this._matchletName;
	}

	/**
	 * @param matchletName the 'matchletName' value to set
	 */
	public void setMatchletName(String matchletName) {
		this._matchletName = matchletName;
	}

	/**
	 * @return the 'matchletType' value
	 */
	public String getMatchletType() {
		return this._matchletType;
	}

	/**
	 * @param matchletType the 'matchletType' value to set
	 */
	public void setMatchletType(String matchletType) {
		this._matchletType = matchletType;
	}

	/**
	 * @return the 'matchletParameters' value
	 */
	public Collection<MatchletConfigurationParameter> getMatchletParameters() {
		return this._matchletParameters;
	}

	/**
	 * @param matchletParameters the 'matchletParameters' value to set
	 */
	public void setMatchletParameters(Collection<MatchletConfigurationParameter> matchletParameters) {
		this._matchletParameters = matchletParameters;
	}
	
	private MatchletConfiguration _with(String parameterName, String parameterValue) {
		return this._with(parameterName, parameterValue, false);
	}

	private MatchletConfiguration _with(String parameterName, Object parameterValue, boolean isTransient) {
		this._matchletParameters.add(
			new MatchletConfigurationParameter(parameterName, 
											   parameterValue == null ? 
													null : 
													isTransient ? 
														parameterValue.getClass().getSimpleName() + "@" + parameterValue.hashCode() :
														parameterValue.toString(),
											   parameterValue, 
											   isTransient)
		);

		return this;
	}

	public MatchletConfiguration with(String parameterName, String parameterValue) {
		return this._with(parameterName, parameterValue);
	}

	public MatchletConfiguration with(String parameterName, Serializable parameterValue) {
		return this._with(parameterName, parameterValue == null ? null : parameterValue.toString());
	}
	
	public MatchletConfiguration withTransient(String parameterName, Serializable parameterValue) {
		return this._with(parameterName, parameterValue == null ? null : parameterValue, true);
	}

	public MatchletConfiguration withTrue(String parameterName) {
		return this.with(parameterName, Boolean.TRUE);
	}

	public MatchletConfiguration withFalse(String parameterName) {
		return this.with(parameterName, Boolean.FALSE);
	}

	public MatchletConfiguration withNull(String parameterName) {
		return this._with(parameterName, null);
	}
}