/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 May 2013
 */
@XmlRootElement(name="DataIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataIdentifier implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8291741839540760978L;

	@XmlAttribute(name="providerId")
	private String _providerId;

	@XmlAttribute(name="id")
	private String _id;

	/**
	 * Class constructor
	 *
	 */
	public DataIdentifier() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param providerId
	 * @param id
	 */
	public DataIdentifier(String providerId, String id) {
		super();

		this._providerId = $nN(providerId, "Provider id cannot be null");;
		this._id = $nN(id, "Id cannot be null");
	}

	/**
	 * @return the 'providerId' value
	 */
	public String getProviderId() {
		return this._providerId;
	}

	/**
	 * @param providerId the 'providerId' value to set
	 */
	public void setProviderId(String providerId) {
		this._providerId = providerId;
	}

	/**
	 * @return the 'id' value
	 */
	public String getId() {
		return this._id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(String id) {
		this._id = id;
	}

	public String toUID() {
		return new StringBuilder(this._id).append("@").append(this._providerId).toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._providerId == null) ? 0 : this._providerId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataIdentifier other = (DataIdentifier) obj;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._providerId == null) {
			if (other._providerId != null)
				return false;
		} else if (!this._providerId.equals(other._providerId))
			return false;
		return true;
	}
}