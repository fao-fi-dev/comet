/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.comet.core.model.engine.MatchingDetails;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingDetailsCollection implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3880196459154012631L;

	@XmlElement(name="MatchingDetails")
	private Collection<MatchingDetails<?, ?>> _matchingDetails;

	/**
	 * Class constructor
	 */
	public MatchingDetailsCollection() {
		super();

		this._matchingDetails = new ArrayList<MatchingDetails<?, ?>>();
	}

	/**
	 * Class constructor
	 *
	 * @param matchingDetails
	 */
	public MatchingDetailsCollection(Collection<MatchingDetails<?, ?>> matchingDetails) {
		super();

		this._matchingDetails = matchingDetails;
	}

	/**
	 * @return the 'matchingDetails' value
	 */
	public Collection<MatchingDetails<?, ?>> getMatchingDetails() {
		return this._matchingDetails;
	}

	/**
	 * @param matchingDetails the 'matchingDetails' value to set
	 */
	public void setMatchingDetails(Collection<MatchingDetails<?, ?>> matchingDetails) {
		this._matchingDetails = matchingDetails;
	}
}