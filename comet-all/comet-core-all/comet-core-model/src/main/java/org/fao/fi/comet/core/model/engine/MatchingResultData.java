/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29/giu/2010   Fabio     Creation.
 *
 * @version 1.0
 * @since 29/giu/2010
 */
@XmlType(name="MatchingResultData")
@XmlAccessorType(XmlAccessType.FIELD)
final public class MatchingResultData<SOURCE_DATA, TARGET_DATA> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -174767788780386093L;

	@XmlTransient
	private MatchingScore _score = null;

	/** The source data for whom the matching result is valid */
	@XmlElement(name="SourceData")
	private SOURCE_DATA _sourceData = null;

	@XmlTransient
	private DataIdentifier _sourceIdentifier = null;

	/** The matching target data for the matching result */
	@XmlElementWrapper(name="TargetDataMatchings")
	@XmlElement(name="TargetData")
	private Collection<TARGET_DATA> _matchingTargetData = new ArrayList<TARGET_DATA>();

	/**
	 * Class constructor
	 *
	 * Required by JAXB
	 */
	public MatchingResultData() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param score
	 * @param sourceData
	 * @param matchingTargetData
	 */
	public MatchingResultData(MatchingScore score, SOURCE_DATA sourceData, DataIdentifier sourceIdentifier, Collection<TARGET_DATA> matchingTargetData) {
		super();

		this.setScore(score);
		this.setSourceData(sourceData);
		this.setSourceIdentifier(sourceIdentifier);
		this.setMatchingTargetData(matchingTargetData);
	}

	/**
	 * @return the 'score' value
	 */
	public MatchingScore getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public void setScore(MatchingScore score) {
		this._score = $nN(score, "The score cannot be null");
	}

	@XmlAttribute(name="score")
	public double getScoreValue() {
		return this._score.getValue();
	}

	public void setScoreValue(double scoreValue) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setValue(scoreValue);
	}

	@XmlAttribute(name="type")
	public MatchingType getScoreType() {
		return this._score.getMatchingType();
	}

	public void setScoreType(MatchingType scoreType) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setMatchingType(scoreType);
	}
	/**
	 * @return the 'sourceData' value
	 */
	final public SOURCE_DATA getSourceData() {
		return this._sourceData;
	}

//	@XmlElement(name="SourceData")
////	final public Object getSourceDataObject() {
//		return this.getSourceData();
//	}

//	@SuppressWarnings("unchecked")
//	final public void setSourceDataObject(Object source) {
//		this._sourceData = (SOURCE_DATA)source;
//	}

	/**
	 * @return the 'sourceIdentifier' value
	 */
	final public DataIdentifier getSourceIdentifier() {
		return this._sourceIdentifier;
	}

	/**
	 * @param sourceData the 'sourceData' value to set
	 */
	final public void setSourceData(SOURCE_DATA sourceData) {
		this._sourceData = sourceData;
	}

	/**
	 * @param sourceIdentifier the 'sourceIdentifier' value to set
	 */
	final public void setSourceIdentifier(DataIdentifier sourceIdentifier) {
		this._sourceIdentifier = $nN(sourceIdentifier, "The source identifier cannot be null");
	}

	/**
	 * @return the 'matchingTargetData' value
	 */
	final public Collection<TARGET_DATA> getMatchingTargetData() {
		return this._matchingTargetData;
	}


	/**
	 * @param matchingTargetsData the 'matchingTargetsData' value to set
	 */
	final public void setMatchingTargetData(Collection<TARGET_DATA> matchingTargetsData) {
		this._matchingTargetData = $nN(matchingTargetsData , "Matching targets data cannot be null");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[ S: " + this._score + ", " +
			     "SD: " + this._sourceData + ", " +
			     "TD: " + this._matchingTargetData + " ]";
	}
}