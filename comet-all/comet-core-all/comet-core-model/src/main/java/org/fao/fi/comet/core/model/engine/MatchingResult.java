/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.fao.fi.comet.core.model.engine.adapters.MatchingResultAdapter;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29/giu/2010   Fabio     Creation.
 *
 * @version 1.0
 * @since 29/giu/2010
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchingResult")
@XmlJavaTypeAdapter(MatchingResultAdapter.class)
final public class MatchingResult<SOURCE_DATA, TARGET_DATA> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5086733156593502974L;

	/** A reference to the ID of the matchlet originating this result */
	@XmlAttribute(name="matchletId")
	private String _originatingMatchletId;

	/** A reference to the name of the matchlet originating this result */
	@XmlAttribute(name="matchletName")
	private String _originatingMatchletName;

	@XmlAttribute(name="matchletWeight")
	private Double _originatingMatchletWeight;

	@XmlTransient
	private MatchingSerializationExclusionPolicy[] _originatingMatchletExclusionPolicies;

	@XmlTransient
	private MatchingScore _score = new MatchingScore();

	/** The matching results */
	@XmlTransient
	private Collection<MatchingResultData<SOURCE_DATA, TARGET_DATA>> _matchingResultsData = new ArrayList<MatchingResultData<SOURCE_DATA, TARGET_DATA>>();

	/**
	 * Class constructor
	 *
	 * Required by JAXB
	 */
	public MatchingResult() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param originatingMatchlet
	 */
	public MatchingResult(Matchlet<?, SOURCE_DATA, ?, TARGET_DATA> originatingMatchlet) {
		super();

		this.configureMatchletMetadata(originatingMatchlet);
	}

	/**
	 * Class constructor
	 *
	 * @param originatingMatchlet
	 * @param score
	 * @param matchingResults
	 */
	public MatchingResult(Matchlet<?, SOURCE_DATA, ?, TARGET_DATA> originatingMatchlet, MatchingScore score, Collection<MatchingResultData<SOURCE_DATA, TARGET_DATA>> matchingResults) {
		super();

		this.configureMatchletMetadata(originatingMatchlet);
		this.setScore(score);
		this.setMatchingsResultData(matchingResults);
	}

	/**
	 * @return the 'score' value
	 */
	@XmlTransient
	public MatchingScore getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public void setScore(MatchingScore score) {
		$nN(score, "The score cannot be null");

		this._score = score;
	}

	/**
	 * @return the 'originatingMatchletWeight' value
	 */
	public Double getOriginatingMatchletWeight() {
		return this._originatingMatchletWeight;
	}

	/**
	 * @param originatingMatchletWeight the 'originatingMatchletWeight' value to set
	 */
	public void setOriginatingMatchletWeight(Double originatingMatchletWeight) {
		this._originatingMatchletWeight = originatingMatchletWeight;
	}

	/**
	 * @return the 'originatingMatchletExclusionPolicies' value
	 */
	public MatchingSerializationExclusionPolicy[] getOriginatingMatchletExclusionPolicies() {
		return this._originatingMatchletExclusionPolicies;
	}

	/**
	 * @param originatingMatchletExclusionPolicies the 'originatingMatchletExclusionPolicies' value to set
	 */
	public void setOriginatingMatchletExclusionPolicies(MatchingSerializationExclusionPolicy[] originatingMatchletExclusionPolicies) {
		this._originatingMatchletExclusionPolicies = originatingMatchletExclusionPolicies;
	}

	@XmlAttribute(name="score")
	public double getMatchletScoreValue() {
		return this._score.getValue();
	}

	public void setMatchletScoreValue(double scoreValue) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setValue(scoreValue);
	}

	@XmlAttribute(name="type")
	public MatchingType getMatchletScoreType() {
		return this._score.getMatchingType();
	}

	public void setMatchletScoreType(MatchingType scoreType) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setMatchingType(scoreType);
	}

	/**
	 * @param originatingMatchlet
	 */
	private void configureMatchletMetadata(Matchlet<?, SOURCE_DATA, ?, TARGET_DATA> originatingMatchlet) {
		$nN(originatingMatchlet, "The originating matchlet cannot be NULL");

		this._originatingMatchletId = originatingMatchlet.getId();
		this._originatingMatchletName = originatingMatchlet.getType();
		this._originatingMatchletWeight = originatingMatchlet.getWeight();

		//To avoid referencing the originating matchlet (that might contain lots of payload: e.g. matchlets that brings
		//references to relationship graphs) we must determine the actual matchlet exclusion cases and store them)
		MatchingSerializationExclusionPolicy[] exclusionCases = originatingMatchlet.getExclusionCases();

		if(exclusionCases == null || exclusionCases.length == 0) {
			if(originatingMatchlet.getClass().isAnnotationPresent(MatchletDefaultSerializationExclusionPolicy.class))
				this._originatingMatchletExclusionPolicies = originatingMatchlet.getClass().getAnnotation(MatchletDefaultSerializationExclusionPolicy.class).value();
		} else {
			this._originatingMatchletExclusionPolicies = exclusionCases;
		}
	}

	/**
	 * @return the 'dataMatchings' value
	 */
	@XmlElementWrapper(name="MatchingResultsData")
	@XmlElement(name="MatchingResultData")
	public Collection<MatchingResultData<SOURCE_DATA, TARGET_DATA>> getMatchingsResultData() {
		return this._matchingResultsData;
	}

	/**
	 * @param dataMatchings the 'dataMatchings' value to set
	 */
	public void setMatchingsResultData(Collection<MatchingResultData<SOURCE_DATA, TARGET_DATA>> dataMatchings) {
		this._matchingResultsData = $nN(dataMatchings, "The data matchings cannot be null");
	}

	final public boolean containsMatchingFor(SOURCE_DATA sourceData, DataIdentifier sourceIdentifier) {
		return this.getMatchingsResultDataFor(sourceData, sourceIdentifier) != null;
	}

	final public MatchingResultData<SOURCE_DATA, TARGET_DATA> getMatchingsResultDataFor(SOURCE_DATA sourceData, DataIdentifier sourceIdentifier) {
		SOURCE_DATA currentData;
		for(MatchingResultData<SOURCE_DATA, TARGET_DATA> currentDataMatching : this.getMatchingsResultData()) {
			currentData = currentDataMatching.getSourceData();

			if(currentData != null && currentData.equals(sourceData))
				return currentDataMatching;
		}

		return null;
	}

	/**
	 * @return true if the score value means NO MATCH
	 */
	public boolean isNoMatch() {
		return this._score.isNoMatch();
	}

	/**
	 * @return true if the score value means FULL MATCH
	 */
	public boolean isFullMatch() {
		return this._score.isFullMatch();
	}

	/**
	 * @return true if the matching type is authoritative
	 */
	public boolean isAuthoritative() {
		return this._score.isAuthoritative();
	}

	/**
	 * @return true if the matching type is non authoritative
	 */
	public boolean isNonAuthoritative() {
		return this._score.isNonAuthoritative();
	}

	/**
	 * @return true if the matching type means "non performed"
	 */
	public boolean isNonPerformed() {
		return this._score.isNonPerformed();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[ M: " + this._originatingMatchletName + ", " +
				 "MW: " + this._originatingMatchletWeight + ", " +
				 "MS: " + this._score + ", " +
				 "MRSD: " + this._matchingResultsData + " ]";
	}

	public boolean excludeFromSerialization() {
		$nN(this._score, "The matching score cannot be null");

		if(this._originatingMatchletExclusionPolicies != null)
			for(MatchingSerializationExclusionPolicy exclusionCase : this._originatingMatchletExclusionPolicies) {
				if(exclusionCase.shouldExclude(this._score))
					return true;
			}

		return false;
	}


	/**
	 * @return the 'originatingMatchletName' value
	 */
	@XmlTransient
	public String getOriginatingMatchletName() {
		return this._originatingMatchletName;
	}

	/**
	 * @param originatingMatchletName the 'originatingMatchletName' value to set
	 */
	public void setOriginatingMatchletName(String originatingMatchletName) {
		this._originatingMatchletName = originatingMatchletName;
	}

	/**
	 * @return the 'originatingMatchletId' value
	 */
	@XmlTransient
	public String getOriginatingMatchletId() {
		return this._originatingMatchletId;
	}

	/**
	 * @param originatingMatchletId the 'originatingMatchletId' value to set
	 */
	public void setOriginatingMatchletId(String originatingMatchletId) {
		this._originatingMatchletId = originatingMatchletId;
	}
}