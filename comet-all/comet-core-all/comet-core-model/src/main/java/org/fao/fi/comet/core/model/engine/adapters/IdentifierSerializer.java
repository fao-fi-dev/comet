/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters;

import java.io.Serializable;

/**
 * @author Fiorellato
 *
 */
public interface IdentifierSerializer<I extends Serializable & Comparable<? super I>> {
	I from(String value);
	String to(I value);
}
