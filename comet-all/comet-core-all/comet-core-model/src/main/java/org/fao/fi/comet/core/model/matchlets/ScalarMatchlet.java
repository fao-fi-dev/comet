/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface ScalarMatchlet<SOURCE,
							   SOURCE_DATA extends Serializable,
							   TARGET,
							   TARGET_DATA extends Serializable> extends Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	SOURCE_DATA extractSourceData(SOURCE source, DataIdentifier sourceIdentifier);
	TARGET_DATA extractTargetData(TARGET target, DataIdentifier targetIdentifier);
}