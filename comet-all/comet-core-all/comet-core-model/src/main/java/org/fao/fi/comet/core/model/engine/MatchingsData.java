/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$pos;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$true;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.fao.fi.comet.core.model.engine.adapters.MatchingDetailsMapAdapter;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Aug 2010
 */
@XmlType(name="MatchingsData")
@XmlAccessorType(XmlAccessType.FIELD)
final public class MatchingsData<SOURCE, TARGET> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7588216518182815655L;

	@XmlJavaTypeAdapter(MatchingDetailsMapAdapter.class)
	@XmlElement(name="MatchingsData")
	private Map<String, MatchingDetails<SOURCE, TARGET>> _matchingDetailsMap;

	@XmlTransient
	final public Comparator<Matching<SOURCE, TARGET>> MATCHINGS_COMPARATOR = new Comparator<Matching<SOURCE, TARGET>>() {
		@Override
		public int compare(Matching<SOURCE, TARGET> w1, Matching<SOURCE, TARGET> w2) {
			double lambda1 = w1.getScore().getValue();
			double lambda2 = w2.getScore().getValue();

			return Double.compare(lambda2, lambda1);
		}
	};

	/**
	 * Class constructor
	 */
	public MatchingsData() {
		this(100, .1f);
	}

	/**
	 * Class constructor
	 *
	 * @param initialCapacity
	 * @param loadFactor
	 */
	public MatchingsData(int initialCapacity, float loadFactor) {
		$pos(initialCapacity, "Initial capacity must be greater than zero (currently: {})", initialCapacity);
		$pos(loadFactor, "Load factor must be greater than zero (currently: {})", loadFactor);

		this._matchingDetailsMap = Collections.synchronizedMap(new HashMap<String, MatchingDetails<SOURCE,TARGET>>(initialCapacity, loadFactor));
	}

	/**
	 * Class constructor
	 *
	 * @param backingMap
	 */
	public MatchingsData(Map<String, MatchingDetails<SOURCE,TARGET>> backingMap) {
		$nN(backingMap, "Backing map cannot be null");

		this._matchingDetailsMap = Collections.synchronizedMap(backingMap);
	}

	@XmlTransient
	public Collection<MatchingDetails<SOURCE,TARGET>> getMatchingDetails() {
		return this._matchingDetailsMap == null ? new ArrayList<MatchingDetails<SOURCE,TARGET>>() : new ArrayList<MatchingDetails<SOURCE,TARGET>>(this._matchingDetailsMap.values());
	}

	@XmlAttribute(name="numIdentifiedEntries")
	public int getNumIdentifiedEntries() {
		return this._matchingDetailsMap.size();
	}

	public void setNumIdentifiedEntries(int numIdentifiedEntries) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}

	public Map<String, MatchingDetails<SOURCE, TARGET>> asMap() {
		return this._matchingDetailsMap;
	}

	/**
	 * @param another
	 * @return
	 */
	public MatchingsData<SOURCE, TARGET> join(MatchingsData<SOURCE, TARGET> another) {
		$nN(another, "Provided matching data cannot be null");
		$nN(another._matchingDetailsMap, "Provided matching data cannot have a null backing data map");

		MatchingDetails<SOURCE,TARGET> details;
		for(String matchingsDataKey : another._matchingDetailsMap.keySet()) {
			details = another._matchingDetailsMap.get(matchingsDataKey);

			if(!details._matchingsMap.isEmpty()) {
				if(this._matchingDetailsMap.containsKey(matchingsDataKey)) {
					this._matchingDetailsMap.get(matchingsDataKey).join(details);
				} else {
					this._matchingDetailsMap.put(matchingsDataKey, SerializationHelper.clone(details));
				}
			}
		}

		return this;
	}

	public MatchingsData<SOURCE, TARGET> cleanup(int maxCandidatesPerEntry) {
		return this.cleanup(maxCandidatesPerEntry, Double.MIN_VALUE, false);
	}

	public MatchingsData<SOURCE, TARGET> cleanup(double weightedScoreThreshold) {
		return this.cleanup(Integer.MIN_VALUE, weightedScoreThreshold, false);
	}

	public MatchingsData<SOURCE, TARGET> cleanup(int maxCandidatesPerEntry, double weightedScoreThreshold) {
		return this.cleanup(maxCandidatesPerEntry, weightedScoreThreshold, false);
	}

	public MatchingsData<SOURCE, TARGET> cleanup(int maxCandidatesPerEntry, double weightedScoreThreshold, boolean keepData) {
		boolean filterNumCandidates = maxCandidatesPerEntry > 0;
		boolean filterScore = Double.compare(weightedScoreThreshold, 0D) > 0;

		if(!filterNumCandidates && !filterScore)
			return this;

		for(MatchingDetails<SOURCE, TARGET> details : this.getMatchingDetails()) {
			if(!keepData)
				details.setSource(null);

			if(details != null && details.getSortedUniqueMatchings() != null) {
				List<Matching<SOURCE, TARGET>> entries = new ArrayList<Matching<SOURCE, TARGET>>(details.getMatchings());

				Collections.sort(entries, MATCHINGS_COMPARATOR);

				if(filterScore)
					for(Matching<SOURCE, TARGET> matching : entries) {
						if(!keepData)
							matching.setTarget(null);

						if(Double.compare(weightedScoreThreshold, matching.getScoreValue()) > 0)
							details.removeMatchingByTargetId(matching.getTargetIdentifier());
					}

				entries = new ArrayList<Matching<SOURCE, TARGET>>(details.getMatchings());

				Collections.sort(entries, MATCHINGS_COMPARATOR);

				if(filterNumCandidates)
					while(entries.size() > maxCandidatesPerEntry) {
						details.removeMatchingByTargetId(entries.get(entries.size() - 1).getTargetIdentifier());

						entries.remove(entries.size() - 1);
					}

				if(details != null && ( details.getMatchings() == null || details.getMatchings().isEmpty() )) {
					this.removeMatchingDetailsBySourceIdentifier(details.getSourceIdentifier());
				}
			}
		}

		return this;
	}

	public MatchingsData<SOURCE, TARGET> filter(double minScore, int maxCandidates) {
		$gte(minScore, MatchingScore.SCORE_NO_MATCH, "The joined matchings data minimum score must be greater than (or equal to) {} (currently: {})", MatchingScore.SCORE_NO_MATCH, minScore);
		$lte(minScore, MatchingScore.SCORE_FULL_MATCH, "The joined matchings data minimum score must be lower than (or equal to) {} (currently: {})", MatchingScore.SCORE_FULL_MATCH, minScore);
		$true(maxCandidates >= 0, "The joined matchings data maximum candidates per entry must be greater than (or equal to) 0 (currently: {})", maxCandidates);

		MatchingDetails<SOURCE,TARGET> details;
		Collection<Matching<SOURCE, TARGET>> updated;
		Set<String> keySet = this._matchingDetailsMap.keySet();

		final boolean limitCandidates = maxCandidates > 0;

		for(String matchingsDataKey : keySet) {
			details = this._matchingDetailsMap.get(matchingsDataKey);
			updated = new ArrayList<Matching<SOURCE, TARGET>>();

			for(Matching<SOURCE, TARGET> matching : details.getSortedUniqueMatchings()) {
				if(limitCandidates && updated.size() == maxCandidates)
					break;

				if(Double.compare(matching.getScore().getValue(), minScore) >=0 )
					updated.add(matching);
			}

			details._matchingsMap.clear();

			for(Matching<SOURCE, TARGET> matching : updated)
				details._matchingsMap.put(matching.getTargetId(), matching);

			this._matchingDetailsMap.put(matchingsDataKey, details);
		}

		return this;
	}

	/**
	 * Retrieves the matching details for a source id
	 *
	 * @param sourceId a source id
	 * @return the matching details for the source id
	 */
	public MatchingDetails<SOURCE,TARGET> findMatchingDetailsBySourceIdentifier(DataIdentifier sourceIdentifier) {
		return this._matchingDetailsMap.get(sourceIdentifier.toUID());
	}

	/**
	 * Removes the matching details for a source id
	 *
	 * @param sourceId a source id
	 * @return the matching details for the source id
	 */
	public MatchingDetails<SOURCE,TARGET> removeMatchingDetailsBySourceIdentifier(DataIdentifier sourceIdentifier) {
		return this._matchingDetailsMap.remove(sourceIdentifier.toUID());
	}

	/**
	 * Builds or retrieves the matching details for a source id
	 *
	 * @param sourceId a target id
	 * @return the matching details for the source id, either retrieved or built from scratch
	 */
	public MatchingDetails<SOURCE,TARGET> lazyGetMatchingDetails(SOURCE source, DataIdentifier sourceIdentifier) {
		//Access the matching details with the source ID
		MatchingDetails<SOURCE,TARGET> matchingDetails = this.findMatchingDetailsBySourceIdentifier(sourceIdentifier);

		//Create it if it doesn't exist yet
		if(matchingDetails == null) {
			matchingDetails = new MatchingDetails<SOURCE,TARGET>();
			matchingDetails.setSource(source);
			matchingDetails.setSourceIdentifier(sourceIdentifier);

			this._matchingDetailsMap.put(sourceIdentifier.toUID(), matchingDetails);
		}

		return matchingDetails;
	}

	/**
	 * Builds or retrieves a matching given the source id, the target id and their current matching result
	 *
	 * @param sourceId a source id
	 * @param targetId a target id
	 * @param currentMatchingResult their current matching result
	 * @return the matching for the source id, target id and current matching result
	 */
	public Matching<SOURCE, TARGET> getMatching(DataIdentifier sourceIdentifier,
									  DataIdentifier targetIdentifier) {
		return this.findMatchingDetailsBySourceIdentifier(sourceIdentifier).
					findMatchingByTargetId(targetIdentifier);
	}

	/**
	 * Builds or retrieves a matching given the source id, the target id and their current matching result
	 *
	 * @param sourceId a source id
	 * @param targetId a target id
	 * @param currentMatchingResult their current matching result
	 * @return the matching for the source id, target id and current matching result
	 */
	public Matching<SOURCE, TARGET> lazyGetMatching(SOURCE source, DataIdentifier sourceIdentifier,
													TARGET target, DataIdentifier targetIdentifier) {
		//Access the matching details by the source ID or build it if it ain't been created yet
		MatchingDetails<SOURCE,TARGET> matchingDetails = this.lazyGetMatchingDetails(source, sourceIdentifier);

		final String targetUID = targetIdentifier.toUID();

		//Try to access the matching by target ID..
		Matching<SOURCE, TARGET> matching = matchingDetails._matchingsMap.get(targetUID);

		//Create it if it doesn't exist yet
		if(matching == null) {
			matching = new Matching<SOURCE, TARGET>();
			matching.setSourceIdentifier(sourceIdentifier);
			matching.setTargetIdentifier(targetIdentifier);

			matching.setTarget(target);

			matching.setScore(new MatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.NON_PERFORMED));

			matchingDetails._matchingsMap.put(targetUID, matching);
		}

		return matching;
	}

	/**
	 * Builds or retrieves a matching given the source id, the target id and their current matching result
	 *
	 * @param sourceId a source id
	 * @param targetId a target id
	 * @param currentMatchingResult their current matching result
	 * @return the matching for the source id, target id and matching result, either retrieved or built from scratch
	 */
	public Matching<SOURCE, TARGET> lazyUpdateMatching(SOURCE source, DataIdentifier sourceIdentifier,
													   TARGET target, DataIdentifier targetIdentifier,
													   MatchingResult<?, ?> currentMatchingResult) {
		Matching<SOURCE, TARGET> matching = this.lazyGetMatching(source, sourceIdentifier, target, targetIdentifier);
		matching.updateMatchingResult(currentMatchingResult);

		return matching;
	}

	/**
	 * @return
	 */
	public synchronized double meanScore() {
		double score = 0D;
		int entries = 0;

		double scorePerMatching = 0D;
		int entriesPerMatching = 0;

		Set<Map.Entry<String, MatchingDetails<SOURCE,TARGET>>> mainEntries = new HashSet<Map.Entry<String, MatchingDetails<SOURCE,TARGET>>>(this._matchingDetailsMap.entrySet());
		Set<Map.Entry<String, Matching<SOURCE, TARGET>>> matchingEntries = null;

		for(Map.Entry<String, MatchingDetails<SOURCE,TARGET>> entry : mainEntries) {
			matchingEntries = new HashSet<Map.Entry<String, Matching<SOURCE, TARGET>>>(entry.getValue()._matchingsMap.entrySet());

			for(Map.Entry<String, Matching<SOURCE, TARGET>> matching : matchingEntries) {
				entriesPerMatching++;
				scorePerMatching += matching.getValue().getScore().getValue();
			}

			if(entriesPerMatching != 0 && Double.compare(scorePerMatching, 0D) > 0) {
				score += scorePerMatching / entriesPerMatching;
				entries++;
			}

			scorePerMatching = 0;
			entriesPerMatching = 0;
		}

		if(entries == 0)
			return 0;

		return score / entries;
	}
}