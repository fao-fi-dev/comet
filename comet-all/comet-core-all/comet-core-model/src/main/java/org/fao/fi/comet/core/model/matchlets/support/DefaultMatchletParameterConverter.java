/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Collections;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
public class DefaultMatchletParameterConverter implements MatchletParameterConverter {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.MatchletParameterConverter#convertToString(java.lang.Object)
	 */
	@Override
	public String convertToString(Object value) {
		if(value == null)
			return null;

		if(value.getClass().isArray())
			return CollectionsHelper.join((Object[])value, "|");

		if(Collections.class.isAssignableFrom(value.getClass()))
			return CollectionsHelper.join((Collection<?>)value, "([^|]|([^|])");

		return value.toString();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConverter#convertToArrayFromString(java.lang.Class, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <C> Object convertToArrayFromString(Class<C> componentClazz, String value) {
		String[] values = value.split("\\|");

		C[] array = (C[])Array.newInstance(componentClazz, values.length);

		int counter = 0;
		for(String currentValue : values)
			array[counter++] = (C)this.convertFromString(componentClazz, currentValue);

		return array;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.MatchletParameterConverter#convertFromString(java.lang.String)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object convertFromString(Class<?> clazz, String value) {
		if(value == null)
			return null;

		if(clazz.isEnum())
			return Enum.valueOf((Class<Enum>)clazz, value);

		if(String.class.equals(clazz))
			return value;

		if(clazz.isPrimitive()) {
			String clazzName = clazz.getName();
			if("boolean".equals(clazzName))
				return Boolean.valueOf(value);
			else if("byte".equals(clazzName))
				return Byte.valueOf(value);
			else if("short".equals(clazzName))
				return Short.valueOf(value);
			else if("int".equals(clazzName))
				return Integer.valueOf(value);
			else if("long".equals(clazzName))
				return Long.valueOf(value);
			else if("float".equals(clazzName))
				return Float.valueOf(value);
			else if("double".equals(clazzName))
				return Double.valueOf(value);
			else if("char".equals(clazzName)) {
				if(value.length() > 1)
					throw new IllegalArgumentException("Cannot assign a char value from '" + value + "'");
				else if(value.length() == 0)
					return null;

				return new Character(value.charAt(0));
			}
		} else {
			if(Character.class.equals(clazz)) {
				if(value.length() > 1)
					throw new IllegalArgumentException("Cannot assign a char value from '" + value + "'");

				else if(value.length() == 0)
					return null;

				return new Character(value.charAt(0));
			}

			Constructor<?> constructor = null;
			try {
				constructor = clazz.getConstructor(String.class);
				constructor.setAccessible(true);

				return constructor.newInstance(value);
			} catch (Throwable t) {
				;
			}
		}

		throw new IllegalArgumentException("Cannot assign a value to field of type " + clazz + " from string value '" + value + "'");
	}
}