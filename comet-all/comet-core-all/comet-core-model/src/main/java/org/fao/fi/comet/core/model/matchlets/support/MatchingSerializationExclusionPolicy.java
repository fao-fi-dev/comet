/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Feb 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Feb 2011
 */
public enum MatchingSerializationExclusionPolicy {
	ALWAYS(0),
	NEVER(1),
	NON_PERFORMED(2),
	AUTHORITATIVE(3),
	NO_MATCH(4),
	AUTHORITATIVE_NO_MATCH(5),
	MATCH(6),
	FULL_MATCH (7),
	AUTHORITATIVE_FULL_MATCH(8);

	int _when;

	private MatchingSerializationExclusionPolicy(int when) {
		this._when = when;
	}

	public int getWhen() {
		return this._when;
	}

	/**
	 * @param score
	 * @return
	 */
	public boolean shouldExclude(MatchingScore score) {
		$nN(score, "Score cannot be null");

		if(this == ALWAYS)
			return true;

		if(this == NEVER)
			return false;

		switch(this) {
			case NON_PERFORMED:
				return score.isNonPerformed();
			case AUTHORITATIVE:
				return score.isAuthoritative();
			case NO_MATCH:
			case AUTHORITATIVE_NO_MATCH:
				return score.isNoMatch();
			case MATCH:
				return !score.isNoMatch(); //Implicitly checks non-performed status too
			case FULL_MATCH:
				return score.isFullMatch();
			case AUTHORITATIVE_FULL_MATCH:
				return score.isAuthoritative() && score.isFullMatch();

			default:
				return false;
		}
	}
}
