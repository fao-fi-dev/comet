/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.exceptions.MatchingEngineConfigurationException;
import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.score.support.MatchingScore;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 24/mag/2013
 */
@XmlType(name="MatchingEngineProcessConfiguration")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingEngineProcessConfiguration implements Serializable {
	static final public int MAX_CANDIDATES_PER_ENTRY_UNBOUND = 0;

	static final public boolean HALT_AT_FIRST_VALID_MATCHING 	  = true;
	static final public boolean DONT_HALT_AT_FIRST_VALID_MATCHING = !HALT_AT_FIRST_VALID_MATCHING;

	static final public boolean HANDLE_ERRORS	   = true;
	static final public boolean DONT_HANDLE_ERRORS = !HANDLE_ERRORS;

	/** Field serialVersionUID */
	private static final long serialVersionUID = 3680496607335454252L;


	/** The minimum weighted score allowed for a match */
	@XmlElement(name="MinimumAllowedWeightedScore", nillable=true)
	protected Double _minimumAllowedWeightedScore;

	/** The maximum number of candidates to consider per entry (sorted in descending score value) */
	@XmlElement(name="MaxCandidatesPerEntry", nillable=true)
	protected Integer _maxCandidatesPerEntry;

	/** Tells whether to halt the current comparison run as soon as the first valid matching (i.e. higher than or equal to the minimum score) is detected */
	@XmlElement(name="HaltAtFirstValidMatching", nillable=true)
	protected Boolean _haltAtFirstValidMatching;

	/** If <code>true</code> instructs the engine to handle errors that might happen during the comparison and not terminate. */
	@XmlElement(name="HandleErrors", nillable=true)
	protected Boolean _handleErrors;

	@XmlElementWrapper(name="MatchletsConfigurations", nillable=false)
	@XmlElement(name="MatchletConfiguration", nillable=false)
	protected Collection<MatchletConfiguration> _matchletsConfigurations;

	public MatchingEngineProcessConfiguration() {
		super();

		this._matchletsConfigurations = new ArrayList<MatchletConfiguration>();
	}

	/**
	 * @return the 'minimumAllowedWeightedScore' value
	 */
	public Double getMinimumAllowedWeightedScore() {
		return this._minimumAllowedWeightedScore;
	}

	/**
	 * @return the 'maxCandidatesPerEntry' value
	 */
	public Integer getMaxCandidatesPerEntry() {
		return this._maxCandidatesPerEntry;
	}

	/**
	 * @return the 'haltAtFirstValidMatching' value
	 */
	public Boolean getHaltAtFirstValidMatching() {
		return this._haltAtFirstValidMatching;
	}

	/**
	 * @param minimumAllowedWeightedScore the 'minimumAllowedWeightedScore' value to set
	 */
	public void setMinimumAllowedWeightedScore(Double minimumAllowedWeightedScore) {
		this._minimumAllowedWeightedScore = minimumAllowedWeightedScore;
	}

	/**
	 * @param maxCandidatesPerEntry the 'maxCandidatesPerEntry' value to set
	 */
	public void setMaxCandidatesPerEntry(Integer maxCandidatesPerEntry) {
		this._maxCandidatesPerEntry = maxCandidatesPerEntry;
	}

	/**
	 * @param haltAtFirstValidMatching the 'haltAtFirstValidMatching' value to set
	 */
	public void setHaltAtFirstValidMatching(Boolean haltAtFirstValidMatching) {
		this._haltAtFirstValidMatching = haltAtFirstValidMatching;
	}

	/**
	 * @return the 'handleErrors' value
	 */
	public Boolean getHandleErrors() {
		return this._handleErrors;
	}

	/**
	 * @param handleErrors the 'handleErrors' value to set
	 */
	public void setHandleErrors(Boolean handleErrors) {
		this._handleErrors = handleErrors;
	}

	/**
	 * @return the 'matchletsConfiguration' value
	 */
	public Collection<MatchletConfiguration> getMatchletsConfigurations() {
		return this._matchletsConfigurations;
	}

	/**
	 * @param matchletsConfiguration the 'matchletsConfiguration' value to set
	 */
	public void setMatchletsConfiguration(Collection<MatchletConfiguration> matchletsConfiguration) {
		this._matchletsConfigurations = matchletsConfiguration;
	}

	public void validate() throws MatchingEngineConfigurationException, MatchletConfigurationException {
		try {
			if(this._minimumAllowedWeightedScore != null) {
				$gt(this._minimumAllowedWeightedScore, MatchingScore.SCORE_NO_MATCH, "Minimum allowed weighted score ({}) cannot be lower than (or equal to) the minimum valid score ({})", this._minimumAllowedWeightedScore, MatchingScore.SCORE_NO_MATCH);
				$lte(this._minimumAllowedWeightedScore, MatchingScore.SCORE_FULL_MATCH, "Minimum allowed weighted score ({}) cannot be greater than the maximum valid score ({})", this._minimumAllowedWeightedScore, MatchingScore.SCORE_FULL_MATCH);
			} else {
				this._minimumAllowedWeightedScore = 0.3D;
			}

			if(this._maxCandidatesPerEntry != null) {
				$nNeg(this._maxCandidatesPerEntry, "Maximum candidates per entry must be greater than or equal to zero (currently: {})", this._maxCandidatesPerEntry);

				if(this._haltAtFirstValidMatching != null)
					$false(this._maxCandidatesPerEntry > 1 && this._haltAtFirstValidMatching, "You cannot set the max number of candidates per entry (currently: {}) and the 'halt at first valid matching' flag", this._maxCandidatesPerEntry);
			} else {
				this._maxCandidatesPerEntry = MAX_CANDIDATES_PER_ENTRY_UNBOUND;
			}

			if(this._haltAtFirstValidMatching == null)
				this._haltAtFirstValidMatching = DONT_HALT_AT_FIRST_VALID_MATCHING;

			if(this._handleErrors == null)
				this._handleErrors = DONT_HANDLE_ERRORS;
		} catch (IllegalArgumentException IAe) {
			throw new MatchingEngineConfigurationException(IAe.getMessage());
		}
	}
}