/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jun 2013
 */
public class MatchingEngineConfigurationException extends MatchingEngineException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1248131267895175537L;

	/**
	 * Class constructor
	 *
	 */
	public MatchingEngineConfigurationException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MatchingEngineConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MatchingEngineConfigurationException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MatchingEngineConfigurationException(Throwable cause) {
		super(cause);
	}
}
