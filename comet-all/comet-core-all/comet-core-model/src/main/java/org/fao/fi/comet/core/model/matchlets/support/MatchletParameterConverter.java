/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
public interface MatchletParameterConverter {
	String convertToString(Object value);
	Object convertFromString(Class<?> clazz, String value);
	<C> Object convertToArrayFromString(Class<C> componentClazz, String value);
}
