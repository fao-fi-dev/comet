@XmlSchema(namespace = "http://common.model.core.comet.fi.fao.org", elementFormDefault = XmlNsForm.UNQUALIFIED)
package org.fao.fi.comet.core.model.common;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
