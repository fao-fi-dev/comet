/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks matchlets metadata that should be considered when serializing
 * the matchlet into its XML form, allowing the override of metadata
 * name and description
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Aug 2010
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MatchletMetadata {
	/**
	 * @return the matchlet's metadata name (when specified)
	 */
	String name() default "";

	/**
	 * @return the matchlet's metadata description (when specified)
	 */
	String description() default "";
}
