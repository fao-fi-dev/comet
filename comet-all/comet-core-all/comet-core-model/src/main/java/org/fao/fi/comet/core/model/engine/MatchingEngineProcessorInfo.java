/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.net.InetAddress;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.core.helpers.singletons.io.MD5Helper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 13/ott/2013
 */
@XmlType(name="MatchingEngineProcessorInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingEngineProcessorInfo<CONFIG extends MatchingEngineProcessConfiguration> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3406956100846096504L;

	@XmlAttribute(name="processID")
	private String _processID;

	@XmlAttribute(name="processExecutorIP")
	private String _processExecutorIP;

	@XmlAttribute(name="processExecutorName")
	private String _processExecutorName;

	@XmlAttribute(name="processExecutorThreadName")
	private String _processExecutorThreadName;

	@XmlElement(name="Configuration")
	private CONFIG _configuration;

	@XmlElement(name="Status")
	private MatchingEngineProcessStatus _status;

	/**
	 * Class constructor
	 *
	 */
	public MatchingEngineProcessorInfo() {
		super();
	}

	/**
	 * Class constructor
	 *
	 */
	public MatchingEngineProcessorInfo(CONFIG configuration, MatchingEngineProcessStatus status) {
		super();

		try {
			InetAddress localhost = InetAddress.getLocalHost();

			this._processExecutorIP = localhost.getHostAddress();
			this._processExecutorName = localhost.getCanonicalHostName();
		} catch (Throwable t) {
			this._processExecutorIP = "127.0.0.1";
			this._processExecutorName = "localhost";
		}

		this._processExecutorThreadName = Thread.currentThread().getName();
		this._processID = System.currentTimeMillis() + ":" + this._processExecutorIP + ":" + this._processExecutorName + ":" + this._processExecutorThreadName;

		try {
			this._processID = MD5Helper.digest(this._processID);
		} catch (Throwable t) {
			;
		}

		this._status = status;
		this._configuration = configuration;
	}

	/**
	 * @return the 'processID' value
	 */
	public String getProcessID() {
		return this._processID;
	}

	/**
	 * @return the 'processExecutorIP' value
	 */
	public String getProcessExecutorIP() {
		return this._processExecutorIP;
	}

	/**
	 * @return the 'processExecutorName' value
	 */
	public String getProcessExecutorName() {
		return this._processExecutorName;
	}

	/**
	 * @return the 'processExecutorThreadName' value
	 */
	public String getProcessExecutorThreadName() {
		return this._processExecutorThreadName;
	}

	/**
	 * @return the 'status' value
	 */
	public MatchingEngineProcessStatus getStatus() {
		return this._status;
	}

	/**
	 * @param status the 'status' value to set
	 */
	public void setStatus(MatchingEngineProcessStatus status) {
		this._status = status;
	}

	/**
	 * @return the 'configuration' value
	 */
	public CONFIG getConfiguration() {
		return this._configuration;
	}

	/**
	 * @param configuration the 'configuration' value to set
	 */
	public void setConfiguration(CONFIG configuration) {
		this._configuration = SerializationHelper.clone($nN(configuration, "Please provide a non-null reference to the originating matching configuration"));
	}
}
