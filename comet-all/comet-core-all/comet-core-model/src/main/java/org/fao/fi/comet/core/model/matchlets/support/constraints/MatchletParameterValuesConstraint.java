/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterValuesConstraint")
public class MatchletParameterValuesConstraint<V> extends MatchletParameterConstraint {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4997538027522537645L;

	@XmlElement(name="values")
	private V[] _values;
	
	@XmlElement(name="multiple")
	private boolean _multiple;

	/**
	 * @return the 'values' value
	 */
	public V[] getValues() {
		return this._values;
	}

	/**
	 * @param values the 'values' value to set
	 */
	public void setValues(V[] values) {
		this._values = values;
	}

	/**
	 * @return the 'multiple' value
	 */
	public boolean isMultiple() {
		return this._multiple;
	}

	/**
	 * @param multiple the 'multiple' value to set
	 */
	public void setMultiple(boolean multiple) {
		this._multiple = multiple;
	}
}