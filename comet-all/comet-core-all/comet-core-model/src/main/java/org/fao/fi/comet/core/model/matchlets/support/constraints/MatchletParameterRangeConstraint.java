/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterRangeConstraint")
public class MatchletParameterRangeConstraint<N extends Number> extends MatchletParameterConstraint {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4997538027522537645L;

	@XmlElement(name="from")
	private N _from;
	
	@XmlElement(name="to")
	private N _to;
	
	@XmlElement(name="includeFrom")
	private boolean _includeFrom;
	
	@XmlElement(name="includeTo")
	private boolean _includeTo;

	/**
	 * Class constructor
	 */
	public MatchletParameterRangeConstraint() {
		super();
	}
	
	/**
	 * @return the 'from' value
	 */
	public N getFrom() {
		return this._from;
	}

	/**
	 * @param from the 'from' value to set
	 */
	public void setFrom(N from) {
		this._from = from;
	}

	/**
	 * @return the 'to' value
	 */
	public N getTo() {
		return this._to;
	}

	/**
	 * @param to the 'to' value to set
	 */
	public void setTo(N to) {
		this._to = to;
	}

	/**
	 * @return the 'includeFrom' value
	 */
	public boolean getIncludeFrom() {
		return this._includeFrom;
	}

	/**
	 * @param includeFrom the 'includeFrom' value to set
	 */
	public void setIncludeFrom(boolean includeFrom) {
		this._includeFrom = includeFrom;
	}

	/**
	 * @return the 'includeTo' value
	 */
	public boolean getIncludeTo() {
		return this._includeTo;
	}

	/**
	 * @param includeTo the 'includeTo' value to set
	 */
	public void setIncludeTo(boolean includeTo) {
		this._includeTo = includeTo;
	}
}