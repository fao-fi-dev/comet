/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterInfo")
public class MatchletParameterInfo implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4122888860022402212L;

	@XmlElement(name="MatchletParameterName")
	private String _parameterName;

	@XmlAttribute(name="type")
	private String _parameterType;

	@XmlElement(name="MatchletParameterDescription")
	private String _parameterDescription;

	@XmlAttribute(name="isMandatory")
	private boolean _parameterIsMandatory;

	@XmlElement(name="Constraint")
	private MatchletParameterConstraint _constraint;

	/**
	 * @return the 'parameterName' value
	 */
	public String getParameterName() {
		return this._parameterName;
	}

	/**
	 * @param parameterName the 'parameterName' value to set
	 */
	public void setParameterName(String parameterName) {
		this._parameterName = parameterName;
	}

	/**
	 * @return the 'parameterType' value
	 */
	public String getParameterType() {
		return this._parameterType;
	}

	/**
	 * @param parameterType the 'parameterType' value to set
	 */
	public void setParameterType(String parameterType) {
		this._parameterType = parameterType;
	}

	/**
	 * @return the 'parameterDescription' value
	 */
	public String getParameterDescription() {
		return this._parameterDescription;
	}

	/**
	 * @param parameterDescription the 'parameterDescription' value to set
	 */
	public void setParameterDescription(String parameterDescription) {
		this._parameterDescription = parameterDescription;
	}

	/**
	 * @return the 'parameterIsMandatory' value
	 */
	public boolean getParameterIsMandatory() {
		return this._parameterIsMandatory;
	}

	/**
	 * @param parameterIsMandatory the 'parameterIsMandatory' value to set
	 */
	public void setParameterIsMandatory(boolean parameterIsMandatory) {
		this._parameterIsMandatory = parameterIsMandatory;
	}

	/**
	 * @return the 'constraint' value
	 */
	public MatchletParameterConstraint getConstraint() {
		return this._constraint;
	}

	/**
	 * @param constraint the 'constraint' value to set
	 */
	public void setConstraint(MatchletParameterConstraint constraint) {
		this._constraint = constraint;
	}
}