/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.score;

import java.io.Serializable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 29, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 29, 2015
 */
public interface Score<SELF extends Score<SELF>> extends Serializable, Comparable<SELF> {
	double toDouble();
	SELF multiply(SELF another);
}
