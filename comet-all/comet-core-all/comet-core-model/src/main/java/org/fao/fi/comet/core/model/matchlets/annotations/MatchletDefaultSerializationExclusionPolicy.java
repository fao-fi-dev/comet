/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;

/**
 * Marker annotation for matchlets whose result doesn't need to appear into
 * the matching result serialized XML.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Aug 2010
 */
@MatchletBehaviour

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MatchletDefaultSerializationExclusionPolicy {
	MatchingSerializationExclusionPolicy[] value() default MatchingSerializationExclusionPolicy.ALWAYS;
}
