/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="MatchingEngineProcessStatus")
public class MatchingEngineProcessStatus implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4364229302255667713L;

	final static private Integer UNKNOWN_NUMBER_OF_COMPARISON_ROUNDS 			= -1;
	final static private Integer UNKNOWN_NUMBER_OF_ATOMIC_COMPARISONS_PER_ROUND = -1;

	@XmlAttribute(name="processId")
	private String _processId;

	@XmlElement(name="CurrentNumberOfAtomicComparisonsPerformedInRound")
	private int _currentNumberOfAtomicComparisonsPerformedInRound;

	@XmlElement(name="CurrentNumberOfComparisonRoundsPerformed")
	private int _currentNumberOfComparisonRoundsPerformed;

	@XmlElement(name="CurrentNumberOfMatchletsApplied")
	private int _currentNumberOfMatchletsApplied;

	@XmlElement(name="MaximumNumberOfAtomicComparisonsPerformedInRound")
	private int _maximumNumberOfAtomicComparisonsPerformedInRound;

	@XmlElement(name="MaximumNumberOfMatchletsApplied")
	private int _maximumNumberOfMatchletsApplied;

	@XmlElement(name="NumberOfAuthoritativeFullMatches")
	private int _numberOfAuthoritativeFullMatches;

	@XmlElement(name="NumberOfAuthoritativeNoMatches")
	private int _numberOfAuthoritativeNoMatches;

	@XmlElement(name="NumberOfComparisonRounds")
	private int _numberOfComparisonRounds;

	@XmlElement(name="NumberOfMatches")
	private int _numberOfMatches;

	@XmlElement(name="ProcessEndTimestamp")
	private long _processEndTimestamp;

	@XmlElement(name="ProcessStartTimestamp")
	private long _processStartTimestamp;

	@XmlElement(name="TotalNumberOfAtomicComparisonsPerformed")
	private int _totalNumberOfAtomicComparisonsPerformed;

	@XmlElement(name="NumberOfComparisonErrors")
	private int _numberOfComparisonErrors;

	@XmlElement(name="NumberOfSkippedAtomicComparisons")
	private int _numberOfSkippedAtomicComparisons;

	@XmlElement(name="Status")
	private MatchingEngineStatus _status;
	
	/**
	 * Class constructor
	 *
	 */
	public MatchingEngineProcessStatus() {
		super();

		this._processStartTimestamp = this._processEndTimestamp = -1;
		this._numberOfComparisonRounds = UNKNOWN_NUMBER_OF_COMPARISON_ROUNDS;
		this._maximumNumberOfAtomicComparisonsPerformedInRound = UNKNOWN_NUMBER_OF_ATOMIC_COMPARISONS_PER_ROUND;
	}
	
	public MatchingEngineStatus getStatus() {
		return this._status;
	}

	public void setStatus(MatchingEngineStatus status) {
		this._status = status;
	}

	public boolean is(MatchingEngineStatus... status) {
		if(status == null || status.length == 0)
			return false;
		
		for(MatchingEngineStatus in : status) {
			if(in != null && in.equals(this._status))
				return true;
		}

		return false;
	}

	/**
	 * @return the 'currentNumberOfAtomicComparisonsPerformedInRound' value
	 */
	public int getCurrentNumberOfAtomicComparisonsPerformedInRound() {
		return this._currentNumberOfAtomicComparisonsPerformedInRound;
	}

	/**
	 * @param currentNumberOfAtomicComparisonsPerformedInRound the 'currentNumberOfAtomicComparisonsPerformedInRound' value to set
	 */
	public synchronized void setCurrentNumberOfAtomicComparisonsPerformedInRound(int currentNumberOfAtomicComparisonsPerformedInRound) {
		this._currentNumberOfAtomicComparisonsPerformedInRound = currentNumberOfAtomicComparisonsPerformedInRound;
	}

	public synchronized int increaseCurrentNumberOfAtomicComparisonsPerformedInRound() {
		return ++this._currentNumberOfAtomicComparisonsPerformedInRound;
	}

	/**
	 * @return the 'currentNumberOfComparisonRoundsPerformed' value
	 */
	public int getCurrentNumberOfComparisonRoundsPerformed() {
		return this._currentNumberOfComparisonRoundsPerformed;
	}

	/**
	 * @param currentNumberOfComparisonRoundsPerformed the 'currentNumberOfComparisonRoundsPerformed' value to set
	 */
	public synchronized void setCurrentNumberOfComparisonRoundsPerformed(int currentNumberOfComparisonRoundsPerformed) {
		this._currentNumberOfComparisonRoundsPerformed = currentNumberOfComparisonRoundsPerformed;
	}

	public synchronized int increaseCurrentNumberOfComparisonRoundsPerformed() {
		return ++this._currentNumberOfComparisonRoundsPerformed;
	}

//	/**
//	 * @return the 'currentNumberOfMatchletsApplied' value
//	 */
//	public int getCurrentNumberOfMatchletsApplied() {
//		return this._currentNumberOfMatchletsApplied;
//	}
//
//	/**
//	 * @param currentNumberOfMatchletsApplied the 'currentNumberOfMatchletsApplied' value to set
//	 */
//	public synchronized void setCurrentNumberOfMatchletsApplied(int currentNumberOfMatchletsApplied) {
//		this._currentNumberOfMatchletsApplied = currentNumberOfMatchletsApplied;
//	}
//
//	public synchronized int increaseCurrentNumberOfMatchletsApplied() {
//		return ++this._currentNumberOfMatchletsApplied;
//	}

	/**
	 * @return the 'maximumNumberOfAtomicComparisonsPerformedInRound' value
	 */
	public int getMaximumNumberOfAtomicComparisonsPerformedInRound() {
		return this._maximumNumberOfAtomicComparisonsPerformedInRound;
	}

	/**
	 * @param maximumNumberOfAtomicComparisonsPerformedInRound the 'maximumNumberOfAtomicComparisonsPerformedInRound' value to set
	 */
	public synchronized void setMaximumNumberOfAtomicComparisonsPerformedInRound(int maximumNumberOfAtomicComparisonsPerformedInRound) {
		this._maximumNumberOfAtomicComparisonsPerformedInRound = maximumNumberOfAtomicComparisonsPerformedInRound;
	}

	public synchronized int increaseMaximumNumberOfAtomicComparisonsPerformedInRound() {
		return ++this._maximumNumberOfAtomicComparisonsPerformedInRound;
	}

	/**
	 * @return the 'maximumNumberOfMatchletsApplied' value
	 */
	public int getMaximumNumberOfMatchletsApplied() {
		return this._maximumNumberOfMatchletsApplied;
	}

	/**
	 * @param maximumNumberOfMatchletsApplied the 'maximumNumberOfMatchletsApplied' value to set
	 */
	public synchronized void setMaximumNumberOfMatchletsApplied(int maximumNumberOfMatchletsApplied) {
		this._maximumNumberOfMatchletsApplied = maximumNumberOfMatchletsApplied;
	}

//	public int increaseMaximumNumberOfMatchletsApplied() {
//		return ++this._maximumNumberOfMatchletsApplied;
//	}

	/**
	 * @return the 'numberOfAuthoritativeFullMatches' value
	 */
	public int getNumberOfAuthoritativeFullMatches() {
		return this._numberOfAuthoritativeFullMatches;
	}

	/**
	 * @param numberOfAuthoritativeFullMatches the 'numberOfAuthoritativeFullMatches' value to set
	 */
	public synchronized void setNumberOfAuthoritativeFullMatches(int numberOfAuthoritativeFullMatches) {
		this._numberOfAuthoritativeFullMatches = numberOfAuthoritativeFullMatches;
	}

	public synchronized int increaseNumberOfAuthoritativeFullMatches() {
		return ++this._numberOfAuthoritativeFullMatches;
	}

	/**
	 * @return the 'numberOfAuthoritativeNoMatches' value
	 */
	public int getNumberOfAuthoritativeNoMatches() {
		return this._numberOfAuthoritativeNoMatches;
	}

	/**
	 * @param numberOfAuthoritativeNoMatches the 'numberOfAuthoritativeNoMatches' value to set
	 */
	public synchronized void setNumberOfAuthoritativeNoMatches(int numberOfAuthoritativeNoMatches) {
		this._numberOfAuthoritativeNoMatches = numberOfAuthoritativeNoMatches;
	}

	public synchronized int increaseNumberOfAuthoritativeNoMatches() {
		return ++this._numberOfAuthoritativeNoMatches;
	}

	/**
	 * @return the 'numberOfComparisonRounds' value
	 */
	public int getNumberOfComparisonRounds() {
		return this._numberOfComparisonRounds;
	}

	/**
	 * @param numberOfComparisonRounds the 'numberOfComparisonRounds' value to set
	 */
	public synchronized void setNumberOfComparisonRounds(int numberOfComparisonRounds) {
		this._numberOfComparisonRounds = numberOfComparisonRounds;
	}

	public synchronized int increaseNumberOfComparisonRounds() {
		return ++this._numberOfComparisonRounds;
	}

	/**
	 * @return the 'numberOfMatches' value
	 */
	public int getNumberOfMatches() {
		return this._numberOfMatches;
	}

	/**
	 * @param numberOfMatches the 'numberOfMatches' value to set
	 */
	public synchronized void setNumberOfMatches(int numberOfMatches) {
		this._numberOfMatches = numberOfMatches;
	}

	public synchronized int increaseNumberOfMatches() {
		return ++this._numberOfMatches;
	}

	/**
	 * @return the 'processEndTimestamp' value
	 */
	public long getProcessEndTimestamp() {
		return this._processEndTimestamp;
	}

	/**
	 * @param processEndTimestamp the 'processEndTimestamp' value to set
	 */
	public void setProcessEndTimestamp(long processEndTimestamp) {
		this._processEndTimestamp = processEndTimestamp;
	}

	/**
	 * @return the 'processId' value
	 */
	public String getProcessId() {
		return this._processId;
	}

	/**
	 * @param processId the 'processId' value to set
	 */
	public void setProcessId(String processId) {
		this._processId = processId;
	}

	/**
	 * @return the 'processStartTimestamp' value
	 */
	public long getProcessStartTimestamp() {
		return this._processStartTimestamp;
	}

	/**
	 * @param processStartTimestamp the 'processStartTimestamp' value to set
	 */
	public void setProcessStartTimestamp(long processStartTimestamp) {
		this._processStartTimestamp = processStartTimestamp;
	}

	/**
	 * @return the 'totalNumberOfAtomicComparisonsPerformed' value
	 */
	public int getTotalNumberOfAtomicComparisonsPerformed() {
		return this._totalNumberOfAtomicComparisonsPerformed;
	}

	/**
	 * @param totalNumberOfAtomicComparisonsPerformed the 'totalNumberOfAtomicComparisonsPerformed' value to set
	 */
	public void setTotalNumberOfAtomicComparisonsPerformed(int totalNumberOfAtomicComparisonsPerformed) {
		this._totalNumberOfAtomicComparisonsPerformed = totalNumberOfAtomicComparisonsPerformed;
	}

	public synchronized int increaseTotalNumberOfAtomicComparisonsPerformed() {
		return ++this._totalNumberOfAtomicComparisonsPerformed;
	}

	public synchronized int increaseNumberOfAtomicComparisonsSkipped() {
		return ++this._numberOfSkippedAtomicComparisons;
	}

	public int getNumberOfAtomicComparisonsSkipped() {
		return this._numberOfSkippedAtomicComparisons;
	}

	/**
	 * @return the 'numberOfComparisonErrors' value
	 */
	public int getNumberOfComparisonErrors() {
		return this._numberOfComparisonErrors;
	}

	public synchronized int increaseNumberOfComparisonErrors() {
		return ++this._numberOfComparisonErrors;
	}

	/**
	 * @param numberOfComparisonErrors the 'numberOfComparisonErrors' value to set
	 */
	public synchronized void setNumberOfComparisonErrors(int numberOfComparisonErrors) {
		this._numberOfComparisonErrors = numberOfComparisonErrors;
	}

	@XmlElement(name="Elapsed")
	public long getElapsed() {
		return this._processStartTimestamp == -1
			   ? 0
			   : this._processEndTimestamp == -1
			     ? System.currentTimeMillis() - this._processStartTimestamp
			     : this._processEndTimestamp - this._processStartTimestamp;
	}

	public void setElapsed(long elapsed) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}

	@XmlElement(name="AtomicThroughput")
	public double getAtomicThroughput() {
		long elapsed = this.getElapsed();

		if(elapsed == 0)
			return 0D;

		return this._totalNumberOfAtomicComparisonsPerformed * 1000.0 / elapsed;
	}

	public void setAtomicThroughput(double atomicThroughput) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}

	@XmlElement(name="MeanInputProcessAtomicThroughput")
	public double getMeanInputProcessAtomicThroughput() {
		long elapsed = this.getElapsed();

		if(elapsed == 0 || this._numberOfComparisonRounds == 0)
			return 0D;

		return this._numberOfComparisonRounds < 0 ? Double.NaN : ( this._totalNumberOfAtomicComparisonsPerformed * 1.0 / this._numberOfComparisonRounds ) * 1000.0 / elapsed;
	}

	public void setMeanInputProcessAtomicThroughput(double meanInputProcessAtomicThroughput) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}

	@XmlElement(name="MeanInputProcessThroughput")
	public double getMeanInputProcessThroughput() {
		long elapsed = this.getElapsed();

		if(elapsed == 0)
			return 0D;

		return this._numberOfComparisonRounds < 0 ? Double.NaN : this._numberOfComparisonRounds * 1000.0 / elapsed;
	}

	public void setMeanInputProcessThroughput(double meanInputProcessThroughput) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}
}