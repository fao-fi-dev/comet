/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.adapters.support.MatchingDetailsCollection;

/**
 * A simple matching result adapter that drives the marshalling of a matching details map.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class MatchingDetailsMapAdapter extends XmlAdapter<MatchingDetailsCollection, Map<String, MatchingDetails<?, ?>>> {
	@Override
	public Map<String, MatchingDetails<?, ?>> unmarshal(MatchingDetailsCollection values) {
		Map<String, MatchingDetails<?, ?>> asMap = new HashMap<String, MatchingDetails<?, ?>>();

		for(MatchingDetails<?, ?> value : values.getMatchingDetails())
			asMap.put(new DataIdentifier(value.getSourceProviderId(), value.getSourceId()).toUID(), value);

		return asMap;
	}

	@Override
	public MatchingDetailsCollection marshal(Map<String, MatchingDetails<?, ?>> boundType) {
		if(boundType == null)
			return null;

		return new MatchingDetailsCollection(boundType.values());
	}
}
