/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_uOp;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$eq;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.fao.fi.comet.core.model.engine.adapters.MatchingResultAdapter;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

/**
 * This entity models a generic matching result.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
@XmlType(name="Matching")
@XmlAccessorType(XmlAccessType.FIELD)
public class Matching<SOURCE, TARGET> implements Serializable {
	private static final long serialVersionUID = 8023951008448179546L;

	@XmlAttribute(name="processID")
	private String _processID;

	@XmlTransient
	private DataIdentifier _sourceIdentifier;

	@XmlTransient
	private DataIdentifier _targetIdentifier;

	@XmlElement(name="Target")
	private TARGET _target;

	@XmlTransient
	private MatchingScore _score;

	@XmlElementWrapper(name="MatchingResults")
	@XmlElement(name="MatchingResult")
	@XmlJavaTypeAdapter(MatchingResultAdapter.class)
	private Collection<MatchingResult<?, ?>> _matchingResults;

	/**
	 * Class constructor
	 *
	 * Required by JAXB
	 */
	public Matching() {
		super();

		this._matchingResults = Collections.synchronizedCollection(new ArrayList<MatchingResult<?, ?>>());
	}

	/**
	 * Class constructor
	 *
	 * @param sourceID
	 * @param targetID
	 * @param score
	 * @param matchingResults
	 */
	public Matching(String processID,
					DataIdentifier sourceIdentifier,
					TARGET target,
					DataIdentifier targetIdentifier,
					MatchingScore score,
					Collection<MatchingResult<?, ?>> matchingResults) {
		super();

		this._processID = processID;

		this._sourceIdentifier = sourceIdentifier;
		this._targetIdentifier = targetIdentifier;

		this._target = target;
		this._score = score;

		this._matchingResults = Collections.synchronizedCollection(matchingResults);
	}

	/**
	 * @return the 'processID' value
	 */
	public String getProcessID() {
		return this._processID;
	}

	/**
	 * @param processID the 'processID' value to set
	 */
	public void setProcessID(String processID) {
		this._processID = processID;
	}

	@XmlAttribute(name="score")
	public double getScoreValue() {
		return this._score.getValue();
	}

	/**
	 * @param scoreValue
	 */
	public void setScoreValue(double scoreValue) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setValue(scoreValue);
	}

	@XmlAttribute(name="type")
	public MatchingType getScoreType() {
		return this._score.getMatchingType();
	}

	public void setScoreType(MatchingType scoreType) {
		if(this._score == null)
			this._score = new MatchingScore();

		this._score.setMatchingType(scoreType);
	}

	/**
	 * @return the 'sourceIdentifier' value
	 */
	public DataIdentifier getSourceIdentifier() {
		return this._sourceIdentifier;
	}

	/**
	 * @param sourceIdentifier the 'sourceIdentifier' value to set
	 */
	public void setSourceIdentifier(DataIdentifier sourceIdentifier) {
		this._sourceIdentifier = sourceIdentifier;
	}

	@XmlAttribute(name="sourceId")
	public String getSourceId() {
		return this._sourceIdentifier.getId();
	}

	public void setSourceId(String sourceId) {
		if(this._sourceIdentifier == null)
			this._sourceIdentifier = new DataIdentifier();

		this._sourceIdentifier.setId(sourceId);
	}

	@XmlAttribute(name="sourceProviderId")
	public String getSourceProviderId() {
		return this._sourceIdentifier.getProviderId();
	}

	public void setSourceProviderId(String sourceProviderId) {
		if(this._sourceIdentifier == null)
			this._sourceIdentifier = new DataIdentifier();

		this._sourceIdentifier.setProviderId(sourceProviderId);
	}

	/**
	 * @return the 'targetIdentifier' value
	 */
	public DataIdentifier getTargetIdentifier() {
		return this._targetIdentifier;
	}

	/**
	 * @param targetIdentifier the 'targetIdentifier' value to set
	 */
	public void setTargetIdentifier(DataIdentifier targetIdentifier) {
		this._targetIdentifier = targetIdentifier;
	}

	@XmlAttribute(name="targetId")
	public String getTargetId() {
		return this._targetIdentifier.getId();
	}

	public void setTargetId(String targetId) {
		if(this._targetIdentifier == null)
			this._targetIdentifier = new DataIdentifier();

		this._targetIdentifier.setId(targetId);
	}

	@XmlAttribute(name="targetProviderId")
	public String getTargetProviderId() {
		return this._targetIdentifier.getProviderId();
	}

	public void setTargetProviderId(String targetProviderId) {
		if(this._targetIdentifier == null)
			this._targetIdentifier = new DataIdentifier();

		this._targetIdentifier.setProviderId(targetProviderId);
	}

	/**
	 * @return the 'target' value
	 */
	public Object getTarget() {
		return this._target;
	}

	/**
	 * @param target the 'target' value to set
	 */
	public void setTarget(TARGET target) {
		this._target = target;
	}

	/**
	 * @return the 'score' value
	 */
	@XmlTransient
	public MatchingScore getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public void setScore(MatchingScore score) {
		$nN(score, "The score cannot be null");

		this._score = score;
	}

	/**
	 * @return true if the score value means NO MATCH
	 */
	public boolean isNoMatch() {
		return this._score.isNoMatch();
	}

	/**
	 * @return true if the score value means FULL MATCH
	 */
	public boolean isFullMatch() {
		return this._score.isFullMatch();
	}

	/**
	 * @return true if the matching type is authoritative
	 */
	public boolean isAuthoritative() {
		return this._score.isAuthoritative();
	}

	/**
	 * @return true if the matching type is non authoritative
	 */
	public boolean isNonAuthoritative() {
		return this._score.isNonAuthoritative();
	}

	/**
	 * @return true if the matching type means "non performed"
	 */
	public boolean isNonPerformed() {
		return this._score.isNonPerformed();
	}

	/**
	 * @return the 'matchingResults' value
	 */
	public Collection<MatchingResult<?, ?>> getMatchingResults() {
		return this._matchingResults;
	}

	/**
	 * Adds a matching result to the matching, updating the matching result set *and* score (if
	 * the provided matching result is authoritative) or the score only (if the provided matching result
	 * is not authoritative)
	 *
	 * @param toAdd a matching result to add
	 */
	public void updateMatchingResult(MatchingResult<?, ?> toAdd) {
		$nN(toAdd, "Matching result cannot be null");

		MatchingScore providedScore = toAdd.getScore();

		if(!toAdd.isNonPerformed())
			this._score.setMatchingType(providedScore.getMatchingType());

		if(toAdd.isAuthoritative()) {
			//Previous results should be cleared, as this is an authoritative result
			this._matchingResults.clear();
			this._score.setValue(toAdd.getMatchletScoreValue());
		}

		this._matchingResults.add(toAdd);
	}

	public Matching<SOURCE, TARGET> join(Matching<SOURCE, TARGET> source) {
		$nN(source, "Source matching cannot be null");

		$_uOp(null != this.getSourceIdentifier(), "Cannot join a matching with null source ID");
		$_uOp( null != this.getTargetIdentifier(), "Cannot join a matching with null target ID");

		$_uOp(null != source.getSourceIdentifier(), "Cannot join a matching with null source ID");
		$_uOp(null != source.getTargetIdentifier(), "Cannot join a matching with null target ID");

		$_uOp(this.getSourceIdentifier().equals(source.getSourceIdentifier()), "Cannot join matchings with different source IDs");

		try {
			$_uOp(this.getTargetIdentifier().equals(source.getTargetIdentifier()), "Cannot join matchings with different target IDs");
		} catch (Throwable t) {
			System.out.println("This should not happen...");
		}

		if(source.isAuthoritative())
			return SerializationHelper.clone(source);

		if(this.isAuthoritative() || source.getScore().isNonPerformed())
			return SerializationHelper.clone(this);

		if(Double.compare(source.getScore().getValue(), this.getScore().getValue()) > 0)
			return SerializationHelper.clone(source);

		return SerializationHelper.clone(this);
	}

	public Matching<SOURCE, TARGET> merge(Matching<SOURCE, TARGET> another) {
		$eq(this._sourceIdentifier, another._sourceIdentifier, "Source IDs must be equal between current and joined matching");
		$eq(this._targetIdentifier, another._targetIdentifier, "Target IDs must be equal between current and joined matching");

		Matching<SOURCE, TARGET> joined = new Matching<SOURCE, TARGET>();
		joined.setSourceIdentifier(this._sourceIdentifier);
		joined.setTargetIdentifier(another._targetIdentifier);

		joined.setTarget(another._target);

		joined.setScore(this._score.multiply(another._score));
		joined._matchingResults = new ArrayList<MatchingResult<?, ?>>(this._matchingResults);

		for(MatchingResult<?, ?> otherResult : another._matchingResults)
			joined._matchingResults.add(otherResult);

		return joined;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[ WTS: " + this._score + ", " +
				 "SPID: " + this._sourceIdentifier.getProviderId() + ", " +
				 "TPID: " + this._targetIdentifier.getProviderId() + ", " +
				 "SID: " + this._sourceIdentifier.getId() + ", " +
				 "TID: " + this._targetIdentifier.getId() + ", " +
				 "MRS: " + this._matchingResults + " ]";
	}

	@Override
	public int hashCode() {
		$nN(this._targetIdentifier, "Cannot compute hashcode of matching with NULL targetID");

		return this._targetIdentifier.hashCode();
	}
}