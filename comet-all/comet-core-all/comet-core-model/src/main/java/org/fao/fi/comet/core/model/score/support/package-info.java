@XmlSchema(namespace = "http://support.model.core.comet.fi.fao.org", elementFormDefault = XmlNsForm.UNQUALIFIED)
package org.fao.fi.comet.core.model.score.support;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
