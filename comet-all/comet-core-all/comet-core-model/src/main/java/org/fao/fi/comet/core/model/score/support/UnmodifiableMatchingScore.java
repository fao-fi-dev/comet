/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.score.support;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_fail;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_failRet;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$eq;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$lte;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
@XmlType(name="UnmodifiableMatchingScore")
final public class UnmodifiableMatchingScore extends MatchingScore {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6821192099216353523L;

	public UnmodifiableMatchingScore() {
		this._value = MatchingScore.SCORE_NO_MATCH;
		this._matchingType = null;
	}

	public UnmodifiableMatchingScore(MatchingScore wrapped) {
		$nN(wrapped, "Provided score cannot be null");

		$gte(wrapped._value, MatchingScore.SCORE_NO_MATCH, "Score value {} cannot be lower than the minimum allowed ({})", wrapped._value, MatchingScore.SCORE_NO_MATCH);
		$lte(wrapped._value, MatchingScore.SCORE_FULL_MATCH, "Score value {} cannot be higher than the maximum allowed ({})", wrapped._value, MatchingScore.SCORE_FULL_MATCH);
		$nN(wrapped._matchingType, "Provided matching type cannot be null");

		this._value = wrapped._value;
		this._matchingType = wrapped._matchingType;
	}

	/**
	 * Class constructor
	 *
	 * @param value
	 * @param matchingType
	 */
	public UnmodifiableMatchingScore(double value, MatchingType matchingType) {
		$gte(value, MatchingScore.SCORE_NO_MATCH, "Score value {} cannot be lower than the minimum allowed ({})", value, MatchingScore.SCORE_NO_MATCH);
		$lte(value, MatchingScore.SCORE_FULL_MATCH, "Score value {} cannot be higher than the maximum allowed ({})", value, MatchingScore.SCORE_FULL_MATCH);
		$nN(matchingType, "Provided matching type cannot be null");

		this._value = value;
		this._matchingType = matchingType;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.support.MatchingScore#setValue(double)
	 */
	@Override
	public void setValue(double value) {
		boolean currentlyNotSet = Double.compare(MatchingScore.SCORE_NO_MATCH, this._value) == 0;

		if(!currentlyNotSet)
			$eq(value, this._value, "You cannot change this' matching score value");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.support.MatchingScore#setMatchingType(org.fao.fi.comet.core.support.MatchingType)
	 */
	@Override
	public void setMatchingType(MatchingType matchingType) {
		if(this._matchingType != null)
			$eq(matchingType, this._matchingType, "You cannot change this' matching type");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.support.MatchingScore#multiply(org.fao.vrmf.core.tools.topology.behaviours.WeightValue)
	 */
	@Override
	public MatchingScore multiply(MatchingScore another) {
		return $_failRet("You cannot multiply this matching score with another");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.support.MatchingScore#increaseValue(double)
	 */
	@Override
	public void increaseValue(double valueDelta) {
		$_fail("You cannot increase this matching score's value");
	}
}
