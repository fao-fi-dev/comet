/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletConfigurationParameter", propOrder={ "_name", "_value" })
public class MatchletConfigurationParameter implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4738691894088028630L;

	@XmlAttribute(name="name", required=true)
	private String _name;

	@XmlAttribute(name="value", required=true)
	private String _value;
	
	@XmlTransient
	private Object _actualValue;

	@XmlTransient
	private boolean _isTransient;
	
	/**
	 * Class constructor
	 *
	 */
	public MatchletConfigurationParameter() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param value
	 * @param isTransient
	 */
	public MatchletConfigurationParameter(String name, String value) {
		this(name, value, value, false);
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param actualValue
	 * @param value
	 */
	public MatchletConfigurationParameter(String name, String value, Object actualValue) {
		this(name, value, actualValue, false);
	}
	
	/**
	 * Class constructor
	 *
	 * @param name
	 * @param value
	 * @param actualValue
	 * @param isTransient
	 */
	public MatchletConfigurationParameter(String name, String value, Object actualValue, boolean isTransient) {
		this._name = name;
		this._value = value;
		this._actualValue = actualValue;
		this._isTransient = isTransient;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'value' value
	 */
	public String getValue() {
		return this._value;
	}

	/**
	 * @param value the 'value' value to set
	 */
	public void setValue(String value) {
		this._value = value;
	}

	/**
	 * @return the 'actualValue' value
	 */
	public Object getActualValue() {
		return this._actualValue;
	}

	/**
	 * @param actualValue the 'actualValue' value to set
	 */
	public void setActualValue(Object actualValue) {
		this._actualValue = actualValue;
	}

	/**
	 * @return the 'isTransient' value
	 */
	public final boolean isTransient() {
		return this._isTransient;
	}

	/**
	 * @param isTransient the 'isTransient' value to set
	 */
	public final void setTransient(boolean isTransient) {
		this._isTransient = isTransient;
	}
}