/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
public class MatchingProcessException extends Exception {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8167953678474135636L;

	/**
	 * Class constructor
	 *
	 */
	public MatchingProcessException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MatchingProcessException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MatchingProcessException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MatchingProcessException(Throwable cause) {
		super(cause);
	}
}
