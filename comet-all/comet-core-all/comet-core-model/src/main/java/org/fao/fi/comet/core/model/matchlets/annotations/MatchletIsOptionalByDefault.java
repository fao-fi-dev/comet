/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation for matchlets that are 'optional', i.e. that will
 * return a 'NON PERFORMED' result when one of the entities being compared
 * has a non-null value with regard to the details compared by the matchlets.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Aug 2010
 */
@MatchletBehaviour
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MatchletIsOptionalByDefault {
	//Currently left empty
}
