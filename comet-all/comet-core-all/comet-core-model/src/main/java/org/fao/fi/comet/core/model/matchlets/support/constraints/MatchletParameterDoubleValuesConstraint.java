/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleValidValues;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterDoubleValuesConstraint")
public class MatchletParameterDoubleValuesConstraint extends MatchletParameterValuesConstraint<Double> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4997538027522537645L;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.support.MatchletParameterConstraint#initializeFromField(java.lang.reflect.Field)
	 */
	@Override
	public void initializeFromField(Field matchletParameter) {
		super.initializeFromField(matchletParameter);
		
		DoubleValidValues conf = this.canBeConfiguredFrom(matchletParameter, DoubleValidValues.class);
		
		if(conf != null) {
			this.setValues(this.convert(conf.values()));
			
			this.setMultiple(conf.multiple());
		}
	}
	
	private Double[] convert(double[] source) {
		if(source == null)
			return null;
		
		Double[] converted = new Double[source.length];
		
		for(int counter = 0; counter < source.length; counter++)
			converted[counter] = new Double(source[counter]);
		
		return converted;
	}
}