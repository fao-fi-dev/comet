/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.fao.fi.comet.core.model.engine.adapters.MatchingsMapAdapter;
import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;

/**
 * (c) 2010 FAO / UN (project: comparison-engine)
 */

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Aug 2010
 */
@XmlType(name="MatchingDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public final class MatchingDetails<SOURCE, TARGET> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3645628461543595729L;

	@XmlTransient
	private DataIdentifier _sourceIdentifier;

	@XmlElement(name="Source")
	private SOURCE _source;

	@XmlJavaTypeAdapter(MatchingsMapAdapter.class)
	@XmlElement(name="Matchings")
	protected Map<String, Matching<SOURCE, TARGET>> _matchingsMap;

	/**
	 * Class constructor
	 *
	 */
	public MatchingDetails() {
		this(1000, .1f);
	}

	/**
	 * Class constructor
	 *
	 * @param initialCapacity
	 * @param loadFactor
	 */
	public MatchingDetails(int initialCapacity, float loadFactor) {
		$pos(initialCapacity, "Initial capacity must be greater than zero (currently: {})", initialCapacity);
		$pos(loadFactor, "Load factor must be greater than zero (currently: {})", loadFactor);

		this._matchingsMap = Collections.synchronizedMap(new HashMap<String, Matching<SOURCE, TARGET>>());
	}

	/**
	 * Class constructor
	 *
	 * @param backingMap
	 */
	public MatchingDetails(Map<String, Matching<SOURCE, TARGET>> backingMap) {
		$nN(backingMap, "Backing map cannot be null");

		this._matchingsMap = Collections.synchronizedMap(backingMap);
	}

	@XmlTransient
	public Collection<Matching<SOURCE, TARGET>> getMatchings() {
		return this._matchingsMap.values();
	}

	@XmlTransient
	public Collection<Matching<SOURCE, TARGET>> getSortedUniqueMatchings() {
		Set<Matching<SOURCE, TARGET>> asList = new HashSet<Matching<SOURCE, TARGET>>();

		if(this._matchingsMap.values() != null)
			asList.addAll(this._matchingsMap.values());

		List<Matching<SOURCE, TARGET>> sorted = new ArrayList<Matching<SOURCE, TARGET>>(asList);

		Collections.sort(sorted, new Comparator<Matching<SOURCE, TARGET>>() {
			@Override
			public int compare(Matching<SOURCE, TARGET> first, Matching<SOURCE, TARGET> second) {
				return second.getScore().compareTo(first.getScore());
			}
		});

		return sorted;
	}

	public MatchingDetails<SOURCE, TARGET> retain(int maxNumCandidates, Comparator<Matching<SOURCE, TARGET>> comparator) {
		List<Matching<SOURCE, TARGET>> entries = new ArrayList<Matching<SOURCE, TARGET>>(this.getMatchings());

		if(entries.size() > maxNumCandidates) {
			if(comparator != null)
				Collections.sort(entries, comparator);

			this._matchingsMap.clear();

			Matching<SOURCE, TARGET> entry;
			for(int r=0; r<maxNumCandidates; r++) {
				entry = entries.get(r);

				this._matchingsMap.put(entry.getTargetIdentifier().toUID(), entry);
			}
		}

		return this;
	}

	@XmlAttribute(name="totalCandidates")
	public int getTotalCandidates() {
		return this._matchingsMap.size();
	}

	public void setTotalCandidates(int totalCandidates) {
		//DO NOTHING. LEFT FOR JAXB UNMARSHALLING REASONS...
	}

	@XmlAttribute(name="sourceId")
	public String getSourceId() {
		return this._sourceIdentifier.getId();
	}

	public void setSourceId(String sourceId) {
		if(this._sourceIdentifier == null)
			this._sourceIdentifier = new DataIdentifier();

		this._sourceIdentifier.setId(sourceId);
	}

	@XmlAttribute(name="sourceProviderId")
	public String getSourceProviderId() {
		return this._sourceIdentifier.getProviderId();
	}

	public void setSourceProviderId(String sourceProviderId) {
		if(this._sourceIdentifier == null)
			this._sourceIdentifier = new DataIdentifier();

		this._sourceIdentifier.setProviderId(sourceProviderId);
	}

	/**
	 * @return the 'sourceIdentifier' value
	 */
	public DataIdentifier getSourceIdentifier() {
		return this._sourceIdentifier;
	}

	/**
	 * @param sourceIdentifier the 'sourceIdentifier' value to set
	 */
	public void setSourceIdentifier(DataIdentifier sourceIdentifier) {
		this._sourceIdentifier = sourceIdentifier;
	}

	/**
	 * @return the 'source' value
	 */
	public Object getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(SOURCE source) {
		this._source = source;
	}

	public Matching<SOURCE, TARGET> findMatchingByTargetId(DataIdentifier targetIdentifier) {
		return this._matchingsMap.get(targetIdentifier.toUID());
	}

	public Matching<SOURCE, TARGET> removeMatchingByTargetId(DataIdentifier targetIdentifier) {
		return this._matchingsMap.remove(targetIdentifier.toUID());
	}

	public Map<String, Matching<SOURCE, TARGET>> asMap() {
		return this._matchingsMap;
	}

	/**
	 * @param another
	 * @return
	 */
	public MatchingDetails<SOURCE, TARGET> join(MatchingDetails<SOURCE, TARGET> another) {
		$nN(another, "Provided matching details cannot be null");
		$nN(another._matchingsMap, "Provided matching details cannot have a null backing data map");

		Matching<SOURCE, TARGET> matching;

		for(String key : another._matchingsMap.keySet()) {
			matching = another._matchingsMap.get(key);

			if(this._matchingsMap.containsKey(key)) {
				this._matchingsMap.get(key).join(matching);
			} else {
				this._matchingsMap.put(key, SerializationHelper.clone(matching));
			}
		}

		return this;
	}
}