/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.LongRangeTo;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterLongRangeConstraint")
public class MatchletParameterLongRangeConstraint extends MatchletParameterRangeConstraint<Long> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1399392558012991825L;
	
	public void initializeFromField(Field matchletParameter) {
		super.initializeFromField(matchletParameter);
		
		LongRange range = this.canBeConfiguredFrom(matchletParameter, LongRange.class);
		
		if(range != null) {
			this.setFrom(range.from());
			this.setTo(range.to());
			this.setIncludeFrom(range.includeFrom());
			this.setIncludeTo(range.includeTo());
			
			return;
		}
		
		LongRangeFrom rangeFrom = this.canBeConfiguredFrom(matchletParameter, LongRangeFrom.class);
		
		if(rangeFrom != null) {
			this.setFrom(rangeFrom.value());
			this.setIncludeFrom(rangeFrom.include());
			
			return;
		}
		
		LongRangeTo rangeTo = this.canBeConfiguredFrom(matchletParameter, LongRangeTo.class);
		
		if(rangeTo != null) {
			this.setTo(rangeTo.value());
			this.setIncludeTo(rangeTo.include());
			
			return;
		}
	}
}