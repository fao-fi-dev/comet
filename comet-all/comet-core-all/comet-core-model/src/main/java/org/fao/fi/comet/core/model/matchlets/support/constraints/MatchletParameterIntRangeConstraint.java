/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support.constraints;

import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRange;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRangeFrom;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.IntRangeTo;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletParameterIntRangeConstraint")
public class MatchletParameterIntRangeConstraint extends MatchletParameterRangeConstraint<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1399392558012991825L;
	
	public void initializeFromField(Field matchletParameter) {
		super.initializeFromField(matchletParameter);
		
		IntRange range = this.canBeConfiguredFrom(matchletParameter, IntRange.class);
		
		if(range != null) {
			this.setFrom(range.from());
			this.setTo(range.to());
			this.setIncludeFrom(range.includeFrom());
			this.setIncludeTo(range.includeTo());
			
			return;
		}
		
		IntRangeFrom rangeFrom = this.canBeConfiguredFrom(matchletParameter, IntRangeFrom.class);
		
		if(rangeFrom != null) {
			this.setFrom(rangeFrom.value());
			this.setIncludeFrom(rangeFrom.include());
			
			return;
		}
		
		IntRangeTo rangeTo = this.canBeConfiguredFrom(matchletParameter, IntRangeTo.class);
		
		if(rangeTo != null) {
			this.setTo(rangeTo.value());
			this.setIncludeTo(rangeTo.include());
			
			return;
		}
	}
}