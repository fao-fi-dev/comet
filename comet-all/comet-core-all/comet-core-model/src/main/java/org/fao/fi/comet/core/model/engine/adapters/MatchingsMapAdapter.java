/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.adapters.support.MatchingsCollection;

/**
 * A simple matching result adapter that drives the marshalling of a matchings map.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class MatchingsMapAdapter extends XmlAdapter<MatchingsCollection, Map<String, Matching<?, ?>>> {
	@Override
	public Map<String, Matching<?, ?>> unmarshal(MatchingsCollection values) {
		Map<String, Matching<?, ?>> asMap = new HashMap<String, Matching<?, ?>>();

		for(Matching<?, ?> value : values.getMatchings())
			asMap.put(new DataIdentifier(value.getTargetProviderId(), value.getTargetId()).toUID(), value);

		return asMap;
	}

	@Override
	public MatchingsCollection marshal(Map<String, Matching<?, ?>> boundType) {
		if(boundType == null)
			return null;

		ArrayList<Matching<?, ?>> sorted = new ArrayList<Matching<?, ?>>();

		if(boundType.values() != null)
			sorted.addAll(boundType.values());

		Collections.sort(sorted, new Comparator<Matching<?, ?>>() {
			@Override
			public int compare(Matching<?, ?> first, Matching<?, ?> second) {
				return second.getScore().compareTo(first.getScore());
			}
		});

		return new MatchingsCollection(sorted);
	}
}
