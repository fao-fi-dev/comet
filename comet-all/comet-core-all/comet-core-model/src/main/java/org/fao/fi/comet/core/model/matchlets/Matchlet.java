/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;

/**
 * The basic matchlet interface.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface Matchlet<SOURCE,
						  SOURCE_DATA,
						  TARGET,
						  TARGET_DATA> extends Serializable, Comparable<Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>> {
	String OPTIONAL_PARAM 		  = "isOptional";
	String CUTOFF_PARAM 		  = "isCutoff";
	String WEIGHT_PARAM 		  = "weight";
	String FORCE_COMPARISON_PARAM = "forceComparison";
	String MIN_SCORE_PARAM 		  = "minimumScore";

	String getId();
	void setId(String id);

	String getName();
	String getType();

	String getDescription();

	void configure(Collection<MatchletConfigurationParameter> parameters) throws MatchletConfigurationException;
	void validateConfiguration() throws MatchletConfigurationException;
	Collection<MatchletConfigurationParameter> getConfiguration() throws MatchletConfigurationException;

	/**
	 * @param exclusionCases
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setExclusionCases(MatchingSerializationExclusionPolicy... exclusionCases);

	MatchingSerializationExclusionPolicy[] getExclusionCases();

	/**
	 * @return the 'force-comparison' mode for the matchlet.
	 */
	boolean forceComparison();

	/**
	 * Sets the 'force-comparison' mode for the matchlet.
	 *
	 * If the matchlet is in 'force-comparison' mode, it will perform a comparison even if one of the data being matched is null.
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setForceComparison(boolean forceComparison);

	/**
	 * @return true if the matchlet is optional, that is will return a 'NON-PERFORMED' whenever one of the two data being
	 * compared is null
	 */
	boolean isOptional();

	/**
	 * @param the 'optional' status for the matchlet.
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setOptional(boolean optional);

	/**
	 * @return the 'cutoff' status for the matchlet.
	 */
	boolean isCutoff();

	/**
	 * Sets the 'cut-off' status for the matchlet.
	 *
	 * If the matchlet is of 'cut-off' type, it will return an {@link MatchingType#AUTHORITATIVE} {@link Matchlet#SCORE_NO_MATCH} whenever the two
	 * data being compared are *STRONGLY* non compatible
	 *
	 * @param isCutoff the 'cut-off' status for the matchlet
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setCutoff(boolean isCutoff);

	/**
	 * @param weight the matchlet's weight
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setWeight(double weight);

	/**
	 * @return the matchlet's weight
	 */
	double getWeight();

	/**
	 * Sets the minimum result score for a valid result to be returned by the matchlet.
	 * @param minimumScore the minimum score
	 * @return
	 */
	Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> setMinimumAllowedScore(double minimumScore);

	/**
	 * @return the minimum result score set for the matchlet.
	 */
	Double getMinimumScore();

	/**
	 * @param source the source entity
	 * @param sourceIdentifier the data identifier for the source entity
	 * @param sourceData the data extracted from the source entity
	 * @param target the target entity
	 * @param targetIdentifier the data identifier for the target entity
	 * @param targetData the data extracted from the target entity
	 *
	 * @return a score identifying the probability of the source entity data of being
	 * 'equal' to the target entity data
	 */
	MatchingScore computeScore(SOURCE source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData,
							   TARGET target, DataIdentifier targetIdentifier, TARGET_DATA targetData);

	/**
	 * @param score a score instance (whose lambda is supposed to range
	 * between {@link Matchlet#SCORE_NO_MATCH} and {@link Matchlet#SCORE_FULL_MATCH})
	 *
	 * @return true if the score lambda is not lower than the minimum
	 * score currently set for the matchlet
	 */
	boolean isScoreAllowed(MatchingScore score);

	/**
	 * Performs a matching between two entities and returns the matching result
	 *
	 * @param sourceEntity the entity to compare to the target
	 * @param sourceIdentifier the data identifier for the source entity
	 * @param targetEntity the target entity
	 * @param targetIdentifier the data identifier for the target entity
	 *
	 * @return the matching result modeling a (possible) matching relationship from the source entity towards the target entity
	 */
	MatchingResult<SOURCE_DATA, TARGET_DATA> performMatching(SOURCE sourceEntity, DataIdentifier sourceIdentifier,
															 TARGET targetEntity, DataIdentifier targetIdentifier);

	MatchingResult<SOURCE_DATA, TARGET_DATA> newMatchingResult();
}