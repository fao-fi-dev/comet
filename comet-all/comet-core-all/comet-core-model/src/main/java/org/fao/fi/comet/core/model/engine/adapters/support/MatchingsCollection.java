/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine.adapters.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.comet.core.model.engine.Matching;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 6 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 6 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingsCollection implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3880196459154012631L;

	@XmlElement(name="Matching")
	private Collection<Matching<?, ?>> _matchings;

	/**
	 * Class constructor
	 */
	public MatchingsCollection() {
		super();

		this._matchings = new ArrayList<Matching<?, ?>>();
	}

	/**
	 * Class constructor
	 *
	 * @param matchings
	 */
	public MatchingsCollection(Collection<Matching<?, ?>> matchings) {
		super();

		this._matchings = matchings;
	}

	/**
	 * @return the 'matchings' value
	 */
	public Collection<Matching<?, ?>> getMatchings() {
		return this._matchings;
	}

	/**
	 * @param matchings the 'matchings' value to set
	 */
	public void setMatchings(Collection<Matching<?, ?>> matchings) {
		this._matchings = matchings;
	}
}