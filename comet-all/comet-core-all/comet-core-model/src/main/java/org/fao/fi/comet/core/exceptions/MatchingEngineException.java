/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jun 2013
 */
public class MatchingEngineException extends MatchingProcessException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6819929225102107366L;

	/**
	 * Class constructor
	 */
	public MatchingEngineException() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MatchingEngineException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MatchingEngineException(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MatchingEngineException(Throwable cause) {
		super(cause);
	}
}
