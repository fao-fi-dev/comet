/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker annotation for matchlets that 'cut-off' results based on the matching result.
 *
 * An example is the LRNO matchlet, that halts the comparison when it compares vessels
 * with two different LRNOs (yields an authoritative no match in such cases).
 *
 * Another example is the mapped vessels matchlet, that halts the comparison when it
 * compares vessels already mapped one onto the other (yields an authoritative match
 * in such cases).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jan 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jan 2011
 */
@MatchletBehaviour

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MatchletIsCutoffByDefault {
	//Currently left empty
}
