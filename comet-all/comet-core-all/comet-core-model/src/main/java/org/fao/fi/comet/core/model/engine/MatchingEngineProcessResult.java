/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 May 2013
 */
@XmlRootElement(name="MatchingEngineProcessResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingEngineProcessResult<SOURCE, TARGET, CONFIG extends MatchingEngineProcessConfiguration> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1369256622240246122L;

	@XmlElementWrapper(name="ProcessorsInfo")
	@XmlElement(name="ProcessorInfo")
	private Collection<MatchingEngineProcessorInfo<CONFIG>> _processorsInfo;

	@XmlElement(name="Results")
	private MatchingsData<SOURCE, TARGET> _results;

	/**
	 * Class constructor
	 *
	 */
	public MatchingEngineProcessResult() {
		super();

		this._processorsInfo = new HashSet<MatchingEngineProcessorInfo<CONFIG>>();
	}

	/**
	 * Class constructor
	 *
	 * @param processID
	 * @param configuration
	 * @param results
	 */
	public MatchingEngineProcessResult(MatchingEngineProcessorInfo<CONFIG> processorInfo,
									   MatchingsData<SOURCE, TARGET> results) {
		this();

		this._processorsInfo.add($nN(processorInfo, "The processor info cannot be NULL"));

		this._results = results;

		if(results != null)
			for(MatchingDetails<SOURCE, TARGET> detail : results.getMatchingDetails()) {
				if(detail != null)
					for(Matching<SOURCE, TARGET> matching : detail.getMatchings())
						matching.setProcessID(processorInfo.getProcessID());
			}
	}

	/**
	 * @return the 'processorsInfo' value
	 */
	public Collection<MatchingEngineProcessorInfo<CONFIG>> getProcessorsInfo() {
		return this._processorsInfo;
	}

	/**
	 * @param processorsInfo the 'processorsInfo' value to set
	 */
	public void setProcessorsInfo(Collection<MatchingEngineProcessorInfo<CONFIG>> processorsInfo) {
		this._processorsInfo = processorsInfo;
	}

	/**
	 * @return the 'results' value
	 */
	public MatchingsData<SOURCE, TARGET> getResults() {
		return this._results;
	}

	/**
	 * @param results the 'results' value to set
	 */
	public void setResults(MatchingsData<SOURCE, TARGET> results) {
		this._results = $nN(results, "Please provide a non-null reference to matchings data");
	}

	/**
	 * @param another
	 * @return
	 */
	public MatchingEngineProcessResult<SOURCE, TARGET, CONFIG> join(MatchingEngineProcessResult<SOURCE, TARGET, CONFIG> another) {
		this._processorsInfo.addAll($nN($nN(another, "Provided matching engine process result cannot be null")._processorsInfo, "Provided matching engine process info cannot be null"));
		this._results = this._results.join(another._results);

		return this;
	}
}