/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-model)
 */
package org.fao.fi.comet.core.model.matchlets.support;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jun 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="MatchletInfo")
public class MatchletInfo implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2650849472982785649L;

	@XmlElement(name="MatchletName")
	private String _matchletName;

	@XmlElement(name="MatchletDescription")
	private String _matchletDescription;

	@XmlAttribute(name="MatchletType")
	private String _matchletType;

	@XmlElement(name="MatchletExtractedSourceDataType")
	private String _extractedSourceDataType;

	@XmlElement(name="MatchletExtractedTargetDataType")
	private String _extractedTargetDataType;

	@XmlElementWrapper(name="MatchletParameters")
	@XmlElement(name="MatchletParameter")
	private Collection<MatchletParameterInfo> _matchletParameters;

	/**
	 * @return the 'matchletName' value
	 */
	public String getMatchletName() {
		return this._matchletName;
	}

	/**
	 * @param matchletName the 'matchletName' value to set
	 */
	public void setMatchletName(String matchletName) {
		this._matchletName = matchletName;
	}

	/**
	 * @return the 'matchletDescription' value
	 */
	public String getMatchletDescription() {
		return this._matchletDescription;
	}

	/**
	 * @param matchletDescription the 'matchletDescription' value to set
	 */
	public void setMatchletDescription(String matchletDescription) {
		this._matchletDescription = matchletDescription;
	}

	/**
	 * @return the 'matchletType' value
	 */
	public String getMatchletType() {
		return this._matchletType;
	}

	/**
	 * @param matchletType the 'matchletType' value to set
	 */
	public void setMatchletType(String matchletType) {
		this._matchletType = matchletType;
	}

	/**
	 * @return the 'extractedSourceDataType' value
	 */
	public String getExtractedSourceDataType() {
		return this._extractedSourceDataType;
	}

	/**
	 * @param extractedSourceDataType the 'extractedSourceDataType' value to set
	 */
	public void setExtractedSourceDataType(String extractedSourceDataType) {
		this._extractedSourceDataType = extractedSourceDataType;
	}

	/**
	 * @return the 'extractedTargetDataType' value
	 */
	public String getExtractedTargetDataType() {
		return this._extractedTargetDataType;
	}

	/**
	 * @param extractedTargetDataType the 'extractedTargetDataType' value to set
	 */
	public void setExtractedTargetDataType(String extractedTargetDataType) {
		this._extractedTargetDataType = extractedTargetDataType;
	}

	/**
	 * @return the 'matchletParameters' value
	 */
	public Collection<MatchletParameterInfo> getMatchletParameters() {
		return this._matchletParameters;
	}

	/**
	 * @param matchletParameters the 'matchletParameters' value to set
	 */
	public void setMatchletParameters(Collection<MatchletParameterInfo> matchletParameters) {
		this._matchletParameters = matchletParameters;
	}
}
