@XmlSchema(namespace = "http://support.adapters.engine.model.core.comet.fi.fao.org", elementFormDefault = XmlNsForm.UNQUALIFIED)
package org.fao.fi.comet.core.model.engine.adapters.support;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
