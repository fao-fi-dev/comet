/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.core.uniform.test.engine;

import java.util.Collection;

import org.fao.fi.comet.core.engine.process.MatchingEngineMetadataResolver;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
public class TestMatchingEngineMetadataResolver {
	private class FakeGenericScalarMatchlet<SOURCE> extends UScalarMatchletSkeleton<SOURCE, String> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public MatchingScore computeScore(SOURCE source, DataIdentifier sourceIdentifier, String sourceData, SOURCE target, DataIdentifier targetIdentifier, String targetData) {
			return null;
		}

		@Override
		protected String doExtractData(SOURCE entity, DataIdentifier dataIdentifier) {
			return null;
		}
	}

	private class FakeScalarMatchlet extends FakeGenericScalarMatchlet<Object> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public String getDescription() {
			return null;
		}
	}

	private class FakeVectorialMatchlet extends UVectorialMatchletSkeleton<Object, String> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4766800601426183557L;

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public MatchingScore computeScore(Object source, DataIdentifier sourceIdentifier, String sourceData, Object target, DataIdentifier targetIdentifier, String targetData) {
			return null;
		}

		@Override
		protected Collection<String> doExtractData(Object entity, DataIdentifier dataIdentifier) {
			return null;
		}
	}

	@Test
	public void testOnGenericScalarMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeGenericScalarMatchlet<Object>());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(info.getExtractedSourceDataType(), info.getExtractedTargetDataType());
	}

	@Test
	public void testOnScalarMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeScalarMatchlet());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(info.getExtractedSourceDataType(), info.getExtractedTargetDataType());
	}

	@Test
	public void testOnVectorialMatchlet() throws Throwable {
		MatchletInfo info = new MatchingEngineMetadataResolver().getMatchletInfo(new FakeVectorialMatchlet());
		
		Assert.assertNotNull(info);
		Assert.assertEquals(String.class.getName(), info.getExtractedSourceDataType());
		Assert.assertEquals(info.getExtractedSourceDataType(), info.getExtractedTargetDataType());
	}
}
