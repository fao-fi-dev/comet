/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.engine;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$_assert;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nEm;
import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.engine.MatchingEngineCore;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.exceptions.MatchingProcessException;
import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessorInfo;
import org.fao.fi.comet.core.model.engine.MatchingEngineStatus;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 May 2013
 */
public class UMatchingEngineCore<ENTITY extends Serializable, CONFIG extends MatchingEngineProcessConfiguration> extends MatchingEngineCore<ENTITY, ENTITY, CONFIG> {
	protected boolean _switchComparisonAndDomain = false;

	/**
	 * Class constructor
	 *
	 */
	public UMatchingEngineCore() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param parallelThreads
	 */
	public UMatchingEngineCore(int parallelThreads) {
		super(parallelThreads); 
	}



	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.AbstractMatchingEngineCore#afterCompletionCallback()
	 */
	@Override
	protected void afterCompletionCallback() {
		//DO NOTHING
	}

	/**
	 * @return the 'switchComparisonAndDomain' value
	 */
	public boolean getSwitchComparisonAndDomain() {
		return this._switchComparisonAndDomain;
	}

	/**
	 * @param switchComparisonAndDomain the 'switchComparisonAndDomain' value to set
	 */
	public void setSwitchComparisonAndDomain(boolean switchComparisonAndDomain) {
		this._switchComparisonAndDomain = switchComparisonAndDomain;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.core.MatchingEngineCore#doPerformActualComparison(java.io.Serializable, java.io.Serializable, org.fao.fi.comet.core.matchlets.Matchlet)
	 */
	@Override
	protected MatchingResult<?, ?> doPerformActualComparison(ENTITY source, DataIdentifier sourceIdentifier, ENTITY target, DataIdentifier targetIdentifier, Matchlet<ENTITY, ?, ENTITY, ?> matchlet) {
		return super.doPerformActualComparison(this._switchComparisonAndDomain ? target : source,
											   this._switchComparisonAndDomain ? targetIdentifier : sourceIdentifier,
											   this._switchComparisonAndDomain ? source : target,
											   this._switchComparisonAndDomain ? sourceIdentifier : targetIdentifier,
											   matchlet);
	}

	@Override
	protected Matching<ENTITY, ENTITY> doGetMatching(MatchingsData<ENTITY, ENTITY> currentMatchingData, DataIdentifier sourceIdentifier, DataIdentifier targetIdentifier, MatchingResult<?, ?> currentMatchingResult) {
		return super.doGetMatching(currentMatchingData,
								   this._switchComparisonAndDomain ? targetIdentifier : sourceIdentifier,
								   this._switchComparisonAndDomain ? sourceIdentifier : targetIdentifier,
								   currentMatchingResult);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.core.MatchingEngineCore#doRemove(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected Matching<ENTITY, ENTITY> doRemoveMatching(MatchingsData<ENTITY, ENTITY> currentMatchingData, DataIdentifier sourceIdentifier, DataIdentifier targetIdentifier) {
		return super.doRemoveMatching(currentMatchingData,
									  this._switchComparisonAndDomain ? targetIdentifier : sourceIdentifier,
									  this._switchComparisonAndDomain ? sourceIdentifier : targetIdentifier);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.core.MatchingEngineCore#doGetMatchingDetails(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingDetails<ENTITY, ENTITY> doGetMatchingDetails(MatchingsData<ENTITY, ENTITY> currentMatchingData, DataIdentifier sourceIdentifier, DataIdentifier targetIdentifier) {
		return super.doGetMatchingDetails(currentMatchingData,
										  this._switchComparisonAndDomain ? targetIdentifier : sourceIdentifier,
										  this._switchComparisonAndDomain ? sourceIdentifier : targetIdentifier);
	}

	/**
	 * @param configuration
	 * @param tracker
	 * @param sources
	 * @param sourcesPartitioner
	 * @param targets
	 * @param targetsPartitioner
	 * @param IDHandler
	 * @return
	 */
	final public MatchingEngineProcessResult<ENTITY, ENTITY, CONFIG> compareAll(CONFIG configuration,
																				MatchingProcessHandler<ENTITY, ENTITY> tracker,
																				DataProvider<ENTITY> sources,
																				DataPartitioner<ENTITY, ENTITY> partitioner,
																				DataProvider<ENTITY> targets,
																				IDHandler<ENTITY, ?> IDHandler) throws MatchingProcessException {

		return this.compareAll(configuration,
							   tracker,
							   sources,
							   partitioner,
							   targets,
							   IDHandler,
							   IDHandler);
	}

	/**
	 * @param configuration
	 * @param tracker
	 * @param sources
	 * @param sourcesPartitioner
	 * @param IDHandler
	 * @return
	 */
	final public MatchingEngineProcessResult<ENTITY, ENTITY, CONFIG> compareAndFeedAll(CONFIG configuration,
																					   MatchingProcessHandler<ENTITY, ENTITY> tracker,
																					   FeedableDataProvider<ENTITY> sources,
																					   DataPartitioner<ENTITY, ENTITY> partitioner,
																					   IDHandler<ENTITY, ?> IDHandler) throws MatchingProcessException {
		return this.compareAndFeedAll(configuration,
									  tracker,
									  sources,
									  partitioner,
									  new CollectionBackedDataProvider<ENTITY>(),
									  IDHandler);
	}

	/**
	 * @param configuration
	 * @param tracker
	 * @param sources
	 * @param sourcesPartitioner
	 * @param targets
	 * @param IDHandler
	 * @return
	 */
	final public MatchingEngineProcessResult<ENTITY, ENTITY, CONFIG> compareAndFeedAll(CONFIG configuration,
																					   MatchingProcessHandler<ENTITY, ENTITY> tracker,
																					   FeedableDataProvider<ENTITY> sources,
																					   DataPartitioner<ENTITY, ENTITY> partitioner,
																					   DataProvider<ENTITY> targets,
																					   IDHandler<ENTITY, ?> IDHandler) throws MatchingProcessException {
		$nN(configuration, "Configuration cannot be null");
		$nN(tracker, "Tracker cannot be null");

		$nN(sources, "Sources data cannot be null");
		$_assert(sources.iterator().hasNext(), "The sources must contain at least one element");

		$nN(partitioner, "Sources partitioner cannot be null");

		$nN(targets, "Targets data cannot be null");

		configuration.validate();

		Collection<? extends Matchlet<ENTITY, ?, ENTITY, ?>> matchlets = this.initializeMatchlets(configuration);

		$nEm(matchlets, "Matchlets cannot be null or empty");
		$nN(IDHandler, "ID handler cannot be null");
		MatchingsData<ENTITY, ENTITY> results = new MatchingsData<ENTITY, ENTITY>();

		tracker.notifyComparisonProcessStarted(tracker.getProcessId());

		if(targets instanceof SizeAwareDataProvider)
			tracker.setNumberOfComparisonRounds(((SizeAwareDataProvider<ENTITY>)targets).getAvailableDataSize());

		for(ProvidedData<ENTITY> target : targets) {
			if(!tracker.getProcessStatus().is(MatchingEngineStatus.RUNNING))
				break;

			tracker.notifyComparisonRoundStart(target);

			results = this.performComparison(configuration,
											 tracker,
											 results,
											 sources,
											 target.getData(),
											 target.getProviderId(),
											 partitioner,
											 matchlets,
											 IDHandler,
											 IDHandler);

			tracker.notifyComparisonRoundPerformed(target);

			//Feeds the just compared data to the targets
			sources.addData(target);
		}

		tracker.notifyComparisonProcessCompleted();

		MatchingEngineProcessorInfo<CONFIG> processorInfo = new MatchingEngineProcessorInfo<CONFIG>(this.updateConfiguration(matchlets, configuration), tracker.getProcessStatus());

		return new MatchingEngineProcessResult<ENTITY, ENTITY, CONFIG>(processorInfo,
																	   results);
	}
}