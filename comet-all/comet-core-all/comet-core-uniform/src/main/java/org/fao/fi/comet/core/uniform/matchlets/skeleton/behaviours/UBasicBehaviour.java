/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours;

import java.io.Serializable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
public class UBasicBehaviour<ENTITY, DATA extends Serializable>
	   extends UBehaviourSkeleton<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2095319246569327723L;

	/**
	 * Class constructor
	 *
	 */
	public UBasicBehaviour() {
		super();
	}


}
