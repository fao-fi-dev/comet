/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets.skeleton;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.uniform.matchlets.UScalarMatchlet;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours.UBasicBehaviour;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours.UBehaviourSkeleton;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 May 2013
 */
abstract public class UScalarMatchletSkeleton<ENTITY, DATA extends Serializable>
				extends ScalarMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UScalarMatchlet<ENTITY, DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = 2978294536560560449L;

	/**
	 * Class constructor
	 *
	 */
	public UScalarMatchletSkeleton() {
		super(new UBasicBehaviour<ENTITY, DATA>());
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UScalarMatchletSkeleton(UBehaviourSkeleton<ENTITY, DATA> behaviour) {
		super(behaviour);
	}

	abstract protected DATA doExtractData(ENTITY entity, DataIdentifier dataIdentifier);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	final public DATA extractSourceData(ENTITY source, DataIdentifier sourceIdentifier) {
		return this.doExtractData(source, sourceIdentifier);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	final public DATA extractTargetData(ENTITY target, DataIdentifier targetIdentifier) {
		return this.doExtractData(target, targetIdentifier);
	}
}