/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.Matchlet;

/**
 * The basic uniform matchlet interface.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface UMatchlet<ENTITY, DATA extends Serializable>
	   extends Matchlet<ENTITY, DATA, ENTITY, DATA> {
}