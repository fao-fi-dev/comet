/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.engine.model;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jun 2013
 */
public class UMatchingEngineProcessResult<ENTITY extends Serializable, CONFIG extends MatchingEngineProcessConfiguration> extends MatchingEngineProcessResult<ENTITY, ENTITY, CONFIG> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2570832693526185305L;
}