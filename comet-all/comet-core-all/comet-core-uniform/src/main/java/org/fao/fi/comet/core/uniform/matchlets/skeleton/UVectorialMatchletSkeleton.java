/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets.skeleton;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.matchlets.skeleton.VectorialMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.uniform.matchlets.UMatchlet;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Jul 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Jul 2012
 */
abstract public class UVectorialMatchletSkeleton<ENTITY, DATA extends Serializable>
				extends VectorialMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UMatchlet<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -643401127603076040L;

	abstract protected Collection<DATA> doExtractData(ENTITY entity, DataIdentifier dataIdentifier);

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.VectorialMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	final public Collection<DATA> extractSourceData(ENTITY source, DataIdentifier sourceIdentifier) {
		return this.doExtractData(source, sourceIdentifier);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.VectorialMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	final public Collection<DATA> extractTargetData(ENTITY target, DataIdentifier targetIdentifier) {
		return this.doExtractData(target, targetIdentifier);
	}
}