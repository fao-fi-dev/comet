/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.score.support.MatchingScore;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
public class UBehaviourSkeleton<ENTITY, DATA extends Serializable> extends BehaviourSkeleton<ENTITY, DATA, ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6487504734829476930L;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton#doUpdateScore(org.fao.fi.comet.core.support.MatchingScore, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingScore doUpdateScore(MatchingScore current, DATA sourceData, DATA targetData) {
		return new MatchingScore(current);
	}
}