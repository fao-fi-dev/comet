/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-core-uniform)
 */
package org.fao.fi.comet.core.uniform.matchlets;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.VectorialMatchlet;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface UVectorialMatchlet<ENTITY extends Serializable, DATA extends Serializable>
	   extends VectorialMatchlet<ENTITY, DATA, ENTITY, DATA>, UMatchlet<ENTITY, DATA> {
	/**
	 * @param vessel a vessel
	 * @return the historical data to match for the provided vessel
	 */
	Collection<DATA> getDataToMatch(ENTITY source);
}
