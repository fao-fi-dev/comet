/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.patterns.data.partitioners.impl;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * @author Fiorellato
 *
 */
public class DifferentIDsDataPartitioner<SOURCE extends Identified<? extends Serializable>, TARGET extends Identified<? extends Serializable>> implements DataPartitioner<SOURCE, TARGET> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner#include(java.io.Serializable, java.io.Serializable, java.util.Collection)
	 */
	@Override
	public boolean include(SOURCE source, TARGET target, Collection<? extends Matchlet<SOURCE, ?, TARGET, ?>> matchlets) {
		return ( source == null && target != null ) ||
			   ( source != null && target == null ) ||
			   ( source.getId() == null && target.getId() != null ) ||
			   ( source.getId() != null && target.getId() == null ) ||
			   ( !source.getId().equals(target.getId()) );
	}
}
