/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.VectorialMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface HistoricalDataMatchlet<SOURCE extends Serializable,
									   SOURCE_DATA extends DateReferenced,
									   TARGET extends Serializable,
									   TARGET_DATA extends DateReferenced>
	   extends VectorialMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
}