/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.patterns.handlers.id.impl.basic;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 26/mag/2013
 */
public class CharIDHandler implements IDHandler<Character, Integer> {

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getId(java.io.Serializable)
	 */
	@Override
	public Integer getId(Character data) {
		return data == null ? null : new Integer(data);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#serializeId(java.io.Serializable)
	 */
	@Override
	public String serializeId(Integer id) {
		return id == null ? null : id.toString();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getSerializeId(java.io.Serializable)
	 */
	@Override
	public String getSerializedId(Character data) {
		return this.serializeId(this.getId(data));
	}
	
//	/* (non-Javadoc)
//	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#deserializeId(java.lang.String)
//	 */
//	@Override
//	public Integer deserializeId(String serializedId) {
//		return Integer.valueOf(serializedId);
//	}
}