/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.common.behaviours;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.helpers.TimeDistanceHelper;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
public class TimeDistanceAwareBehaviour<SOURCE extends Serializable,
										SOURCE_DATA extends DateReferenced,
										TARGET extends Serializable,
										TARGET_DATA extends DateReferenced> extends BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8218136145191518357L;

	private final long _absoluteMaximumTimeDistance;
	private final TimeResolutionUnit _timeResolutionUnit;

	/** The time distance helper reference */
	private final TimeDistanceHelper _timeHelper = new TimeDistanceHelper();

	/**
	 * Class constructor
	 *
	 * @param absoluteMaximumTimeDistance
	 * @param timeResolutionUnit
	 */
	public TimeDistanceAwareBehaviour(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit) {
		super();

		assert timeResolutionUnit != null : "Provided time resolution unit cannot be null";
		assert Double.compare(absoluteMaximumTimeDistance, 0D) > 0 : "The absolute maximum time distance cannot be zero or lower (currently: " + absoluteMaximumTimeDistance + ")";

		this._absoluteMaximumTimeDistance = absoluteMaximumTimeDistance;
		this._timeResolutionUnit = timeResolutionUnit;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour#doUpdateScore(org.fao.fi.comet.core.support.MatchingScore, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingScore doUpdateScore(MatchingScore current, SOURCE_DATA sourceData, TARGET_DATA targetData) {
		MatchingScore updated = new MatchingScore(current);

		//Compute the time distance
		Long timeDistance = this._timeHelper.getTimeDistance(sourceData.getReferenceDate(), targetData.getReferenceDate(), this._timeResolutionUnit);

		//If the time distance cannot be computed or it exceeds the maximum time distance,
		//manage it as a NO MATCH
		if(timeDistance == null || timeDistance > this._absoluteMaximumTimeDistance)
			updated = MatchingScore.getNonAuthoritativeNoMatchTemplate();
		else {
			double relativeDistance = timeDistance * 1D / this._absoluteMaximumTimeDistance;

			//Update the score by weighting its value with the relative time distance
			updated.setValue(updated.getValue() * ( 1D - relativeDistance ));
		}

		return updated;
	}
}
