/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.helpers;

import java.io.Serializable;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 12 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 12 Jul 2010
 */
abstract public class StringComparisonHelperSkeleton implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7219197782203476211L;

	final public static double DEFAULT_WEIGHT = 1D;
	final public static boolean MOVE_NUMBERS_TO_RIGHT = true;
	final public static boolean DONT_MOVE_NUMBERS_TO_RIGHT = !StringComparisonHelperSkeleton.MOVE_NUMBERS_TO_RIGHT;

	final public static boolean REMOVE_ALL_SPACES = true;
	final public static boolean DONT_REMOVE_ALL_SPACES = false;

	final public MatchingScore compareStrings(String first, String second, boolean moveNumbersToRight, boolean removeAllSpaces) {
		return this.compareStrings(StringComparisonHelperSkeleton.DEFAULT_WEIGHT, moveNumbersToRight, removeAllSpaces, first, second);
	}

	final public MatchingScore compareStrings(String first, String second, int maximumDistance, boolean moveNumbersToRight, boolean removeAllSpaces) {
		return this.compareStrings(StringComparisonHelperSkeleton.DEFAULT_WEIGHT, maximumDistance, moveNumbersToRight, removeAllSpaces, first, second);
	}

	final public MatchingScore compareStrings(double weight, boolean moveNumbersToRight, boolean removeAllSpaces, String first, String second) {
		assert Double.compare(weight, 0D) >= 0 : "The comparison weight cannot be lower than zero";
		assert Double.compare(weight, 1D) <= 0 : "The comparison weight cannot be higher than one";

		if(first == null && second == null)
			return MatchingScore.getNonPerformedTemplate();

		if(first == null || second == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		double lambda = MatchingScore.SCORE_NO_MATCH;

		String firstString = first.trim().toUpperCase();
		String secondString = second.trim().toUpperCase();

		if(first.trim().equalsIgnoreCase(second)) {
			lambda = MatchingScore.SCORE_FULL_MATCH;
		} else {
			if(moveNumbersToRight) {
				firstString = StringsHelper.shiftNumbersToRight(firstString);
				secondString = StringsHelper.shiftNumbersToRight(secondString);
			}

			if(removeAllSpaces) {
				firstString = StringsHelper.removeAllSpaces(firstString);
				secondString = StringsHelper.removeAllSpaces(secondString);
			} else {
				firstString = StringsHelper.removeMultipleSpaces(firstString);
				secondString = StringsHelper.removeMultipleSpaces(secondString);
			}

			//lambda = MatchingScore.SCORE_FULL_MATCH - this._lexicalHelper.computeRelativeDistance(StringHelper.removeUnnecessarySpaces(firstString), StringHelper.removeUnnecessarySpaces(secondString));

			//Not removing unnecessary spaces, speeds up the comparison process in spite of little less accuracy...
			lambda = MatchingScore.SCORE_FULL_MATCH * ( 1 - StringDistanceHelper.computeRelativeDistance(firstString, secondString) );
		}

		//Decide whether adding a threshold for the lambda computed so far (according to Levenshtein relative distance)

		//This way we ensure that a NO MATCH remains a NO MATCH, while other matchings are updated according to the weight
		return new MatchingScore(lambda * weight, MatchingType.NON_AUTHORITATIVE);
	}

	final protected MatchingScore compareStrings(double weight, int maximumDistance, boolean moveNumbersToRight, boolean removeAllSpaces, String first, String second) {
		assert Double.compare(weight, 0D) >= 0 : "The comparison weight cannot be lower than zero";
		assert Double.compare(weight, 1D) <= 0 : "The comparison weight cannot be higher than one";
		assert maximumDistance >= 0 : "The maximum distance cannot be lower than zero";

		if(first == null && second == null)
			return MatchingScore.getNonPerformedTemplate();

		if(first == null || second == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		double lambda = MatchingScore.SCORE_NO_MATCH;

		String firstString = first.trim().toUpperCase();
		String secondString = second.trim().toUpperCase();

		if(firstString.equals(secondString)) {
			lambda = MatchingScore.SCORE_FULL_MATCH;
		} else {
			if(moveNumbersToRight) {
				firstString = StringsHelper.shiftNumbersToRight(firstString);
				secondString = StringsHelper.shiftNumbersToRight(secondString);
			}

			if(removeAllSpaces) {
				firstString = StringsHelper.removeAllSpaces(firstString);
				secondString = StringsHelper.removeAllSpaces(secondString);
			} else {
				firstString = StringsHelper.removeMultipleSpaces(firstString);
				secondString = StringsHelper.removeMultipleSpaces(secondString);
			}

//			int distance = this._lexicalHelper.computeDistance(StringHelper.removeUnnecessarySpaces(firstString), StringHelper.removeUnnecessarySpaces(secondString));

			//Not removing unnecessary spaces, speeds up the comparison process in spite of little less accuracy...
			int distance = StringDistanceHelper.computeDistance(firstString, secondString);

			if(distance <= maximumDistance)
				lambda = MatchingScore.SCORE_FULL_MATCH * ( 1 - ( maximumDistance > 0 ? ( distance * 1D / maximumDistance ) : ( distance == 0 ? 0D : 1D ) ));
			else
				lambda = MatchingScore.SCORE_NO_MATCH;
		}

		//Decide whether adding a threshold for the lambda computed so far (according to Levenshtein relative distance)

		//This way we ensure that a NO MATCH remains a NO MATCH, while other matchings are updated according to the weight
		return new MatchingScore(lambda * weight, MatchingType.NON_AUTHORITATIVE);
	}
}