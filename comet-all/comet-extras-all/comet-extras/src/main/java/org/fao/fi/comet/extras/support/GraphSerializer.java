/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.support;

import java.io.Serializable;
import java.util.Map;

import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.engine.adapters.IdentifierSerializer;
import org.fao.fi.sh.utility.topology.GraphNode;
import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.impl.SimpleWeightValue;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Oct 29, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Oct 29, 2015
 */
public class GraphSerializer<SOURCE, TARGET> {
	public <I extends Serializable & Comparable<? super I>> WeightedGraph<I> asGraph(MatchingsData<SOURCE, TARGET> data, IdentifierSerializer<I> serializer) {
		WeightedGraph<I> graph = new WeightedGraph<I>();
		GraphNode<I> sourceNode;
		GraphNode<I> targetNode;

		Map<String, MatchingDetails<SOURCE, TARGET>> asMap = data.asMap();
		Map<String, Matching<SOURCE, TARGET>> matchingsMap;
		
		for(Serializable sourceEntryKey : asMap.keySet()) {
			sourceNode = new GraphNode<I>(serializer.from(asMap.get(sourceEntryKey).getSourceId()));

			matchingsMap = asMap.get(sourceEntryKey).asMap();
			
			for(Serializable targetEntryKey : matchingsMap.keySet()) {
				targetNode = new GraphNode<I>(serializer.from(matchingsMap.get(targetEntryKey).getTargetId()));

				graph.link(sourceNode, targetNode, new SimpleWeightValue(matchingsMap.get(targetEntryKey).getScore().getValue()));
			}
		}

		return graph;
	}
}
