/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.historical;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletMetadata;
import org.fao.fi.comet.extras.matchlets.TimeDistanceAwareMatchlet;
import org.fao.fi.comet.extras.matchlets.common.behaviours.TimeDistanceAwareBehaviour;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class HistoricalTimeDistanceAwareMatchletSkeleton<SOURCE extends Serializable,
																 SOURCE_DATA extends DateReferenced,
																 TARGET extends Serializable,
																 TARGET_DATA extends DateReferenced>
				extends HistoricalMatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements TimeDistanceAwareMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -6557383015276568622L;

	/** Field _absoluteMaximumTimeDistance */
	@MatchletMetadata
	private final long _absoluteMaximumTimeDistance;

	/** Field _timeResolutionUnit */
	@MatchletMetadata
	private final TimeResolutionUnit _timeResolutionUnit;

	/**
	 * Class constructor
	 *
	 * @param maximumAbsoluteTimeDistance
	 * @param timeResolutionUnit
	 */
	public HistoricalTimeDistanceAwareMatchletSkeleton(long maximumAbsoluteTimeDistance, TimeResolutionUnit timeResolutionUnit) {
		super(new TimeDistanceAwareBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>(maximumAbsoluteTimeDistance, timeResolutionUnit));

		this._absoluteMaximumTimeDistance = maximumAbsoluteTimeDistance;
		this._timeResolutionUnit = timeResolutionUnit;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.TimeDistanceAwareMatchlet#getAbsoluteMaximumTimeDistance()
	 */
	@Override
	public long getAbsoluteMaximumTimeDistance() {
		return this._absoluteMaximumTimeDistance;
	}

	/**
	 * @return the 'resolutionunit' value
	 */
	@Override
	public TimeResolutionUnit getTimeResolutionUnit() {
		return this._timeResolutionUnit;
	}
}
