/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.common.behaviours;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.helpers.ValueDistanceHelper;
import org.fao.fi.sh.model.core.spi.Valued;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
public class ValueDistanceAwareBehaviour<SOURCE extends Serializable,
										 SOURCE_DATA extends Valued<? extends Number>,
										 TARGET extends Serializable,
										 TARGET_DATA extends Valued<? extends Number>> extends BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8218136145191518357L;

	/** The value distance helper reference */
	private final ValueDistanceHelper<Valued<? extends Number>> _valueHelper = new ValueDistanceHelper<Valued<? extends Number>>();

	private double _maximumRelativeValueDistance = 0D;

	/**
	 * Class constructor
	 *
	 * @param maximumRelativeValueDistance
	 */
	public ValueDistanceAwareBehaviour(double maximumRelativeValueDistance) {
		super();

		assert Double.compare(maximumRelativeValueDistance, 0D) >= 0 : "The maximum relative value distance cannot be lower than zero (currently: " + maximumRelativeValueDistance + ")";
		assert Double.compare(maximumRelativeValueDistance, 0D) <= 1 : "The maximum relative value distance cannot be higher than one (currently: " + maximumRelativeValueDistance + ")";

		this._maximumRelativeValueDistance = maximumRelativeValueDistance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour#doUpdateScore(org.fao.fi.comet.core.support.MatchingScore, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingScore doUpdateScore(MatchingScore current, SOURCE_DATA sourceData, TARGET_DATA targetData) {
		MatchingScore updated = new MatchingScore(current);

		//Compute the relative value distance
		Double relativeValueDistance = this._valueHelper.getRelativeValueDistance(sourceData, targetData);

		if(relativeValueDistance == null || Double.compare(relativeValueDistance, this._maximumRelativeValueDistance) > 0)
			//If the relative value distance cannot be computed, manage it as a NO MATCH
			updated = MatchingScore.getNonAuthoritativeNoMatchTemplate();
		else
			//Update the score by weighting its value with the relative value distance
			updated.setValue(updated.getValue() * ( 1D - relativeValueDistance ));

		return updated;
	}
}
