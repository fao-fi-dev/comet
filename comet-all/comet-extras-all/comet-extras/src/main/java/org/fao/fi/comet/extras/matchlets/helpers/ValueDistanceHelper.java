/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.helpers;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Valued;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
final public class ValueDistanceHelper<DATA extends Valued<? extends Number>> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7425081492706257342L;

	/**
	 * @param firstData the data where the first value should be taken from
	 * @param secondData the data where the second value should be taken from
	 * 
	 * @return the relative value distance (0 means that the two values are the same)
	 */
	final public Double getRelativeValueDistance(DATA firstData, DATA secondData) {
		if(firstData == null || firstData.getValue() == null || 
		   secondData == null || secondData.getValue() == null)
			return null;
		
		double firstValue = firstData.getValue().doubleValue();
		double secondValue = secondData.getValue().doubleValue();
						
		double maxValue = Math.max(Math.abs(firstValue), Math.abs(secondValue));
		double distance = Math.abs(firstValue - secondValue);
		
		double relativeDistance = maxValue == 0 ? 0 : distance / maxValue;
		
		assert relativeDistance >= 0 && relativeDistance <= 1 : "Returned relative distance shall range between 0 and 1 included (was: " + relativeDistance + ")";
		
		return relativeDistance;
	}	
}
