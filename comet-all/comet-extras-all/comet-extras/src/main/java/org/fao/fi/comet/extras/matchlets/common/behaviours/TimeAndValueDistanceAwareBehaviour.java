/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.common.behaviours;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.helpers.TimeDistanceHelper;
import org.fao.fi.comet.extras.matchlets.helpers.ValueDistanceHelper;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.Valued;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
public class TimeAndValueDistanceAwareBehaviour<SOURCE extends Serializable,
												SOURCE_DATA extends DateReferenced & Valued<? extends Number>,
												TARGET extends Serializable,
												TARGET_DATA extends DateReferenced & Valued<? extends Number>> extends BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8218136145191518357L;

	protected final long _absoluteMaximumTimeDistance;
	protected final TimeResolutionUnit _timeResolutionUnit;
	protected final double _maximumRelativeValueDistance;

	/** The time distance helper reference */
	private final TimeDistanceHelper _timeHelper = new TimeDistanceHelper();

	/** The value distance helper reference */
	private final ValueDistanceHelper<Valued<? extends Number>> _valueHelper = new ValueDistanceHelper<Valued<? extends Number>>();

	/**
	 * Class constructor
	 *
	 * @param absoluteMaximumTimeDistance
	 * @param timeResolutionUnit
	 * @param maximumRelativeValueDistance
	 */
	public TimeAndValueDistanceAwareBehaviour(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit, double maximumRelativeValueDistance) {
		super();

		assert timeResolutionUnit != null : "Provided time resolution unit cannot be null";
		assert Double.compare(absoluteMaximumTimeDistance, 0D) > 0 : "The absolute maximum time distance cannot be zero or lower (currently: " + absoluteMaximumTimeDistance + ")";
		assert Double.compare(maximumRelativeValueDistance, 0D) >= 0 : "The maximum relative value distance cannot be lower than zero (currently: " + maximumRelativeValueDistance + ")";
		assert Double.compare(maximumRelativeValueDistance, 0D) <= 1 : "The maximum relative value distance cannot be higher than one (currently: " + maximumRelativeValueDistance + ")";

		this._absoluteMaximumTimeDistance = absoluteMaximumTimeDistance;
		this._timeResolutionUnit = timeResolutionUnit;
		this._maximumRelativeValueDistance = maximumRelativeValueDistance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour#doUpdateScore(org.fao.fi.comet.core.support.MatchingScore, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingScore doUpdateScore(MatchingScore current, SOURCE_DATA sourceData, TARGET_DATA targetData) {
		MatchingScore updated = new MatchingScore(current);

		Long timeDistance = this._timeHelper.getTimeDistance(sourceData.getReferenceDate(), targetData.getReferenceDate(), this._timeResolutionUnit);

		//If the time distance cannot be computed or it exceeds the maximum time distance,
		//manage it as a NO MATCH
		if(timeDistance == null || timeDistance > this._absoluteMaximumTimeDistance)
			updated = MatchingScore.getNonAuthoritativeNoMatchTemplate();
		else {
			double relativeTimeDistance = timeDistance * 1D / this._absoluteMaximumTimeDistance;

			//Update the score value by weighting it with the relative time distance
			updated.setValue(updated.getValue() * ( 1D - relativeTimeDistance ));

			if(this.continueComparison(updated)) {
				//Compute the relative value distance
				Double relativeValueDistance = this._valueHelper.getRelativeValueDistance(sourceData, targetData);

				if(relativeValueDistance == null || Double.compare(relativeValueDistance, this._maximumRelativeValueDistance) > 0)
					updated = MatchingScore.getNonAuthoritativeNoMatchTemplate();
				else {
					//Update the score by weighting its value with the relative value distance
					updated.setValue(updated.getValue() * ( 1D - relativeValueDistance ));
				}
			}
		}

		return updated;
	}


}