/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.patterns.handlers.id.impl;

import java.io.Serializable;

import org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 24/mag/2013
 */
abstract public class IdentifiableDataIDConverterHandler<DATA extends Identified<IDENTIFIER>, IDENTIFIER extends Serializable> extends IDHandlerSkeleton<DATA, IDENTIFIER> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.io.Serializable)
	 */
	@Override
	protected IDENTIFIER doGetId(DATA data) {
		return data.getId();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doSerializeId(java.io.Serializable)
	 */
	@Override
	protected String doSerializeId(IDENTIFIER id) {
		return id.toString();
	}
}
