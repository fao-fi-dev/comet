/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.common;

import static org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy.NON_PERFORMED;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletForcesComparisonByDefault;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.sh.model.core.spi.Groupable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletForcesComparisonByDefault
@MatchletDefaultSerializationExclusionPolicy(value={NON_PERFORMED})
public class MappedEntityMatchlet<SOURCE extends Groupable<SOURCE_DATA>,
								  SOURCE_DATA extends Serializable,
								  TARGET extends Groupable<TARGET_DATA>,
								  TARGET_DATA extends Serializable>
	   extends IdentityMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public MappedEntityMatchlet() {
		super();

		this._name = "MappedEntityMatchlet";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.extras.matchlets.common.IdentityMatchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(SOURCE source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, TARGET target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		return sourceData != null || targetData != null ? MatchingScore.getAuthoritativeNoMatchTemplate() : MatchingScore.getNonPerformedTemplate();
	}
}