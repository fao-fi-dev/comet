/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.current;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.CurrentDataMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
abstract public class CurrentMatchletSkeleton<SOURCE extends Serializable,
											 SOURCE_DATA extends DateReferenced,
											 TARGET extends Serializable,
											 TARGET_DATA extends DateReferenced>
				extends MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements CurrentDataMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2794121500041289312L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public CurrentMatchletSkeleton() {
		this(new BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>());
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public CurrentMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#compareData(org.fao.vrmf.core.behaviours.data.Identifiable, org.fao.vrmf.core.behaviours.data.Identifiable)
	 */
	@Override
	public MatchingResult<SOURCE_DATA, TARGET_DATA> compareData(SOURCE source, DataIdentifier sourceIdentifier, TARGET target, DataIdentifier targetIdentifier) {
		$nN(source, "Source cannot be null");
		$nN(sourceIdentifier, "Source identifier cannot be null");
		$nN(target, "Target cannot be null");
		$nN(targetIdentifier, "Target identifier cannot be null");

		SOURCE_DATA sourceData = this.extractSourceData(source, sourceIdentifier);
		TARGET_DATA targetData = this.extractTargetData(target, targetIdentifier);

		MatchingScore score = null;

		if(this.forceComparison()) {
			score = this.computeScore(source, sourceIdentifier, sourceData, target, targetIdentifier, targetData);
		} else {
			if(sourceData == null && targetData == null) {
				score = MatchingScore.getNonPerformedTemplate();
			} else if(sourceData == null || targetData == null) {
				if(this.isOptional())
					score = MatchingScore.getNonPerformedTemplate();
				else
					score = MatchingScore.getNonAuthoritativeNoMatchTemplate();
			} else {
				score = this.computeScore(source, sourceIdentifier, sourceData, target, targetIdentifier, targetData);
			}
		}

		MatchingScore updated = this._behaviour.updateScore(score, source, sourceData, target, targetData);

		return this.updateResult(this.newMatchingResult(), updated, sourceData, sourceIdentifier, targetData);
	}
}