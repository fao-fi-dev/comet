/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.historical;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$nN;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BasicBehaviour;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingResultData;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.HistoricalDataMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
abstract public class HistoricalMatchletSkeleton<SOURCE extends Serializable,
												SOURCE_DATA extends DateReferenced,
												TARGET extends Serializable,
												TARGET_DATA extends DateReferenced>
				extends MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements HistoricalDataMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2794121500041289312L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public HistoricalMatchletSkeleton() {
		this(new BasicBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>());
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public HistoricalMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton#compareData(org.fao.vrmf.core.behaviours.data.Identifiable, org.fao.vrmf.core.behaviours.data.Identifiable)
	 */
	@Override
	public MatchingResult<SOURCE_DATA, TARGET_DATA> compareData(SOURCE source, DataIdentifier sourceIdentifier, TARGET target, DataIdentifier targetIdentifier) {
		$nN(source, "Source cannot be null");
		$nN(sourceIdentifier, "Source identifier cannot be null");
		$nN(target, "Target cannot be null");
		$nN(targetIdentifier, "Target identifier cannot be null");

		Collection<SOURCE_DATA> sourceData = this.extractSourceData(source, sourceIdentifier);
		Collection<TARGET_DATA> targetData = this.extractTargetData(target, targetIdentifier);

		boolean noSourceData = sourceData == null;
		boolean noTargetData = targetData == null;

		boolean emptySourceData = noSourceData || sourceData.isEmpty();
		boolean emptyTargetData = noTargetData || targetData.isEmpty();

		boolean dontPerformMatching = noSourceData && noTargetData;
		boolean assertNoMatch = ( emptySourceData || emptyTargetData );

		MatchingResult<SOURCE_DATA, TARGET_DATA> result = this.newMatchingResult();

		//Matching cannot be performed (no data to compare)
		if(dontPerformMatching) {
			result.setScore(MatchingScore.getNonPerformedTemplate());

			return result;
		//Perform the match
		} else if(!assertNoMatch) {
			MatchingResult<SOURCE_DATA, TARGET_DATA> temporaryResult = null;

			MatchingScore currentScore = null;

			for(SOURCE_DATA currentSourceData : sourceData) {
				for(TARGET_DATA currentTargetData : targetData) {
					if(currentSourceData == null && currentTargetData == null) {
						currentScore = MatchingScore.getNonPerformedTemplate();
					} else if(currentSourceData == null || currentTargetData == null) {
						currentScore = MatchingScore.getNonAuthoritativeNoMatchTemplate();
					} else {
						currentScore = this._behaviour.updateScore(this.computeScore(source, sourceIdentifier, currentSourceData, target, targetIdentifier, currentTargetData), source, currentSourceData, target, currentTargetData);
					}

					temporaryResult = this.updateResult(new MatchingResult<SOURCE_DATA, TARGET_DATA>(this), currentScore, currentSourceData, sourceIdentifier, currentTargetData);

					if(temporaryResult.isAuthoritative())
						return temporaryResult;

					if(!temporaryResult.isNonPerformed()) {
						//Currently compared data have a higher level of score: replace the result with the one for the currently compared data
						if(Double.compare(temporaryResult.getScore().getValue(), result.getScore().getValue()) > 0) {
							result = temporaryResult;
						} else if(Double.compare(temporaryResult.getScore().getValue(), result.getScore().getValue()) == 0) {
							//Currently compared data have the same level of score: update the result by adding / merging the currently compared data
							MatchingResultData<SOURCE_DATA, TARGET_DATA> matching = null;

							if(result.containsMatchingFor(currentSourceData, sourceIdentifier) && currentTargetData != null) {
								matching = result.getMatchingsResultDataFor(currentSourceData, sourceIdentifier);

								matching.getMatchingTargetData().add(currentTargetData);
							} else {
								result.getMatchingsResultData().addAll(temporaryResult.getMatchingsResultData());
							}

							result.getScore().setMatchingType(temporaryResult.getScore().getMatchingType());
						} else {
							; //DO NOTHING: computed score is lower than currently set one
						}
					}
				}
			}

			return result;
		} else {
			if(this.isOptional())
				result.setScore(MatchingScore.getNonPerformedTemplate());
			else
				result.setScore(MatchingScore.getNonAuthoritativeNoMatchTemplate());

			return result;
		}
	}
}