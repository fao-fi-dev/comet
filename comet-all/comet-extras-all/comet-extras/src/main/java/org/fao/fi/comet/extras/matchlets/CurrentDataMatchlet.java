/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.ScalarMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * The matchlet interface. To be implemented by each custom matchlet (for
 * vessels, vessel names, reg. nos etc.)
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public interface CurrentDataMatchlet<SOURCE extends Serializable,
									SOURCE_DATA extends DateReferenced,
									TARGET extends Serializable,
									TARGET_DATA extends DateReferenced>
	   extends ScalarMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
}