/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.patterns.handlers.id.impl.basic;

import org.fao.fi.comet.extras.patterns.handlers.id.impl.IdentifiableDataIDConverterHandler;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 24/mag/2013
 */
public class IdentifiableDataStringIDHandler<DATA extends Identified<String>> extends IdentifiableDataIDConverterHandler<DATA, String> {
//	/* (non-Javadoc)
//	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doDeserializeId(java.lang.String)
//	 */
//	@Override
//	protected String doDeserializeId(String serializedId) {
//		return serializedId;
//	}
}