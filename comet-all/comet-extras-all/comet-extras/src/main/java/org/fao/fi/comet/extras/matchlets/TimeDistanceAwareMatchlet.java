/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Matchlet interface for time-distances aware matchlets
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public interface TimeDistanceAwareMatchlet<SOURCE extends Serializable,
										  SOURCE_DATA extends DateReferenced,
										  TARGET extends Serializable,
										  TARGET_DATA extends DateReferenced> extends Matchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/**
	 * @return the time resolution unit currently set for the matchlet
	 */
	TimeResolutionUnit getTimeResolutionUnit();

	/**
	 * @return the current maximum time distance to be considered (according to
	 * currently set time resolution unit)
	 */
	long getAbsoluteMaximumTimeDistance();
}