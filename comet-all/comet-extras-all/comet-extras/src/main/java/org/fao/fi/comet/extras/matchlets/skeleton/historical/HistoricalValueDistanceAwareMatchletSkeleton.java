/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.historical;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletMetadata;
import org.fao.fi.comet.extras.matchlets.ValueDistanceAwareMatchlet;
import org.fao.fi.comet.extras.matchlets.common.behaviours.ValueDistanceAwareBehaviour;
import org.fao.fi.comet.extras.matchlets.helpers.ValueDistanceHelper;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.Valued;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class HistoricalValueDistanceAwareMatchletSkeleton<SOURCE extends Serializable,
																  SOURCE_DATA extends DateReferenced & Valued<? extends Number>,
																  TARGET extends Serializable,
																  TARGET_DATA extends DateReferenced & Valued<? extends Number>>
				extends HistoricalMatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements ValueDistanceAwareMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -6557383015276568622L;

	/** Field _maximumRelativeValueDistance */
	@MatchletMetadata
	private double _maximumRelativeValueDistance = 0;

	/** The value helper */
	protected ValueDistanceHelper<Valued<? extends Number>> _valueHelper = new ValueDistanceHelper<Valued<? extends Number>>();

	/**
	 * Class constructor
	 *
	 * @param maximumRelativeValueDistance
	 */
	public HistoricalValueDistanceAwareMatchletSkeleton(double maximumRelativeValueDistance) {
		super(new ValueDistanceAwareBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>(maximumRelativeValueDistance));

		this._maximumRelativeValueDistance = maximumRelativeValueDistance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ValueDistanceAwareMatchlet#getMaximumRelativeValueDistance()
	 */
	@Override
	public double getMaximumRelativeValueDistance() {
		return this._maximumRelativeValueDistance;
	}
}