/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.matchlets.ScalarMatchlet;
import org.fao.fi.comet.extras.matchlets.skeleton.SimpleNameMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.SimplifiedNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
abstract public class CurrentSimpleNameMatchletSkeleton<SOURCE extends Serializable,
													   SOURCE_DATA extends DateReferenced & SimplifiedNameAware,
													   TARGET extends Serializable,
													   TARGET_DATA extends DateReferenced & SimplifiedNameAware>
				extends SimpleNameMatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements ScalarMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;

	/**
	 * Class constructor
	 *
	 */
	public CurrentSimpleNameMatchletSkeleton() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 * @param simplifiedNameWeight
	 */
	public CurrentSimpleNameMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour, 	double simplifiedNameWeight) {
		super(behaviour, simplifiedNameWeight);
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public CurrentSimpleNameMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/**
	 * Class constructor
	 *
	 * @param simplifiedNameWeight
	 */
	public CurrentSimpleNameMatchletSkeleton(double simplifiedNameWeight) {
		super(simplifiedNameWeight);
	}


}