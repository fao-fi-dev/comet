/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.helpers;

import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.extras.matchlets.skeleton.helpers.StringComparisonHelperSkeleton;
import org.fao.fi.sh.model.core.spi.SimplifiedNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
final public class SimpleNameHelper<DATA extends SimplifiedNameAware> extends StringComparisonHelperSkeleton {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5095522091352365846L;

	/**
	 * Computes the matching score of two named data
	 *
	 * @param simplifiedNameWeight how much should the simplified name matching (when available) weight
	 * @param firstData the first simplified named data
	 * @param secondData the second simplified named data
	 *
	 * @return the matching score for the name attributes of the two provided data
	 */
	public MatchingScore computeScore(double simplifiedNameWeight, DATA firstData, DATA secondData) {
		assert Double.compare(simplifiedNameWeight, MatchingScore.SCORE_NO_MATCH) >= 0 : "The simplified name weight cannot be lower than zero";
		assert Double.compare(simplifiedNameWeight, MatchingScore.SCORE_FULL_MATCH) <= 0 : "The simplified name weight cannot be greater than one";

		String firstName = firstData.getName();
		String secondName = secondData.getName();

		String simplifiedFirstName = firstData.getSimplifiedName();
		String simplifiedSecondName = secondData.getSimplifiedName();

		if(firstName == null && secondName == null)
			return MatchingScore.getNonPerformedTemplate();

		if(firstName == null || secondName == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		assert simplifiedFirstName != null || simplifiedSecondName != null : "A non-null name should not lead to a null simplified name";

		if(firstName.trim().equalsIgnoreCase(secondName.trim()))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		return super.compareStrings(simplifiedNameWeight, MOVE_NUMBERS_TO_RIGHT, REMOVE_ALL_SPACES, simplifiedFirstName, simplifiedSecondName);
	}
}