/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.common;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletDefaultSerializationExclusionPolicy
public class IdentityMatchlet<SOURCE extends Identified<SOURCE_DATA>,
							  SOURCE_DATA extends Serializable,
							  TARGET extends Identified<TARGET_DATA>,
							  TARGET_DATA extends Serializable>
	   extends ScalarMatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 */
	public IdentityMatchlet() {
		super();

		this._name="IdentityMatchlet";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Matches entity identifiers as extracted from a pair of input / reference matched entities. Returns an AUTHORITATIVE NO MATCH if the two identifiers being " +
			   "compared are the same, and a NON PERFORMED result if they differ. This matchlet has mainly the purpose to avoid that two entities of the same type AND with " +
			   "the same ID are compared, as they are indeed expected to represent the same data.";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	public SOURCE_DATA extractSourceData(SOURCE source, DataIdentifier sourceIdentifier) {
		return source.getId();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	public TARGET_DATA extractTargetData(TARGET target, DataIdentifier sourceIdentifier) {
		return target.getId();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(SOURCE source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, TARGET target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		String sourceProviderId, targetProviderId;
		sourceProviderId = sourceIdentifier.getProviderId();
		targetProviderId = targetIdentifier.getProviderId();

		if(sourceProviderId.equals(targetProviderId) && sourceData.equals(targetData))
			return MatchingScore.getAuthoritativeNoMatchTemplate(); //Halts comparison when first and second data have the same ID and the same provider (i.e. are the same)

		return MatchingScore.getNonPerformedTemplate();
	}
}