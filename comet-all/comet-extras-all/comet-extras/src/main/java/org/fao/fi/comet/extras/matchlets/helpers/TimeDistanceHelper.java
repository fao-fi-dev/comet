/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.helpers;

import java.io.Serializable;
import java.util.Date;

import org.fao.fi.sh.utility.model.extensions.dates.SmartDate;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
final public class TimeDistanceHelper implements Serializable {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7425081492706257342L;

	/**
	 * @param firstDate
	 * @param secondDate
	 * @param timeUnit
	 * @return
	 */
	public Long getTimeDistance(Date firstDate, Date secondDate, TimeResolutionUnit timeUnit) {
		if(firstDate == null || secondDate == null)			
			return null;
		
		return Long.valueOf(Math.abs(new SmartDate(firstDate).dateDifference(secondDate, timeUnit.getUnit())));
	}	
}