/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.MatchletSkeleton;
import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletMetadata;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.sh.model.core.spi.SimplifiedNameAware;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
abstract public class SimpleNameMatchletSkeleton<SOURCE extends Serializable,
												SOURCE_DATA extends SimplifiedNameAware,
												TARGET extends Serializable,
												TARGET_DATA extends SimplifiedNameAware>
				extends MatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;

	/** The value to weight the simplified name matching when needed */
	@MatchletMetadata
	protected double _simplifiedNameWeight = .7D;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public SimpleNameMatchletSkeleton() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public SimpleNameMatchletSkeleton(double simplifiedNameWeight) {
		super();

		this._simplifiedNameWeight = simplifiedNameWeight;
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 * @param simplifiedNameWeight
	 */
	public SimpleNameMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour) {
		super(behaviour);
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 * @param simplifiedNameWeight
	 */
	public SimpleNameMatchletSkeleton(BehaviourSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> behaviour, double simplifiedNameWeight) {
		super(behaviour);

		assert Double.compare(simplifiedNameWeight, 0) >= 0;

		this._simplifiedNameWeight = simplifiedNameWeight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(SOURCE source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, TARGET target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		String sourceName = StringsHelper.trim(sourceData.getName());
		String targetName = StringsHelper.trim(targetData.getName());

		String simplifiedSourceName = StringsHelper.trim(sourceData.getSimplifiedName());
		String simplifiedTargetName = StringsHelper.trim(targetData.getSimplifiedName());

		if(sourceName == null && targetName == null)
			return MatchingScore.getNonPerformedTemplate();

		if(sourceName == null || targetName == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		if(sourceName.trim().equalsIgnoreCase(targetName.trim()))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		if(simplifiedSourceName == null && simplifiedTargetName == null)
			return MatchingScore.getNonPerformedTemplate();

		if(simplifiedSourceName == null || simplifiedTargetName == null)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();

		double lambda = MatchingScore.SCORE_NO_MATCH;

		if(simplifiedSourceName.trim().equalsIgnoreCase(simplifiedTargetName)) {
			lambda = MatchingScore.SCORE_FULL_MATCH;
		} else {
			lambda = StringDistanceHelper.computeRelativeDistance(simplifiedSourceName.toUpperCase(), simplifiedTargetName.toUpperCase());
		}

		//This way we ensure that a NO MATCH remains a NO MATCH, while other matchings are updated according to the weight
		return new MatchingScore(this._simplifiedNameWeight * lambda, MatchingType.NON_AUTHORITATIVE);
	}
}