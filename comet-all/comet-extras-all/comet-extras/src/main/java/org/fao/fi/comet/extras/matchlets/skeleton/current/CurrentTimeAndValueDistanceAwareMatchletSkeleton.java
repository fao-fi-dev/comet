/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras)
 */
package org.fao.fi.comet.extras.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletMetadata;
import org.fao.fi.comet.extras.matchlets.TimeDistanceAwareMatchlet;
import org.fao.fi.comet.extras.matchlets.ValueDistanceAwareMatchlet;
import org.fao.fi.comet.extras.matchlets.common.behaviours.TimeAndValueDistanceAwareBehaviour;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.Valued;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class CurrentTimeAndValueDistanceAwareMatchletSkeleton<SOURCE extends Serializable,
																	  SOURCE_DATA extends DateReferenced & Valued<? extends Number>,
																	  TARGET extends Serializable,
																	  TARGET_DATA extends DateReferenced & Valued<? extends Number>>
				extends CurrentMatchletSkeleton<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>
				implements TimeDistanceAwareMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>,
						   ValueDistanceAwareMatchlet<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6557383015276568622L;

	/** Field _maximumRelativeValueDistance */
	@MatchletMetadata
	private double _maximumRelativeValueDistance = 0;

	/** Field _absoluteMaximumTimeDistance */
	@MatchletMetadata
	private final long _absoluteMaximumTimeDistance;

	/** Field _timeResolutionUnit */
	@MatchletMetadata
	private final TimeResolutionUnit _timeResolutionUnit;

	/**
	 * Class constructor
	 *
	 * @param absoluteMaximumTimeDistance
	 * @param timeResolutionUnit
	 * @param maximumRelativeValueDistance
	 */
	public CurrentTimeAndValueDistanceAwareMatchletSkeleton(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit, double maximumRelativeValueDistance) {
		super(new TimeAndValueDistanceAwareBehaviour<SOURCE, SOURCE_DATA, TARGET, TARGET_DATA>(absoluteMaximumTimeDistance, timeResolutionUnit, maximumRelativeValueDistance));

		this._absoluteMaximumTimeDistance = absoluteMaximumTimeDistance;
		this._timeResolutionUnit = timeResolutionUnit;
		this._maximumRelativeValueDistance = maximumRelativeValueDistance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.TimeDistanceAwareMatchlet#getTimeResolutionUnit()
	 */
	@Override
	public TimeResolutionUnit getTimeResolutionUnit() {
		return this._timeResolutionUnit;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.TimeDistanceAwareMatchlet#getAbsoluteMaximumTimeDistance()
	 */
	@Override
	public long getAbsoluteMaximumTimeDistance() {
		return this._absoluteMaximumTimeDistance;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ValueDistanceAwareMatchlet#getMaximumRelativeValueDistance()
	 */
	@Override
	public double getMaximumRelativeValueDistance() {
		return this._maximumRelativeValueDistance;
	}
}