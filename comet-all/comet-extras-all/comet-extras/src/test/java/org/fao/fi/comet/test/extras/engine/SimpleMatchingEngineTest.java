/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.test.extras.engine;

import static org.fao.fi.comet.core.model.matchlets.Matchlet.FORCE_COMPARISON_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.MIN_SCORE_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.OPTIONAL_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.WEIGHT_PARAM;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.core.engine.MatchingEngineCore;
import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.extras.patterns.handlers.id.impl.basic.CharIDHandler;
import org.fao.fi.comet.extras.patterns.handlers.id.impl.basic.IdentifiableDataIntegerIDHandler;
import org.fao.fi.comet.test.extras.support.data.SimpleMappableData;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 14/mar/2013
 */
public class SimpleMatchingEngineTest {
	private MatchingEngineCore<Character, SimpleMappableData, MatchingEngineProcessConfiguration> getEngine() {
		return new MatchingEngineCore<Character, SimpleMappableData, MatchingEngineProcessConfiguration>();
	}

	private MatchingEngineCore<Character, SimpleMappableData, MatchingEngineProcessConfiguration> getEngine(int numThreads) {
		return new MatchingEngineCore<Character, SimpleMappableData, MatchingEngineProcessConfiguration>(numThreads);
	}

	private Collection<MatchletConfiguration> getMatchletsConfiguration() {
		Collection<MatchletConfiguration> matchlets = new ArrayList<MatchletConfiguration>();
		matchlets.add(MatchletConfiguration.configure("SimpleMappableDataMatchlet").
													  with(WEIGHT_PARAM, 100D).
													  with(MIN_SCORE_PARAM, .5).
													  with("delta", 1).
													  withFalse(FORCE_COMPARISON_PARAM).
													  withFalse(OPTIONAL_PARAM));

		matchlets.add(MatchletConfiguration.configure("SimpleMappableDataMatchlet").
													  with(WEIGHT_PARAM, 50D).
													  with(MIN_SCORE_PARAM, .5).
													  with("delta", 2).
													  withFalse(FORCE_COMPARISON_PARAM).
													  withFalse(OPTIONAL_PARAM));

		return matchlets;
	}

	private MatchingEngineProcessConfiguration configure(double minScore, int maxCandidates) {
		MatchingEngineProcessConfiguration config = new MatchingEngineProcessConfiguration();
		config.setMinimumAllowedWeightedScore(minScore);
		config.setMaxCandidatesPerEntry(maxCandidates);
		config.setHaltAtFirstValidMatching(false);

		config.setMatchletsConfiguration(this.getMatchletsConfiguration());

		return config;
	}

	private DataProvider<Character> getSourceDataProvider(int initialId, int finalId) {
		Collection<Character> dataset = new ListSet<Character>();

		char data;
		for(int c=initialId; c<=finalId; c++) {
			data = (char)('a' + c - 1);
			dataset.add(new Character(data));
		}

		return new CollectionBackedDataProvider<Character>(dataset);
	}

	private DataProvider<SimpleMappableData> getTargetDataProvider(int initialId, int finalId) {
		Collection<SimpleMappableData> dataset = new ListSet<SimpleMappableData>();

		char data;
		for(int c=initialId; c<=finalId; c++) {
			data = (char)('a' + c - 1);
			dataset.add(new SimpleMappableData(new Integer(c), new Character(data)));
		}

		return new CollectionBackedDataProvider<SimpleMappableData>(dataset);
	}

	@Test
	public void testSequentialMatching() throws Throwable {
		this.testMatching(1);
	}

	@Test
	public void test2ParallelMatching() throws Throwable {
		this.testMatching(2);
	}

	@Test
	public void test3ParallelMatching() throws Throwable {
		this.testMatching(3);
	}

	@Test
	public void test4ParallelMatching() throws Throwable {
		this.testMatching(4);
	}

	@Test
	public void test6ParallelMatching() throws Throwable {
		this.testMatching(6);
	}

	@Test
	public void testDefaultMatching() throws Throwable {
		this.testMatching(0);
	}

	private void testMatching(int numThreads) throws Throwable {
		System.out.println("Testing using " + numThreads + " threads");

		MatchingEngineCore<Character, SimpleMappableData, MatchingEngineProcessConfiguration> engine = numThreads > 0 ? this.getEngine(numThreads) : this.getEngine();
		MatchingEngineProcessConfiguration conf = this.configure(.5, 2);
		MatchingProcessHandler<Character, SimpleMappableData> tracker = null;

		tracker = new SilentMatchingProcessHandler<Character, SimpleMappableData>();
//		tracker = new SimpleGUIMatchingProcessHandler();
//		tracker = new SysOutMatchingProcessHandler();

//		String XML = XMLBuilderUtils.prettyPrint(
//						JAXBUtils.toXML(
//							new Class[] { SimpleMappableData.class },
//							engine.compareAll(
//								conf,
//								tracker,
//								this.getSourceDataProvider(1, 900),
//								new IdentityDataPartitioner<Character, SimpleMappableData>(),
//								this.getTargetDataProvider(1, 700),
//								new CharIDHandler(),
//								new IdentifiableDataIntegerIDHandler<SimpleMappableData>()
//							)
//						)
//					);
//
//		System.out.println(XML);

		engine.compareAll(
			conf,
			tracker,
			this.getSourceDataProvider(1, 900),
			new IdentityDataPartitioner<Character, SimpleMappableData>(),
			this.getTargetDataProvider(1, 700),
			new CharIDHandler(),
			new IdentifiableDataIntegerIDHandler<SimpleMappableData>()
		);

		System.out.println(numThreads + " threads - Total Errors: " + tracker.getNumberOfComparisonErrors());
		System.out.println(numThreads + " threads - Total Matches: " + tracker.getTotalNumberOfMatches());
		System.out.println(numThreads + " threads - Matches: " + tracker.getNumberOfMatches());
		System.out.println(numThreads + " threads - Auth. full matches: " + tracker.getNumberOfAuthoritativeFullMatches());
		System.out.println(numThreads + " threads - Elapsed: " + tracker.getElapsed());
		System.out.println(numThreads + " threads - Atomic comparisons: " + tracker.getTotalNumberOfAtomicComparisonsPerformed());
		System.out.println(numThreads + " threads - Atomic throughput: " + tracker.getAtomicThroughput() * 1000 + " atomic comparisons per second");
		System.out.println(numThreads + " threads - Throughput: " + tracker.getThroughput() * 1000 + " comparisons per second");
	}
}