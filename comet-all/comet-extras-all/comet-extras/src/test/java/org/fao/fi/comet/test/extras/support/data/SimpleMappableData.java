/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.test.extras.support.data;

import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 14/mar/2013
 */
public class SimpleMappableData implements Identified<Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2684128294518992462L;

	private Character _data;
	private Integer _id;
	
	/**
	 * Class constructor
	 *
	 */
	public SimpleMappableData() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param data
	 * @param id
	 */
	public SimpleMappableData(Integer id, Character data) {
		super();
		this._id = id;
		this._data = data;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Identifiable#getId()
	 */
	@Override
	public Integer getId() {
		return this._id;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Identifiable#setId(java.io.Serializable)
	 */
	@Override
	public void setId(Integer id) {
		this._id= id;
	}

	/**
	 * @return the 'data' value
	 */
	public Character getData() {
		return this._data;
	}

	/**
	 * @param data the 'data' value to set
	 */
	public void setData(Character data) {
		this._data = data;
	}
}