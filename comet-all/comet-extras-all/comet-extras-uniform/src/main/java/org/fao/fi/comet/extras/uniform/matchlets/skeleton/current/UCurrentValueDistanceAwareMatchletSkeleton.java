/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.skeleton.current.CurrentValueDistanceAwareMatchletSkeleton;
import org.fao.fi.comet.extras.uniform.matchlets.UValueDistanceAwareMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.Valued;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class UCurrentValueDistanceAwareMatchletSkeleton<ENTITY extends Serializable,
																DATA extends DateReferenced & Valued<? extends Number>>
				extends CurrentValueDistanceAwareMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UValueDistanceAwareMatchlet<ENTITY, DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -2196323377336445514L;

	/**
	 * Class constructor
	 *
	 * @param maximumRelativeValueDistance
	 */
	public UCurrentValueDistanceAwareMatchletSkeleton(double maximumRelativeValueDistance) {
		super(maximumRelativeValueDistance);
	}
}