/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.skeleton.current.CurrentTimeDistanceAwareMatchletSkeleton;
import org.fao.fi.comet.extras.uniform.matchlets.UTimeDistanceAwareMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class UCurrentTimeDistanceAwareMatchletSkeleton<ENTITY extends Serializable,
															   DATA extends DateReferenced>
				extends CurrentTimeDistanceAwareMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UTimeDistanceAwareMatchlet<ENTITY, DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = 6759850865258891691L;

	/**
	 * Class constructor
	 *
	 * @param maximumTimeDistance
	 * @param resolutionunit
	 */
	public UCurrentTimeDistanceAwareMatchletSkeleton(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit) {
		super(absoluteMaximumTimeDistance, timeResolutionUnit);
	}
}