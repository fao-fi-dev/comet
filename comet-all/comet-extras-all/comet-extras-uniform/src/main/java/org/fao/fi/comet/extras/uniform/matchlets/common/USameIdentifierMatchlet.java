/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.common;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletDefaultSerializationExclusionPolicy
@MatchletIsCutoffByDefault
@MatchletIsOptionalByDefault
public class USameIdentifierMatchlet<ENTITY extends Serializable, DATA extends Serializable>
	   extends UScalarMatchletSkeleton<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public USameIdentifierMatchlet() {
		super();

		this._name = "UniformSameIdentifierMatchlet";
	}



	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Fake " + this.getClass().getSimpleName() + " description";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.io.Serializable, org.fao.fi.comet.core.engine.model.DataIdentifier)
	 */
	@Override
	protected DATA doExtractData(ENTITY entity, DataIdentifier dataIdentifier) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, org.fao.fi.comet.core.engine.model.DataIdentifier, java.io.Serializable, java.io.Serializable, org.fao.fi.comet.core.engine.model.DataIdentifier, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(ENTITY source, DataIdentifier sourceIdentifier, DATA sourceData, ENTITY target, DataIdentifier targetIdentifier, DATA targetData) {
		return sourceIdentifier.equals(targetIdentifier) ? MatchingScore.getAuthoritativeNoMatchTemplate() : MatchingScore.getNonPerformedTemplate();
	}
}