/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.common.behaviours;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.common.behaviours.TimeDistanceAwareBehaviour;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
final public class UTimeDistanceAwareBehaviour<ENTITY extends Serializable,
													 DATA extends DateReferenced> extends TimeDistanceAwareBehaviour<ENTITY, DATA, ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8218136145191518357L;
	
	/**
	 * Class constructor
	 *
	 * @param absoluteMaximumTimeDistance
	 * @param timeResolutionUnit
	 */
	public UTimeDistanceAwareBehaviour(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit) {
		super(absoluteMaximumTimeDistance, timeResolutionUnit);
	}
}