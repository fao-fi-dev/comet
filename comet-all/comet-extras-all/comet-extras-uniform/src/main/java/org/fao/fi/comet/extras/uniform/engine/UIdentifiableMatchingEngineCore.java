/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.engine;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23/mag/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 23/mag/2013
 */
abstract public class UIdentifiableMatchingEngineCore<IDENTIFIER extends Serializable, ENTITY extends Identified<IDENTIFIER>, CONFIG extends MatchingEngineProcessConfiguration> extends UMatchingEngineCore<ENTITY, CONFIG> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore#doGetMatching(org.fao.fi.comet.core.engine.model.MatchingsData, java.io.Serializable, java.io.Serializable, org.fao.fi.comet.core.engine.model.MatchingResult)
	 */
	@Override
	protected Matching<ENTITY, ENTITY> doGetMatching(MatchingsData<ENTITY, ENTITY> matchingsData, DataIdentifier sourceIdentifier, DataIdentifier targetIdentifier, MatchingResult<?, ?> currentMatchingResult) {
		return super.doGetMatching(matchingsData, sourceIdentifier, targetIdentifier, currentMatchingResult);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore#doGetMatchingDetails(org.fao.fi.comet.core.engine.model.MatchingsData, java.io.Serializable, java.io.Serializable)
	 */
	@Override
	protected MatchingDetails<ENTITY, ENTITY> doGetMatchingDetails(MatchingsData<ENTITY, ENTITY> matchingsData, DataIdentifier sourceIdentifier, DataIdentifier targetIdentifier) {
		return super.doGetMatchingDetails(matchingsData, sourceIdentifier, targetIdentifier);
	}
}
