/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical;

import java.io.Serializable;
import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours.UBasicBehaviour;
import org.fao.fi.comet.extras.matchlets.skeleton.historical.HistoricalMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
abstract public class UHistoricalMatchletSkeleton<ENTITY extends Serializable, DATA extends DateReferenced>
				extends HistoricalMatchletSkeleton<ENTITY, DATA, ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2794121500041289312L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UHistoricalMatchletSkeleton() {
		this(new UBasicBehaviour<ENTITY, DATA>());
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UHistoricalMatchletSkeleton(UBasicBehaviour<ENTITY, DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.VectorialMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	final public Collection<DATA> extractSourceData(ENTITY source, DataIdentifier sourceIdentifier) {
		return this.extractData(source, sourceIdentifier);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.VectorialMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	final public Collection<DATA> extractTargetData(ENTITY target, DataIdentifier targetIdentifier) {
		return this.extractData(target, targetIdentifier);
	}

	abstract protected Collection<DATA> extractData(ENTITY entity, DataIdentifier dataIdentifier);
}