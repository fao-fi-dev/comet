/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.common;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.uniform.matchlets.UMatchlet;
import org.fao.fi.comet.extras.matchlets.common.IdentityMatchlet;
import org.fao.fi.sh.model.core.spi.Identified;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletDefaultSerializationExclusionPolicy
public class UIdentityMatchlet<ENTITY extends Identified<DATA>, DATA extends Serializable>
	   extends IdentityMatchlet<ENTITY, DATA, ENTITY, DATA>
	   implements UMatchlet<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UIdentityMatchlet() {
		super();

		this._name = "UniformIdentityMatchlet";
	}
}