/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.TimeDistanceAwareMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Matchlet interface for time-distances aware matchlets
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public interface UTimeDistanceAwareMatchlet<ENTITY extends Serializable, DATA extends DateReferenced>
	   extends TimeDistanceAwareMatchlet<ENTITY, DATA, ENTITY, DATA> {
}