/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.skeleton.historical.HistoricalTimeAndValueDistanceAwareMatchletSkeleton;
import org.fao.fi.comet.extras.uniform.matchlets.UTimeDistanceAwareMatchlet;
import org.fao.fi.comet.extras.uniform.matchlets.UValueDistanceAwareMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferencedValued;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class UHistoricalTimeAndValueDistanceAwareMatchletSkeleton<ENTITY extends Serializable, 
																		  DATA extends DateReferencedValued<? extends Number>> 
				extends HistoricalTimeAndValueDistanceAwareMatchletSkeleton<ENTITY, DATA, ENTITY, DATA> 
				implements UTimeDistanceAwareMatchlet<ENTITY, DATA>, 
						   UValueDistanceAwareMatchlet<ENTITY, DATA> {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6557383015276568622L;

	/**
	 * Class constructor
	 *
	 * @param absoluteMaximumTimeDistance
	 * @param timeResolutionUnit
	 * @param maximumRelativeValueDistance
	 */
	public UHistoricalTimeAndValueDistanceAwareMatchletSkeleton(long absoluteMaximumTimeDistance, TimeResolutionUnit timeResolutionUnit, double maximumRelativeValueDistance) {
		super(absoluteMaximumTimeDistance, timeResolutionUnit, maximumRelativeValueDistance);
	}
}