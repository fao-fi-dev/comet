/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.common;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.extras.matchlets.common.MappedEntityMatchlet;
import org.fao.fi.sh.model.core.spi.Groupable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletDefaultSerializationExclusionPolicy
public class UMappedEntityMatchlet<ENTITY extends Groupable<DATA>,
								   DATA extends Serializable> extends MappedEntityMatchlet<ENTITY, DATA, ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UMappedEntityMatchlet() {
		super();

		this._name = "UniformMappedEntityMatchlet";
	}
}