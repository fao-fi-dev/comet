/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.CurrentDataMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * The matchlet interface. To be implemented by each custom matchlet (for
 * vessels, vessel names, reg. nos etc.)
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/giu/2010   ffiorellato     Creation.
 *
 * @version 1.0
 * @since 28/giu/2010
 */
public interface UCurrentDataMatchlet<ENTITY extends Serializable, DATA extends DateReferenced>
	   extends CurrentDataMatchlet<ENTITY, DATA, ENTITY, DATA> {
}