/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.uniform.matchlets.UMatchlet;
import org.fao.fi.comet.extras.matchlets.skeleton.SimpleNameMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.SimplifiedNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Aug 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Aug 2010
 */
@MatchletDefaultSerializationExclusionPolicy
abstract public class USimpleNameMatchletSkeleton<ENTITY extends Serializable,
												 DATA extends DateReferenced & SimplifiedNameAware>
				extends SimpleNameMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UMatchlet<ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4220739817539555026L;

	/**
	 * Class constructor
	 *
	 */
	public USimpleNameMatchletSkeleton() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 * @param simplifiedNameWeight
	 */
	public USimpleNameMatchletSkeleton(BehaviourSkeleton<ENTITY, DATA, ENTITY, DATA> behaviour, double simplifiedNameWeight) {
		super(behaviour, simplifiedNameWeight);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public USimpleNameMatchletSkeleton(BehaviourSkeleton<ENTITY, DATA, ENTITY, DATA> behaviour) {
		super(behaviour);
	}

	/**
	 * Class constructor
	 *
	 * @param simplifiedNameWeight
	 */
	public USimpleNameMatchletSkeleton(double simplifiedNameWeight) {
		super(simplifiedNameWeight);
	}
}