/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.behaviours.BehaviourSkeleton;
import org.fao.fi.comet.core.model.matchlets.ScalarMatchlet;
import org.fao.fi.comet.extras.uniform.matchlets.skeleton.USimpleNameMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.model.core.spi.SimplifiedNameAware;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Jul 2010
 */
abstract public class UCurrentSimpleNameMatchletSkeleton<ENTITY extends Serializable,
														DATA extends DateReferenced & SimplifiedNameAware>
				extends USimpleNameMatchletSkeleton<ENTITY, DATA>
				implements ScalarMatchlet<ENTITY, DATA, ENTITY, DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5051549998242880045L;

	/**
	 * Class constructor
	 *
	 */
	public UCurrentSimpleNameMatchletSkeleton() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 * @param simplifiedNameWeight
	 */
	public UCurrentSimpleNameMatchletSkeleton(BehaviourSkeleton<ENTITY, DATA, ENTITY, DATA> behaviour, double simplifiedNameWeight) {
		super(behaviour, simplifiedNameWeight);
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UCurrentSimpleNameMatchletSkeleton(BehaviourSkeleton<ENTITY, DATA, ENTITY, DATA> behaviour) {
		super(behaviour);
	}

	/**
	 * Class constructor
	 *
	 * @param simplifiedNameWeight
	 */
	public UCurrentSimpleNameMatchletSkeleton(double simplifiedNameWeight) {
		super(simplifiedNameWeight);
	}
}