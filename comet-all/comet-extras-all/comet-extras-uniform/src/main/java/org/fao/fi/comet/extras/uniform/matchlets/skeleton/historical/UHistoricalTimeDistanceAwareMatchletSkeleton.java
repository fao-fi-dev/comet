/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical;

import java.io.Serializable;

import org.fao.fi.comet.extras.matchlets.skeleton.historical.HistoricalTimeDistanceAwareMatchletSkeleton;
import org.fao.fi.comet.extras.uniform.matchlets.UTimeDistanceAwareMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;
import org.fao.fi.sh.utility.model.extensions.dates.TimeResolutionUnit;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 2 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 2 Jul 2010
 */
abstract public class UHistoricalTimeDistanceAwareMatchletSkeleton<ENTITY extends Serializable,
																  DATA extends DateReferenced> 
				extends HistoricalTimeDistanceAwareMatchletSkeleton<ENTITY, DATA, ENTITY, DATA> 
				implements UTimeDistanceAwareMatchlet<ENTITY, DATA> {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6557383015276568622L;
		
	/**
	 * Class constructor
	 *
	 * @param maximumAbsoluteTimeDistance
	 * @param timeResolutionUnit
	 */
	public UHistoricalTimeDistanceAwareMatchletSkeleton(long maximumAbsoluteTimeDistance, TimeResolutionUnit timeResolutionUnit) {
		super(maximumAbsoluteTimeDistance, timeResolutionUnit);
	}
}
