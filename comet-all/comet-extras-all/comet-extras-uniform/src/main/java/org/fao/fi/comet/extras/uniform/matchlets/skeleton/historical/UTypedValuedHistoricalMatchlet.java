/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.historical;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.sh.model.core.spi.DateReferencedValued;
import org.fao.fi.sh.model.core.spi.Typed;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Feb 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Feb 2011
 */
abstract public class UTypedValuedHistoricalMatchlet<ENTITY extends Serializable,
													DATA extends Typed<?> & DateReferencedValued<? extends Number>> 
				extends UHistoricalMatchletSkeleton<ENTITY, DATA> {
	
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6826432008374557426L;
	
	@MatchletParameter(name="excludeTypesComparison")
	protected boolean _excludeTypesComparison = false;

	/**
	 * @return the 'excludeTypesComparison' value
	 */
	final public boolean getExcludeTypesComparison() {
		return this._excludeTypesComparison;
	}

	/**
	 * @param excludeTypesComparison the 'excludeTypesComparison' value to set
	 */
	final public void setExcludeTypesComparison(boolean excludeTypesComparison) {
		this._excludeTypesComparison = excludeTypesComparison;
	}
}