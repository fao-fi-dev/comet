/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets.skeleton.current;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.uniform.matchlets.UMatchlet;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.behaviours.UBasicBehaviour;
import org.fao.fi.comet.extras.matchlets.skeleton.current.CurrentMatchletSkeleton;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
abstract public class UCurrentMatchletSkeleton<ENTITY extends Serializable,
											  DATA extends DateReferenced>
				extends CurrentMatchletSkeleton<ENTITY, DATA, ENTITY, DATA>
				implements UMatchlet<ENTITY, DATA> {

	/** Field serialVersionUID */
	private static final long serialVersionUID = -2794121500041289312L;

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UCurrentMatchletSkeleton() {
		this(new UBasicBehaviour<ENTITY, DATA>());
	}

	/**
	 * Class constructor
	 *
	 * @param behaviour
	 */
	public UCurrentMatchletSkeleton(UBasicBehaviour<ENTITY, DATA> behaviour) {
		super(behaviour);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractSourceData(java.io.Serializable)
	 */
	@Override
	final public DATA extractSourceData(ENTITY source, DataIdentifier sourceIdentifier) {
		return this.extractData(source, sourceIdentifier);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.ScalarMatchlet#extractTargetData(java.io.Serializable)
	 */
	@Override
	final public DATA extractTargetData(ENTITY target, DataIdentifier targetIdentifier) {
		return this.extractData(target, targetIdentifier);
	}

	abstract protected DATA extractData(ENTITY entity, DataIdentifier dataIdentifier);
}