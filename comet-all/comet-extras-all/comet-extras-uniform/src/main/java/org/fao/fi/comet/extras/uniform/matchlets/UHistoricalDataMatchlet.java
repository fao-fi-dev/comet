/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-uniform)
 */
package org.fao.fi.comet.extras.uniform.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.uniform.matchlets.UVectorialMatchlet;
import org.fao.fi.sh.model.core.spi.DateReferenced;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Jul 2010   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Jul 2010
 */
public interface UHistoricalDataMatchlet<ENTITY extends Serializable, DATA extends DateReferenced>
	   extends UVectorialMatchlet<ENTITY, DATA> {
}
