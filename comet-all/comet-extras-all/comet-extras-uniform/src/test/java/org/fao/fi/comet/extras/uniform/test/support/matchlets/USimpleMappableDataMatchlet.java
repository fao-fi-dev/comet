/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.extras.uniform.test.support.matchlets;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.extras.uniform.test.support.data.USimpleMappableData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Mar 2013
 */
public class USimpleMappableDataMatchlet extends UScalarMatchletSkeleton<USimpleMappableData, Character> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6116412804832997470L;

	@MatchletParameter(name="delta")
	private int _delta = 1;

	public USimpleMappableDataMatchlet() {
		super();
	}
	
	/**
	 * Class constructor
	 *
	 * @param delta
	 */
	public USimpleMappableDataMatchlet(int delta) {
		super();

		this._delta = delta;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Fake " + this.getClass().getSimpleName() + " description";
	}	

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.io.Serializable)
	 */
	@Override
	protected Character doExtractData(USimpleMappableData entity, DataIdentifier dataIdentifier) {
		return entity == null ? null : entity.getData();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(USimpleMappableData source, DataIdentifier sourceIdentifier, Character sourceData, USimpleMappableData target, DataIdentifier targetIdentifier, Character targetData) {
		if(targetData == null || sourceData == null)
			return MatchingScore.getNonPerformedTemplate();

		char sourceChar = sourceData.charValue();
		char targetChar = targetData.charValue();

		int delta = Math.abs(sourceChar - targetChar);

		if(delta >=1 && delta <= this._delta)
			return new MatchingScore(1.0 / delta, MatchingType.NON_AUTHORITATIVE);

		return MatchingScore.getNonAuthoritativeNoMatchTemplate();
	}
}