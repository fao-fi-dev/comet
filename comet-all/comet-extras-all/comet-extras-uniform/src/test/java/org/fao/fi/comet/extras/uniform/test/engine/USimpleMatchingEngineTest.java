/**
 * (c) 2013 FAO / UN (project: vrmf-comparison-engine-core)
 */
package org.fao.fi.comet.extras.uniform.test.engine;

import static org.fao.fi.comet.core.model.matchlets.Matchlet.FORCE_COMPARISON_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.MIN_SCORE_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.OPTIONAL_PARAM;
import static org.fao.fi.comet.core.model.matchlets.Matchlet.WEIGHT_PARAM;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.io.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.FeedableDataProvider;
import org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore;
import org.fao.fi.comet.extras.patterns.handlers.id.impl.basic.IdentifiableDataIntegerIDHandler;
import org.fao.fi.comet.extras.uniform.test.support.data.USimpleMappableData;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14/mar/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 14/mar/2013
 */
public class USimpleMatchingEngineTest {
	private UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration> getEngine() {
		UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration> engine = new UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration>();
//		engine.setMinimumAllowedWeightedScore(.5).setProcessListener(new SysOutMatchingProcessHandler());
//
//		engine.addMatchlet(new USimpleMappableDataMatchlet().
//						  	  setWeight(100D).
//						  	  setExclusionCases(MatchingSerializationExclusionPolicy.NON_PERFORMED,
//						  			  			MatchingSerializationExclusionPolicy.AUTHORITATIVE_NO_MATCH).
//						  	  setCutoff(true).
//						  	  setOptional(false).
//						  	  setForceComparison(false).
//						  	  setMinimumAllowedScore(.5));

		return engine;
	}

	private Collection<MatchletConfiguration> getMatchletsConfiguration() {
		Collection<MatchletConfiguration> matchlets = new ArrayList<MatchletConfiguration>();

		matchlets.add(MatchletConfiguration.configure("USimpleMappableDataMatchlet").
													with(WEIGHT_PARAM, 100D).
													with(MIN_SCORE_PARAM, .5D).
													withFalse(OPTIONAL_PARAM).
													withFalse(FORCE_COMPARISON_PARAM));

		matchlets.add(MatchletConfiguration.configure("USimpleMappableDataMatchlet").
													with(WEIGHT_PARAM, 50D).
													with(MIN_SCORE_PARAM, .5D).
													with("delta", 2).
													withFalse(OPTIONAL_PARAM).
													withFalse(FORCE_COMPARISON_PARAM));

		return matchlets;
	}

	private MatchingEngineProcessConfiguration configure(double minScore, int maxCandidates) {
		MatchingEngineProcessConfiguration config = new MatchingEngineProcessConfiguration();
		config.setMinimumAllowedWeightedScore(minScore);
		config.setMaxCandidatesPerEntry(maxCandidates);
		config.setHaltAtFirstValidMatching(false);

		config.setMatchletsConfiguration(this.getMatchletsConfiguration());

		return config;
	}

	private FeedableDataProvider<USimpleMappableData> getSourceDataProvider(int initialId, int finalId) {
		return this.getTargetDataProvider(initialId, finalId);
	}

	private FeedableDataProvider<USimpleMappableData> getTargetDataProvider(int initialId, int finalId) {
		Collection<USimpleMappableData> dataset = new ListSet<USimpleMappableData>();

		char data;
		for(int c=initialId; c<=finalId; c++) {
			data = (char)('a' + c - 1);
			dataset.add(new USimpleMappableData(new Integer(c), new Character(data)));
		}

		return new CollectionBackedDataProvider<USimpleMappableData>(dataset);
	}

	@Test
	public void testMatching() throws Throwable {
		UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration> engine = this.getEngine();
		MatchingEngineProcessConfiguration conf = this.configure(.5, 2);
		MatchingProcessHandler<USimpleMappableData, USimpleMappableData> tracker = null;

		tracker = new SilentMatchingProcessHandler<USimpleMappableData, USimpleMappableData>();
//		tracker = new SysOutMatchingProcessHandler();

		String XML = //XMLBuilderUtils.prettyPrint(
						JAXBHelper.toXML(
							new Class[] { USimpleMappableData.class },
							engine.compareAll(
								conf,
								tracker,
								this.getSourceDataProvider(2, 900),
								new IdentityDataPartitioner<USimpleMappableData, USimpleMappableData>(),
								this.getTargetDataProvider(1, 900),
								new IdentifiableDataIntegerIDHandler<USimpleMappableData>()
							)
						);
					//);

		System.out.println(XML);

		System.out.println("Elapsed: " + tracker.getElapsed());
		System.out.println("Atomic comparisons: " + tracker.getTotalNumberOfAtomicComparisonsPerformed());
		System.out.println("Atomic throughput: " + tracker.getAtomicThroughput() * 1000 + " atomic comparisons per second");
		System.out.println("Throughput: " + tracker.getThroughput() * 1000 + "  comparisons per second");
	}

	@Test
	public void testFeedingFromScratch() throws Throwable {
		UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration> engine = this.getEngine();
		MatchingEngineProcessConfiguration conf = this.configure(.99, 2);
		MatchingProcessHandler<USimpleMappableData, USimpleMappableData> tracker = null;

		tracker = new SilentMatchingProcessHandler<USimpleMappableData, USimpleMappableData>();
//		tracker = new SysOutMatchingProcessHandler();

		String XML = //XMLBuilderUtils.prettyPrint(
						JAXBHelper.toXML(
							new Class[] { USimpleMappableData.class },
							engine.compareAndFeedAll(
								conf,
								tracker,
								this.getSourceDataProvider(1, 900),
								new IdentityDataPartitioner<USimpleMappableData, USimpleMappableData>(),
								new IdentifiableDataIntegerIDHandler<USimpleMappableData>()
							)
						);
					//);

		System.out.println(XML);

		System.out.println("Elapsed: " + tracker.getElapsed());
		System.out.println("Atomic comparisons: " + tracker.getTotalNumberOfAtomicComparisonsPerformed());
		System.out.println("Atomic throughput: " + tracker.getAtomicThroughput() * 1000 + " atomic comparisons per second");
		System.out.println("Throughput: " + tracker.getThroughput() * 1000 + " comparisons per second");
	}

	@Test
	public void testFeeding() throws Throwable {
		UMatchingEngineCore<USimpleMappableData, MatchingEngineProcessConfiguration> engine = this.getEngine();
		MatchingEngineProcessConfiguration conf = this.configure(.99, 2);
		MatchingProcessHandler<USimpleMappableData, USimpleMappableData> tracker = null;

		tracker = new SilentMatchingProcessHandler<USimpleMappableData, USimpleMappableData>();

		String XML = //XMLBuilderUtils.prettyPrint(
						JAXBHelper.toXML(
							new Class[] { USimpleMappableData.class },
							engine.compareAndFeedAll(
								conf,
								tracker,
								this.getSourceDataProvider(4, 7),
								new IdentityDataPartitioner<USimpleMappableData, USimpleMappableData>(),
								this.getTargetDataProvider(1, 3),
								new IdentifiableDataIntegerIDHandler<USimpleMappableData>()
							)
						);
					//);

		System.out.println(XML);

		System.out.println("Elapsed: " + tracker.getElapsed());
		System.out.println("Atomic comparisons: " + tracker.getTotalNumberOfAtomicComparisonsPerformed());
		System.out.println("Atomic throughput: " + tracker.getAtomicThroughput() * 1000 + " atomic comparisons per second");
		System.out.println("Throughput: " + tracker.getThroughput() * 1000 + " comparisons per second");
	}
}