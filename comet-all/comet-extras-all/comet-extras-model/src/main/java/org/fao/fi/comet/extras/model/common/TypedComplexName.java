/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-model)
 */
package org.fao.fi.comet.extras.model.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreComplexNamedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@XmlType(name="TypedComplexName")
@XmlAccessorType(XmlAccessType.FIELD)
public class TypedComplexName extends CoreComplexNamedData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7035044517380157597L;

	@XmlElement(name="type")
	private String _type;

	/**
	 * Class constructor
	 *
	 */
	public TypedComplexName() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 */
	public TypedComplexName(String type) {
		super();

		this._type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CoreComplexNamedData other) {
		if(other == null)
			return 1;

		if(this.equals(other) || this == other)
			return 0;

		int result = this._type == null ?
				( ((TypedComplexName)other)._type == null ? 0 : -1 )
			:
				( ((TypedComplexName)other)._type == null ? 1 : this._type.compareTo(((TypedComplexName)other)._type) );

		if(result == 0)
			result = super.compareTo(other);

		return result;
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param simplifiedName
	 * @param simplifiedNameNGrams
	 * @param simplifiedNameSoundex
	 */
	public TypedComplexName(String type, String name, String simplifiedName, String[] simplifiedNameNGrams, String simplifiedNameSoundex) {
		super(name, simplifiedName, simplifiedNameNGrams, simplifiedNameSoundex);
		this._type = type;
	}

	/**
	 * @return the 'type' value
	 */
	public String getType() {
		return this._type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	public void setType(String type) {
		this._type = type;
	}

	public boolean equalData(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._type == null) ? 0 : this._type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypedComplexName other = (TypedComplexName) obj;
		if (this._type == null) {
			if (other._type != null)
				return false;
		} else if (!this._type.equals(other._type))
			return false;
		return true;
	}
}