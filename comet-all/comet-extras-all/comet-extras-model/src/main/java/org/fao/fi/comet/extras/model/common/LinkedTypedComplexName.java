/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: comet-extras-model)
 */
package org.fao.fi.comet.extras.model.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreComplexNamedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Oct 2013
 */
@XmlType(name="LinkedTypedComplexName")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkedTypedComplexName extends TypedComplexName {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5915905922941787773L;

	@XmlElement(name="parentId")
	private String _parentId;

	/**
	 * Class constructor
	 *
	 */
	public LinkedTypedComplexName() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param type
	 * @param name
	 * @param simplifiedName
	 * @param simplifiedNameSoundex
	 */
	public LinkedTypedComplexName(String parentId, String type, String name, String simplifiedName, String[] simplifiedNameNGrams, String simplifiedNameSoundex) {
		super(type, name, simplifiedName, simplifiedNameNGrams, simplifiedNameSoundex);

		this._parentId = parentId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CoreComplexNamedData other) {
		if(other == null)
			return 1;

		if(this.equals(other) || this == other)
			return 0;

		int result = super.compareTo(other);

		if(result != 0 || !(other instanceof LinkedTypedComplexName))
			return result;

		result = this._parentId == null ?
					( ((LinkedTypedComplexName)other)._parentId == null ? 0 : -1 )
				 :
					( ((LinkedTypedComplexName)other)._parentId == null ? 1 : this._parentId.compareTo(((LinkedTypedComplexName)other)._parentId) );

		return result;
	}

	/**
	 * @return the 'parentId' value
	 */
	public String getParentId() {
		return this._parentId;
	}

	/**
	 * @param parentId the 'parentId' value to set
	 */
	public void setParentId(String parentId) {
		this._parentId = parentId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._parentId == null) ? 0 : this._parentId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinkedTypedComplexName other = (LinkedTypedComplexName) obj;
		if (this._parentId == null) {
			if (other._parentId != null)
				return false;
		} else if (!this._parentId.equals(other._parentId))
			return false;
		return true;
	}
}